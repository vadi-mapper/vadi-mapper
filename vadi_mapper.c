
/*
 * Copyright (c) 2004, 2005, 2008, 2009, 2010, 2011, 2012, 2013 Andrei Vasiliu, Vadim Peretokin, Florian Scheel, Roland Goldberg
 *
 *
 * This file is part of the Vadi Mapper.
 *
 * MudBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * MudBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vadi Mapper; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#define I_MAPPER_ID "$Name:  $ $Id: i_mapper.c,v 3.13 2008/01/26 10:24:22 tobiassjosten Exp $"

#include "module.h"
#include "vadi_mapper.h"

#include <sys/stat.h>
#include <stdio.h>

#include <curl/curl.h>
#include <curl/easy.h>
#include <glib.h>
#include <glib/gstdio.h>

int mapper_version_major = 1;
int mapper_version_minor = 4;
int mapper_version_bugfix = 0;

char *i_mapper_id = I_MAPPER_ID "\r\n" I_MAPPER_H_ID "\r\n" HEADER_ID "\r\n" MODULE_ID "\r\n";

char *room_color = "\33[33m";
int room_color_len = 5;

char map_file[256] = "IMap";
char map_file_area[256] = "IMap.area";
char map_file_bin[256] = "IMap.bin";

/* A few things we'll need. */
char *dir_name[] = { "none", "north", "northeast", "east", "southeast", "south",
    "southwest", "west", "northwest", "up", "down", "in", "out", NULL
};

char *dir_small_name[] = { "-", "n", "ne", "e", "se", "s", "sw", "w", "nw", "u", "d", "in", "out", NULL };

char *dir_small_name_reverse[] =
    { "-", "s", "sw", "w", "nw", "n", "ne", "e", "se", "d", "u", "out", "in", NULL };

char *map_tags[] = { "wilderness", "shop", "news", "postal", "bank", "harbour", "commshop", NULL };

char *map_tag_signs[] = { C_y "w", C_Y "$", C_R "N", C_B "P", C_G "$", C_B "H", C_Y "C", NULL };

int reverse_exit[] = { 0, EX_S, EX_SW, EX_W, EX_NW, EX_N, EX_NE, EX_E, EX_SE,
    EX_D, EX_U, EX_OUT, EX_IN, 0
};

ROOM_TYPE *room_types;
ROOM_TYPE *null_room_type;

COLOR_DATA color_names[] = {
    {"normal", C_0, "\33[0m", 4},
    {"red", C_r, "\33[31m", 5},
    {"green", C_g, "\33[32m", 5},
    {"brown", C_y, "\33[33m", 5},
    {"blue", C_b, "\33[34m", 5},
    {"magenta", C_m, "\33[35m", 5},
    {"cyan", C_c, "\33[36m", 5},
    {"white", C_0, "\33[37m", 5},
    {"dark", C_D, C_D, 7},
    {"bright-red", C_R, C_R, 7},
    {"bright-green", C_G, C_G, 7},
    {"bright-yellow", C_Y, C_Y, 7},
    {"bright-blue", C_B, C_B, 7},
    {"bright-magenta", C_M, C_M, 7},
    {"bright-cyan", C_C, C_C, 7},
    {"bright-white", C_W, C_W, 7},
    {"background-red", C_b_r, C_b_r, 5},
    {"background-yellow", C_b_y, C_b_y, 5},
    {"background-green", C_b_g, C_b_g, 5},
    {"background-blue", C_b_b, C_b_b, 5},
    {"background-magenta", C_b_m, C_b_m, 5},
    {"background-cyan", C_b_c, C_b_c, 5},
    {"background-white", C_b_w, C_b_w, 5},

    {NULL, NULL, 0}
};

ROOM_DATA *world;
ROOM_DATA *world_last;
ROOM_DATA *current_room;
ROOM_DATA *hash_world[MAX_HASH];

ROOM_DATA *link_next_to;
ROOM_DATA *last_room;

AREA_DATA *areas;
AREA_DATA *areas_last;
AREA_DATA *current_area;

EXIT_DATA *global_special_exits;

ROOM_DATA *map[MAP_X + 2][MAP_Y + 2];
char extra_dir_map[MAP_X + 2][MAP_Y + 2];

/* New! */
MAP_ELEMENT map_new[MAP_X][MAP_Y];

/* Newer! */
ELEMENT *exit_tags;

int last_vnum;

/* Path finder chains. */
ROOM_DATA *pf_current_openlist;
ROOM_DATA *pf_new_openlist;

/* New path-finder chains. */
#define MAX_COST 13
int pf_step = 0, pf_rooms;
ROOM_DATA *pf_queue[MAX_COST];

/* Command queue. -1 is look, positive number is direction. */
int queue[256];
int q_top;

int auto_bump;
ROOM_DATA *bump_room;
int bump_exits;

/* option to color indoor rooms differently from outdoor ones */
int highlight_indoors = 0;

/* Threading */
THREAD_DATA thread;

/* Where deleted room numbers go for recycling */
GQueue *recycle_bin;

/* Various regexes for matching */
GRegex *scry_new_regex;

/* Used for counting recycled rooms */
int recycled_rooms_count;

/* Used to find a vnum in the bin */
int compare_recycled_rooms(gpointer data, gpointer user_data);

int farseeing; /* Keep track if we're using farsee or another ability */

int gallop_direction; /* keep track of the galloping direction */

int composing = 0; /* Disable aliases and triggers in the editor */

int wait_balance = 0; /* Wait for balance or equilibrum (for example afer scrying or warping) */

/* convenience goto function: goto person */
int goto_person = 0;

int indoors = 2; //This keeps track of indoor rooms

/* all the "wholist" stuff goes here */
int wholist=0;               //did we want to parse wholist?
char wholist_argument[256];  //did we give any arguments to wholist?

/* Here we register our functions. */

void i_mapper_module_init_data();
void i_mapper_module_unload();
void i_mapper_process_server_prompt(LINE * line);
void i_mapper_process_server_paragraph(LINES * l);
int i_mapper_process_client_command(char *cmd);
int i_mapper_process_client_aliases(char *line);
void i_mapper_mxp_enabled();
void do_vmap_follow(char *arg);
int save_settings(char *file);
int g_unlink(const gchar * filename);
int g_rename(const gchar * oldfilename, const gchar * newfilename);
int load_map(char *file, char *specific_area);
void convert_vnum_exits();
int check_map();
void recycle_room(int i);
void clear_recycle_bin();
void fill_recycle_bin();
gpointer download_file(gpointer null);
void do_vmap_save(char *arg);
void do_goto(char *arg);
AREA_DATA *get_area_by_name(char *string, int areanumber);
void link_special_exit(ROOM_DATA * source, EXIT_DATA * spexit, ROOM_DATA * dest);
int case_strcmp(const char *s1, const char *s2);

ENTRANCE(i_mapper_module_register)
{
    self->name = strdup("Mapper");
    self->version_major = mapper_version_major;
    self->version_minor = mapper_version_minor;
    self->id = i_mapper_id;

    self->init_data = i_mapper_module_init_data;
    self->unload = i_mapper_module_unload;
    self->process_server_prompt = i_mapper_process_server_prompt;
    self->process_server_paragraph = i_mapper_process_server_paragraph;
    self->process_client_aliases = i_mapper_process_client_aliases;
    self->mxp_enabled = i_mapper_mxp_enabled;

    GET_FUNCTIONS(self);
}

int get_free_vnum()
{
    last_vnum++;
    return last_vnum;
}

char *background_foreground(char *name)
{
    if (!strcmp(name, C_b_r))
        return C_r;
    else if (!strcmp(name, C_b_g))
        return C_g;
    else if (!strcmp(name, C_b_y))
        return C_y;
    else if (!strcmp(name, C_b_b))
        return C_b;
    else if (!strcmp(name, C_b_m))
        return C_m;
    else if (!strcmp(name, C_b_c))
        return C_c;
    else if (!strcmp(name, C_b_w))
        return C_W;
    else
        return NULL;
}

void refind_last_vnum()
{
    ROOM_DATA *room;

    last_vnum = 0;

    for (room = world; room; room = room->next_in_world)
        if (last_vnum < room->vnum)
            last_vnum = room->vnum;
}

void refind_last_good_vnum()
{
    ROOM_DATA *room;

    last_vnum = 0;

    for (room = world; room; room = room->next_in_world)
        if (last_vnum < room->vnum)
            last_vnum = room->vnum;
}

void link_to_area(ROOM_DATA * room, AREA_DATA * area)
{
    DEBUG("link_to_area");

    /* Link to an area. */
    if (area) {
        if (area->last_room) {
            area->last_room->next_in_area = room;
            room->next_in_area = NULL;
        } else {
            room->next_in_area = area->rooms;
            area->rooms = room;
        }

        area->last_room = room;
        room->area = area;
    } else if (current_area)
        link_to_area(room, current_area);
    else if (areas) {
        debugf("No current area set, while trying to link a room.");
        link_to_area(room, areas);
    } else
        clientfv("Can't link this room anywhere.");
}

void unlink_from_area(ROOM_DATA * room)
{
    ROOM_DATA *r;

    DEBUG("unlink_from_area");

    /* Unlink from area. */
    if (room->area->rooms == room) {
        room->area->rooms = room->next_in_area;

        if (room->area->last_room == room)
            room->area->last_room = NULL;
    } else
        for (r = room->area->rooms; r; r = r->next_in_area) {
            if (r->next_in_area == room) {
                r->next_in_area = room->next_in_area;

                if (room->area->last_room == room)
                    room->area->last_room = r;

                break;
            }
        }
}

char *cutoff(char *buf)
{
    if (buf[0])
        buf[strlen(buf) - 1] = '\0';
    return buf;
}

ROOM_DATA *create_room(int vnum_create, char *error_string, int max_size)
{
    ROOM_DATA *new_room;
    int recycled = 0;

    DEBUG("create_room");

    if (edit_only) {

        /* Have we got an ID we can re-use? */
        if (!g_queue_is_empty(recycle_bin)) {
            int *x;

            recycled = 1;
            x = (int *) g_queue_pop_tail(recycle_bin);
            vnum_create = *x;
            g_free(x);
        } else {
            snprintf(error_string, max_size,
                     "Couldn't create the new room - we're in editing only mode, and we don't have any recycled numbers to use.");
            return NULL;
        }
    }

    /* calloc will also clear everything to 0, for us. */
    new_room = calloc(sizeof(ROOM_DATA), 1);

    /* Init. */
    new_room->name = NULL;
    new_room->special_exits = NULL;
    new_room->room_type = null_room_type;
	new_room->warp = NULL;
	new_room->indoors = 2;
    if (vnum_create < 0) {

        /* Have we got an ID we can re-use? */
        if (!g_queue_is_empty(recycle_bin)) {
            int *x;

            recycled = 1;
            x = (int *) g_queue_pop_tail(recycle_bin);
            new_room->vnum = *x;
            g_free(x);
        } else {
            new_room->vnum = get_free_vnum();
        }
    } else {
        new_room->vnum = vnum_create;
    }

    new_room->recycled = recycled;

    /* Link to main chain. */
    if (!world) {
        world = new_room;
        world_last = new_room;
    } else
        world_last->next_in_world = new_room;

    new_room->next_in_world = NULL;
    world_last = new_room;

    /* Link to hashed table. */
    if (!hash_world[new_room->vnum % MAX_HASH]) {
        hash_world[new_room->vnum % MAX_HASH] = new_room;
        new_room->next_in_hash = NULL;
    } else {
        new_room->next_in_hash = hash_world[new_room->vnum % MAX_HASH];
        hash_world[new_room->vnum % MAX_HASH] = new_room;
    }

    /* Link to current area. */
    link_to_area(new_room, current_area);

    return new_room;
}

AREA_DATA *create_area()
{
    AREA_DATA *new_area;

    DEBUG("create_area");

    new_area = calloc(sizeof(AREA_DATA), 1);

    /* Init. */
    new_area->name = NULL;
    new_area->rooms = NULL;
	new_area->warps = NULL;

    /* Link to main chain. */
    if (!areas) {
        areas = new_area;
        areas_last = new_area;
    } else
        areas_last->next = new_area;

    new_area->next = NULL;
    areas_last = new_area;

    return new_area;
}

EXIT_DATA *create_exit(ROOM_DATA * room)
{
    EXIT_DATA *new_exit, *e;

    new_exit = calloc(sizeof(EXIT_DATA), 1);

    new_exit->vnum = -1;

    /* If room is null, create it in the global list. */
    if (room) {
        if (room->special_exits) {
            e = room->special_exits;
            while (e->next)
                e = e->next;
            e->next = new_exit;
        } else {
            room->special_exits = new_exit;
        }

        new_exit->next = NULL;
        new_exit->owner = room;
    } else {
        if (global_special_exits) {
            e = global_special_exits;
            while (e->next)
                e = e->next;
            e->next = new_exit;
        } else {
            global_special_exits = new_exit;
        }

        new_exit->next = NULL;
        new_exit->owner = NULL;
    }

    return new_exit;
}

WARP_DATA *create_warp_structure(ROOM_DATA *source, ROOM_DATA *dest, EXIT_DATA *spexit){

    WARP_DATA *new_warp, *w;

	DEBUG("create_warp_structure");

	if(!source){
		clientfv("Don't know the source room. Please map it first.");
		return NULL;
	}

	if(source->warp)
		return NULL;

	if(!dest){
		clientfv("Don't know the destination room. Please map it first.");
		return NULL;
	}

    new_warp = calloc(1,sizeof(WARP_DATA));

	new_warp->link = spexit;

    new_warp->area = source->area;
	new_warp->next = NULL;
	new_warp->reverse = NULL;
	source->warp = new_warp;
	new_warp->source = source;
	if(!source->area->warps){
		source->area->warps=new_warp;
	}else{
		for(w = source->area->warps; w; w=w->next){
			if(!w->next){
				w->next = new_warp;
				break;
			}
		}
	}

	new_warp->link->to = dest;

	return new_warp;

}

WARP_DATA *create_warp(ROOM_DATA *source, ROOM_DATA *dest)
{
    WARP_DATA *new_warp;

	DEBUG("create_warp");

	if(!source){
		clientfv("Don't know the source room. Please map it first.");
		return NULL;
	}

	if(source->warp)
		return NULL;

	if(!dest){
		clientfv("Don't know the destination room. Please map it first.");
		return NULL;
	}

    new_warp = create_warp_structure(source, dest, create_exit(source));


	link_special_exit(source, new_warp->link, dest);
	new_warp->link->vnum = dest->vnum;
	new_warp->link->cost = 3;
	new_warp->link->alias = 0;
	new_warp->link->command = strdup("worm warp");
	new_warp->link->message = strdup("The wormhole spits you out at the other end.");

	return new_warp;
}

void free_exit(EXIT_DATA * spexit)
{
	DEBUG("free_exit");

    if (spexit->command)
        free(spexit->command);
    if (spexit->message)
        free(spexit->message);
    free(spexit);
}

void link_element(ELEMENT * elem, ELEMENT ** first)
{
    elem->next = *first;
    if (elem->next)
        elem->next->prev = elem;
    elem->prev = NULL;

    *first = elem;
    elem->first = first;
}

void unlink_element(ELEMENT * elem)
{
    if (elem->prev)
        elem->prev->next = elem->next;
    else
        *(elem->first) = elem->next;

    if (elem->next)
        elem->next->prev = elem->prev;
}

void unlink_rooms(ROOM_DATA * source, int dir, ROOM_DATA * dest)
{
    REVERSE_EXIT *rexit, *r;

    if (!dir)
        return;

    for (rexit = dest->rev_exits; rexit; rexit = rexit->next)
        if (rexit->from == source && rexit->direction == dir)
            break;

    if (!rexit)
        return;

    /* Unlink. */
    if (rexit == dest->rev_exits)
        dest->rev_exits = rexit->next;
    else {
        for (r = dest->rev_exits; r->next != rexit; r = r->next);
        r->next = rexit->next;
    }

    free(rexit);

    source->exits[dir] = NULL;
	source->detected_exits[dir] = 1;
}

void link_rooms(ROOM_DATA * source, int dir, ROOM_DATA * dest)
{
    REVERSE_EXIT *rexit;

    if (!dir)
        return;

    /* Just to be sure. */
    if (source->exits[dir])
        unlink_rooms(source, dir, source->exits[dir]);

    source->exits[dir] = dest;

    rexit = calloc(1, sizeof(REVERSE_EXIT));
    rexit->next = dest->rev_exits;
    rexit->from = source;
    rexit->direction = dir;
    rexit->spexit = NULL;

    dest->rev_exits = rexit;
}

void link_special_exit(ROOM_DATA * source, EXIT_DATA * spexit, ROOM_DATA * dest)
{
    REVERSE_EXIT *rexit;

	DEBUG("link_special_exit");

    if (!spexit)
        return;

    rexit = calloc(1, sizeof(REVERSE_EXIT));
    rexit->next = dest->rev_exits;
    rexit->from = source;
    rexit->direction = 0;
    rexit->spexit = spexit;

    dest->rev_exits = rexit;
}

void unlink_special_exit(ROOM_DATA * source, EXIT_DATA * spexit, ROOM_DATA * dest)
{
    REVERSE_EXIT *rexit = NULL, *r;

	DEBUG("unlink_special_exit");

    if (!spexit)
        return;
	if(dest)
		for (rexit = dest->rev_exits; rexit; rexit = rexit->next)
			if (rexit->from == source && rexit->spexit == spexit)
				break;
    if (!rexit)
        return;

    /* Unlink. */
	if (rexit == dest->rev_exits){
        dest->rev_exits = rexit->next;
	}else {
        for (r = dest->rev_exits; r && r->next != rexit; r = r->next);
		if(r){
			r->next = rexit->next;
		}
    }

    free(rexit);
}

void destroy_exit(EXIT_DATA * spexit)
{
    ROOM_DATA *room;
    EXIT_DATA *e;

	DEBUG("destroy_exit");

	if(!spexit){
		return;
	}

    /* Delete its reverse. */
    if ((room = spexit->to) && spexit->owner) {
        unlink_special_exit(spexit->owner, spexit, room);
    }

    /* Unlink from room, or global. */
    if ((room = spexit->owner)) {
        if (room->special_exits == spexit)
            room->special_exits = spexit->next;
        else
            for (e = room->special_exits; e; e = e->next)
                if (e->next == spexit) {
                    e->next = spexit->next;
                    break;
                }
    } else {
        if (global_special_exits == spexit)
            global_special_exits = spexit->next;
        else
            for (e = global_special_exits; e; e = e->next)
                if (e->next == spexit) {
                    e->next = spexit->next;
                    break;
                }
    }

    /* Free it up. */
    free_exit(spexit);
}

void destroy_warp(WARP_DATA *warp){

	WARP_DATA *w;

	DEBUG("destroy_warp");

	if(!warp || !warp->area){
		return;
	}

	if(warp==warp->area->warps){
		warp->area->warps=warp->next;
	}else{
		for(w=warp->area->warps; w; w = w->next){
			if(w->next==warp){
				w->next=warp->next;
				break;
			}
		}
	}
	if(warp->reverse){
		warp->reverse->reverse = NULL;
		destroy_warp(warp->reverse);
	}
	warp->reverse=NULL;
        if(warp->link)
            destroy_exit(warp->link);
	warp->source->warp = NULL;
	warp->source = NULL;
	warp->link = NULL;
	warp->area = NULL;
	free(warp);

}

void free_room(ROOM_DATA * room)
{
    int i;
    ELEMENT *temp;

	if(room->warp)
		destroy_warp(room->warp);

    for (i = 1; dir_name[i]; i++)
        if (room->exits[i])
            unlink_rooms(room, i, room->exits[i]);

    while (room->rev_exits) {
        if (room->rev_exits->direction)
            unlink_rooms(room->rev_exits->from, room->rev_exits->direction, room);
		else{
			if(room->rev_exits->spexit->owner){
				destroy_exit(room->rev_exits->spexit);
			}else{
				unlink_special_exit(room->rev_exits->from,room->rev_exits->spexit,room);
			}
		}
    }

    while (room->special_exits)
        destroy_exit(room->special_exits);

    while (room->tags) {
        temp = room->tags;
        unlink_element(room->tags);
        free(temp->p);
        free(temp);
    }

    if (room->name)
        free(room->name);

    free(room);
}

void destroy_room(ROOM_DATA * room)
{
    ROOM_DATA *r;

    unlink_from_area(room);

    /* Unlink from world. */
    if (world == room) {
        world = room->next_in_world;
        if (room == world_last)
            world_last = world;
    } else
        for (r = world; r; r = r->next_in_world) {
            if (r->next_in_world == room) {
                r->next_in_world = room->next_in_world;
                if (room == world_last)
                    world_last = r;
                break;
            }
        }

    /* Unlink from hash table. */
    if (room == hash_world[room->vnum % MAX_HASH])
        hash_world[room->vnum % MAX_HASH] = room->next_in_hash;
    else {
        for (r = hash_world[room->vnum % MAX_HASH]; r; r = r->next_in_hash) {
            if (r->next_in_hash == room) {
                r->next_in_hash = room->next_in_hash;
                break;
            }
        }
    }

    /* Send to room recycle bin */
    recycle_room(room->vnum);

    /* Free it up. */
    free_room(room);
}

void do_area_destroy(char *arg)
{
    ROOM_DATA *room;

    for (room = world; room; room = room->next_in_world) {
        if (case_strstr((char *) room->area->name, arg)) {

            /* Can't destroy the area you're in */
            if (case_strstr((char *) room->area->name, current_area->name)) {
                clientff(C_W "[" C_B "Map" C_W
                         "]: Can't destroy the area you're in (%s). Please move out first.\r\n"
                         C_0, room->area->name);
                return;
            }

            clientff(C_W "[" C_B "Map" C_W
                     "]: '%s' destroyed. (You better rebuild it now!)\r\n" C_0, room->area->name);
            destroy_area(room->area);
            return;

        }

    }

    /* Still here? We didn't find it then */
    clientfv("Can't find that area on the map.");

}

void destroy_area(AREA_DATA * area)
{
    AREA_DATA *a;
    ROOM_DATA *room, *next_room;

    for (room = area->rooms; room; room = next_room) {
        next_room = room->next_in_area;
        destroy_room(room);
    }

    /* Unlink from areas. */
    if (area == areas) {
        areas = area->next;
        if (area == areas_last)
            areas_last = areas; /* Always null, anyways. */
    } else
        for (a = areas; a; a = a->next) {
            if (a->next == area) {
                a->next = area->next;
                if (area == areas_last)
                    areas_last = a;
                break;
            }
        }

    /* Free it up. */
    if (area->name)
        free(area->name);

    free(area);
}

/* Destroy everything. */
void destroy_map()
{
    int i;

    DEBUG("destroy_map");

	/* Areas. */
    while (areas)
        destroy_area(areas);

    /* Rooms. */
    while (world)
        destroy_room(world);

    /* Global special exits. */
    while (global_special_exits)
        destroy_exit(global_special_exits);


    /* Hash table. */
    for (i = 0; i < MAX_HASH; i++)
        hash_world[i] = NULL;

    world = world_last = NULL;
    areas = areas_last = NULL;
    current_room = NULL;
    current_area = NULL;

    mode = NONE;

	/* Clear our map version */
	map_version = 0;

    refind_last_vnum();
}

/* Free -everything- up, and prepare to be destroyed. */
void i_mapper_module_unload()
{

    g_regex_unref(scry_new_regex);

    save_settings("config.mapper.txt");

    del_timer("queue_reset_timer");
    del_timer("remove_players");
    del_timer("remove_marks");
    del_timer("remove_wormholes");

    destroy_map();

	clear_recycle_bin();

	g_queue_free(recycle_bin);
}

/* This is what the hash table is for. */
ROOM_DATA *get_room(int vnum)
{
    ROOM_DATA *room;

    for (room = hash_world[vnum % MAX_HASH]; room; room = room->next_in_hash)
        if (room->vnum == vnum)
            return room;

    return NULL;
}

void t_autoupdate(TIMER * self)
{
    do_vmap_update("");
}

void t_farsee(TIMER * self)
{
    farseeing = 0;
}

void queue_reset(TIMER * self)
{
    if (!q_top)
        return;

    q_top = 0;

    clientff("\r\n" C_R "[" C_D "Mapper's command queue cleared." C_R "]" C_0 "\r\n");
    show_prompt();
}

void add_queue(int value)
{
    int i;

    for (i = q_top; i > 0; i--)
        queue[i] = queue[i - 1];
    q_top++;
    queue[0] = value;

    add_timer("queue_reset_timer", 10, queue_reset, 0, 0, 0);
}

void add_queue_top(int value)
{
    queue[q_top] = value;
    q_top++;

    add_timer("queue_reset_timer", 10, queue_reset, 0, 0, 0);
}

int must_swim(ROOM_DATA * src, ROOM_DATA * dest)
{
    if (!src || !dest)
        return 0;

    if (disable_swimming)
        return 0;

    if (pear_defence == 2)
        return 0;

    if (src->room_type->must_swim || dest->room_type->must_swim)
        return 1;

    return 0;
}

int room_cmp(const char *room, const char *smallr)
{
    if (!strcmp(room, smallr))
        return 0;

    return 1;
}

/* This is a title. Do something with it. */
int parse_title(const char *line)
{
    ROOM_DATA *new_room;
    int created = 0;
    int q, i;

    DEBUG("parse_title");

    /* Capturing mode. */
    if (mode == CREATING && capture_special_exit ==2 && !q_top) {
        EXIT_DATA *spexit;

        capture_special_exit = 0;
        new_room = NULL;

        if (!current_room) {
            clientff(C_R " (Current Room is NULL! Capturing disabled)" C_0);
            mode = FOLLOWING;
            return 0;
        }

        if (!cse_message[0]) {
            clientff(C_R " (No message found! Capturing disabled)" C_0);
            mode = FOLLOWING;
            return 0;
        }

        if (special_exit_vnum > 0) {
            new_room = get_room(special_exit_vnum);

            if (!new_room) {
                clientf(C_R " (Destination room is now NULL! Capturing disabled)" C_0);
                mode = FOLLOWING;
                return 0;
            }

            /* Make sure the destination matches. */
            if (strcmp(line, new_room->name) && !(similar && !room_cmp(new_room->name, line))) {
                clientf(C_R " (Destination does not match! Capturing disabled)" C_0);
                mode = FOLLOWING;
                return 0;
            }
        } else if (!special_exit_vnum) {
            char error[256];
            new_room = create_room(-1, error, 256);
            if (!new_room) {
                clientff("\r\n");
                clientfv(error);
                return 0;
            }
            new_room->name = strdup(line);

        }

        spexit = create_exit(current_room);

        if (special_exit_vnum >= 0) {
            clientff(C_R " (" C_W "sp:" C_G "%d" C_R ")" C_0, new_room->vnum);
            spexit->to = new_room;
            spexit->vnum = new_room->vnum;
            link_special_exit(current_room, spexit, new_room);
        } else {
            mode = GET_UNLOST;
            spexit->vnum = -1;
        }

        clientff(C_R "\r\n[Special exit created.]");

        if (special_exit_nocommand || !cse_command[0]) {
            clientff(C_R "\r\nCommand: " C_W "none" C_R "." C_0);
            spexit->command = NULL;
        } else {
            clientff(C_R "\r\nCommand: '" C_W "%s" C_R "'" C_0, cse_command[0] ? cse_command : "null");
            spexit->command = strdup(cse_command);
        }

        clientff(C_R "\r\nMessage: '" C_W "%s" C_R "'" C_0, cse_message);
        spexit->message = strdup(cse_message);

        if (special_exit_alias) {
            clientff(C_R "\r\nAlias: '" C_W "%s" C_R "'" C_0, dir_name[special_exit_alias]);
            spexit->alias = special_exit_alias;
        }

        if (new_room) {
            current_room = new_room;
            current_area = current_room->area;
        }
        // Why was it -1? There must've been a reason..
        return 2;
    }

    /* Following or Mapping mode. */
    if (mode == FOLLOWING || mode == CREATING) {
        /* Queue empty? */
        if (!q_top) {
            /* parsing_room = 0; */
            return 0;
        }

        q = queue[q_top - 1];
        q_top--;

        if (!q_top)
            del_timer("queue_reset_timer");

        if (!current_room) {
            clientff(" (Current room is null, while mapping is not!)");
            mode = NONE;
            /* parsing_room = 0; */
            return 0;
        }

        /* Not just a 'look'? */
        if (q > 0) {
            if (mode == FOLLOWING) {
                if (current_room->exits[q]) {
                    current_room = current_room->exits[q];
                    current_area = current_room->area;
                } else {
                    /* We moved into a strange exit, while not creating. */
                    clientf(C_R " (" C_G "lost" C_R ")" C_0);
                    current_room = NULL;
                    mode = GET_UNLOST;
                }
            }

            else if (mode == CREATING) {
                if (current_room->exits[q]) {
                    /* Just follow around. */
                    new_room = current_room->exits[q];
                } else {
                    char *color = C_G;

                    /* Check for autolinking. */
                    if (!link_next_to && !disable_autolink && !switch_exit_stops_mapping) {
                        ROOM_DATA *get_room_at(int dir, int length);
                        int length;

                        if (set_length_to == -1)
                            length = 1;
                        else
                            length = set_length_to + 1;

                        link_next_to = get_room_at(q, length);

                        if (link_next_to && strcmp(line, link_next_to->name))
                            link_next_to = NULL;

                        if (link_next_to && link_next_to->exits[reverse_exit[q]])
                            link_next_to = NULL;

                        color = C_C;
                    }

                    /* Create or link an exit. */
                    if (link_next_to) {
                        new_room = link_next_to;
                        link_next_to = NULL;
                        clientff(C_R " (%slinked" C_R ")" C_0, color);
                    } else {
                        char error[256];
                        new_room = create_room(-1, error, 256);
                        if (!new_room) {
                            clientff("\r\n");
                            clientfv(error);
                            return 0;
                        }
                        clientff(C_R " (" C_G "%s" C_R ")" C_0, new_room->recycled ? "recycled" : "created");
                        created = 1;
                        if (!disable_autolink)
                            check_for_duplicates = 1;
                    }

                    link_rooms(current_room, q, new_room);
                    if (!unidirectional_exit) {
                        if (!new_room->exits[reverse_exit[q]]) {
                            link_rooms(new_room, reverse_exit[q], current_room);
                        } else {
                            current_room->exits[q] = NULL;
                            clientf(C_R " (" C_G "unlinked: reverse error" C_R ")");
                        }
                    } else
                        unidirectional_exit = 0;
                }

                /* Change the length, if asked so. */
                if (set_length_to) {
                    if (set_length_to == -1)
                        set_length_to = 0;

                    current_room->exit_length[q] = set_length_to;
                    if (new_room->exits[reverse_exit[q]] == current_room)
                        new_room->exit_length[reverse_exit[q]] = set_length_to;
                    clientf(C_R " (" C_G "new length set" C_R ")" C_0);

                    set_length_to = 0;
                }

                /* Stop mapping from here on? */
                if (switch_exit_stops_mapping) {
                    i = current_room->exit_stops_mapping[q];

                    i = !i;

                    current_room->exit_stops_mapping[q] = i;
                    if (new_room->exits[reverse_exit[q]] == current_room)
                        new_room->exit_stops_mapping[reverse_exit[q]] = i;

                    if (i)
                        clientf(C_R " (" C_G "exit stop set" C_R ")" C_0);
                    else
                        clientf(C_R " (" C_G "exit stop unset" C_R ")" C_0);

                    switch_exit_stops_mapping = 0;
                }

                /* Show rooms even from another area? */
                if (switch_exit_joins_areas) {
                    i = current_room->exit_joins_areas[q];

                    i = !i;

                    if (i && (current_room->area == new_room->area))
                        clientf(C_R " (" C_G "did not join areas" C_R ")" C_0);
                    else {
                        current_room->exit_joins_areas[q] = i;
                        if (new_room->exits[reverse_exit[q]] == current_room)
                            new_room->exit_joins_areas[reverse_exit[q]] = i;

                        if (i)
                            clientf(C_R " (" C_G "areas joined" C_R ")" C_0);
                        else
                            clientf(C_R " (" C_G "areas unjoined" C_R ")" C_0);
                    }

                    switch_exit_joins_areas = 0;
                }

                /* Show this somewhere else on the map, instead? */
                if (use_direction_instead) {
                    if (use_direction_instead == -1)
                        use_direction_instead = 0;

                    current_room->use_exit_instead[q] = use_direction_instead;
                    if (new_room->exits[reverse_exit[q]] == current_room)
                        new_room->use_exit_instead[reverse_exit[q]] = reverse_exit[use_direction_instead];
                    if (use_direction_instead)
                        clientf(C_R " (" C_G "different direction set" C_R ")" C_0);
                    else
                        clientf(C_R " (" C_G "different direction unset" C_R ")" C_0);

                    use_direction_instead = 0;
                }

                current_room = new_room;
                current_area = new_room->area;
            }
		}else{
			/*Did we shadow?*/
			if(q==-2){
				/* look for a room with that name */
				for(i=1; dir_name[i];i++){
					if(current_room->exits[i]&&!strcmp(current_room->exits[i]->name,line)){
						current_room = current_room->exits[i];
						current_area = current_room->area;
						break;
					}
				}
			}
		}

        if (mode == CREATING) {
            if (!current_room->name || strcmp(line, current_room->name)) {
                if (!created)
                    clientf(C_R " (" C_G "room name updated" C_R ")" C_0);

                if (current_room->name)
                    free(current_room->name);
                current_room->name = strdup(line);
            }

            /* Enviroment not set? Update automatically. */
            if (current_room->room_type == null_room_type)
                send_to_server("survey\r\n");

			/* Indoors not set? Update automatically. */
            if (current_room && current_room->indoors == 2)
               if(current_room){
                  current_room->indoors = indoors;
               }

        } else if (mode == FOLLOWING) {
            if (strcmp(line, current_room->name)
                && !(similar && !room_cmp(current_room->name, line))) {
                /* Didn't enter where we expected to? */
                clientf(C_R " (" C_G "Oops, we're lost" C_R ")" C_0);
                current_room = NULL;
                mode = GET_UNLOST;
            }
        }
    }

    if (mode == GET_UNLOST) {
        ROOM_DATA *r, *found = NULL;
        int more = 0;

        for (r = world; r; r = r->next_in_world) {
            if (!strcmp(line, r->name)) {
                if (!found)
                    found = r;
                else {
                    more = 1;
                    break;
                }
            }
        }

        if (found) {
            current_room = found;
            current_area = found->area;
            mode = FOLLOWING;

            get_unlost_exits = more;
        }
    }

    if (mode == CREATING) {
        clientff(C_R " (" C_G "%d" C_R ")" C_0, current_room->vnum);
    } else if (mode == FOLLOWING) {
        if (!disable_areaname) {
            /* Suffix area name to room name */
            if (!godname) {
                int len = strlen(current_room->area->name);
                char buf[len];
                strcpy(buf, current_room->area->name);
                buf[len - 1] = '\0';
                clientff(C_R " (" C_g "%s" C_R "%s)" C_0, buf, get_unlost_exits ? "?" : "");
            } else
                clientff(C_R " (" C_g "%d" C_R "%s)" C_0, current_room->vnum, get_unlost_exits ? "?" : "");
        }
        if(alert_wormhole && current_room->warp &&
                       current_room->warp->link &&
                       current_room->warp->link->to &&
                       current_room->warp->link->to->name &&
                       current_room->warp->link->to->area &&
                       current_room->warp->link->to->area->name){
           char buf[256];
           g_snprintf(buf, sizeof(buf),  C_R "(" C_B "Wormhole to %s (%s)" C_R ")" C_0,
                      current_room->warp->link->to->name,
                      current_room->warp->link->to->area->name);
           clientff("\n\r%s",buf);
        }
    }

    /* We're ready to move. */
    if (mode == FOLLOWING && auto_walk == 1)
        auto_walk = 2;
    /* parsing_room = 2; */
    return 2;
}

int check_for_title(char *line, int start_offset)
{
    char *p;
    int end_offset;

    /* Format:
     *  Title. (road).
     * or
     *  Title. (road)   (GODNAME1)
     */

    /* So... Check for the title name. */

    if (line[0] == '(')
        return -1;

    godname = 0;

    end_offset = 0;
    p = line + start_offset;
    while (*p && *p != '(') {
        if (*p == '(')
            break;

        if ((*p < 'A' || *p > 'Z') && (*p < 'a' || *p > 'z') && *p != ',' && *p != '-' && *p != ' '
            && *p != '.' && *p != '\'' && *p != '\"' && *p != '&')
            return -1;

        if (*p == '.')
            end_offset = p - line + 1;
        p++;
    }

    if (!*p && end_offset == p - line)
        end_offset = 0;

    /* Make sure the rest is valid too. */

    if (*p == '(' && (!strncmp(p, "(path)", 6) || !strncmp(p, "(road)", 6)))
        p += 6;

    while (*p == ' ')
        p++;

    if (*p == '.')
        return end_offset;

    if (*p == '(')
        godname = 1;

    /* Remove the road from: "Title. (road)." *
       if ( l->len > 9 && l->line[l->len-2] == ')' &&
       ( eol = strstr( l->line, ". (" ) ) )
       {
       end_offset = ( eol - l->line ) + 1;
       } */

    return end_offset;
}

void parse_exits()
{
    char buf[256], tempexit[56];
    char *exit = NULL;
    int i;

    strcpy(buf, get_variable("a_roomexits"));

    if (!buf[0])
        return;

    if (mode == CREATING && current_room) {
        for (i = 1; dir_name[i]; i++)
            current_room->detected_exits[i] = 0;
    } else if (get_unlost_exits) {
        for (i = 1; dir_name[i]; i++)
            get_unlost_detected_exits[i] = 0;
    }

    exit = strtok(buf, ",");
    while (exit != NULL) {

        /* Achaea reports "i" and "o", while we want "in" and "out" */
        strcpy(tempexit, (char *) exit);
        if (!strcmp(tempexit, "i"))
            g_snprintf(tempexit, sizeof(tempexit), "in");
        else if (!strcmp(tempexit, "o"))
            g_snprintf(tempexit, sizeof(tempexit), "out");

        if (mode == CREATING && current_room) {
            for (i = 1; dir_small_name[i]; i++) {
                if (!strcmp(dir_small_name[i], tempexit))
                    current_room->detected_exits[i] = 1;
            }
        } else if (get_unlost_exits) {
            for (i = 1; dir_small_name[i]; i++) {
				if (!strcmp(dir_small_name[i], tempexit)){
                    get_unlost_detected_exits[i] = 1;
				}
            }
        }
        exit = strtok(NULL, ",");
    }

}

void parse_room(LINES * l)
{
    char *title = get_variable("a_roomname");

    /* ATCP value doesn't end with a dot. If there was one already, it means the room title didn't change. */
    if (title[strlen(title) - 1] != '.') {

        strcat(title, ".");

        int title_offset = 0;
        if (!strncmp(title, "In the trees above ", 19))
            title_offset = 19;
        else if (!strncmp(title, "Flying above ", 13))
            title_offset = 13;

        int i, offset = 1;
        for (i = 1; i <= l->nr_of_lines; i++) {
            if (!strcmp(title, l->line[i])) {
                break;
            }
            offset++;
        }

        char *p = NULL;

        if(p = strrchr(title, '(')){
            p--;
            *p = '.';
            p++;
            *p = 0;
            indoors = 1;
        }else{
            indoors = 0;
        }

        /* Small safeguard against the odd chance of not finding the room title in paragraph */
        if (i <= l->nr_of_lines) {
            set_line(offset);
            if (title + title_offset){
                parse_title(title + title_offset);
                }
        }
    }

    parse_exits();

}

ROOM_TYPE *add_room_type(char *name, char *color, char *color2)
{
    ROOM_TYPE *type;

    for (type = room_types; type; type = type->next)
        if (!strcmp(name, type->name))
            break;

    /* New? Create one more. */
    if (!type) {
        ROOM_TYPE *t;

        type = calloc(1, sizeof(ROOM_TYPE));
        type->name = strdup(name);
        type->next = NULL;

        if (!room_types)
            room_types = type;
        else {
            for (t = room_types; t->next; t = t->next);
            t->next = type;
        }
    }

    if (!type->color || (color && strcmp(type->color, color)))
        type->color = color;
    if (!type->color2 || (color2 && strcmp(type->color2, color2)))
        type->color2 = color2;

    return type;
}

void check_area(char *name)
{
    AREA_DATA *area;

    if (!strcmp(name, current_room->area->name))
        return;

    /* Area doesn't match. What shall we do? */

    if (mode == FOLLOWING && auto_bump) {
        clientf(C_R " (" C_W "doesn't match!" C_R ")" C_0);
        auto_bump = 0;
    }

    if (mode != CREATING || !current_room)
        return;

    /* Check if the user wants to update an existing one. */

    if (update_area_from_survey || !strcmp(current_room->area->name, "New area")) {
        update_area_from_survey = 0;

        for (area = areas; area; area = area->next)
            if (!strcmp(area->name, name)) {
                clientf(C_R " (" C_W "warning: duplicate!" C_R ")" C_0);
                break;
            }

        area = current_room->area;
        if (area->name)
            free(area->name);
        area->name = strdup(name);
        clientf(C_R " (" C_G "area name updated" C_R ")" C_0);

        return;
    }

    /* First, check if the area exists. If it does, switch it. */

    for (area = areas; area; area = area->next)
        if (!strcmp(area->name, name)) {
            unlink_from_area(current_room);
            link_to_area(current_room, area);
            current_room->area = area;
            current_area = area;
            clientf(C_R " (" C_G "switched!" C_R ")" C_0);

            return;
        }

    /* If not, create one. */

    area = create_area();
    area->name = strdup(name);
    unlink_from_area(current_room);
    link_to_area(current_room, area);
    current_room->area = area;
    current_area = area;
    clientff(C_R " (" C_G "new area, \"%s\" created" C_R ")" C_0, area->name);
}

void parse_survey(char *line)
{
    char buf[256], *p;
    DEBUG("parse_survey");

    if ((mode == CREATING || auto_bump) && current_room) {
        if (!cmp("You discern that you are in *", line)) {
            p = buf;

            extract_wildcard(0, buf, 256);

            /* Might not have a "the" in there, rare cases though. */
            if (!strncmp(p, "the ", 4))
                p += 4;

            check_area(p);
        } else if (!cmp("Your environment conforms to that of *.", line)) {
            ROOM_TYPE *type;

            extract_wildcard(0, buf, 256);

            for (type = room_types; type; type = type->next)
                if (!strcmp(buf, type->name))
                    break;

            /* Lusternia - lowercase letters. - Del
               if ( !type )
               {
               char buf[256];

               for ( type = room_types; type; type = type->next )
               {
               strcpy( buf, type->name );

               buf[0] -= ( 'A' - 'a' );

               if ( !strncmp( line, buf, strlen( buf ) ) )
               break;
               }
               } */

            if (type != current_room->room_type) {
                if (mode == CREATING) {
                    if (type)
                        clientf(C_R " (" C_G "updated" C_R ")" C_0);
                    else {
                        type = add_room_type(buf, NULL, NULL);
                        clientf(C_R " (" C_G "new type created" C_R ")" C_0);
                        clientf(C_R
                                "\r\n[Don't forget to use 'room env' to properly set this environment!]" C_0);
                    }

                    current_room->room_type = type;
                } else if (auto_bump) {
                    clientf(C_R " (" C_W "doesn't match!" C_R ")" C_0);
                    auto_bump = 0;
                }
            }
        } else if (!strcmp("You cannot glean any information about your surroundings.", line)) {
            ROOM_TYPE *type;

            for (type = room_types; type; type = type->next) {
                if (!strcmp("Undefined", type->name)) {
                    if (type != current_room->room_type) {
                        if (mode == CREATING) {
                            current_room->room_type = type;

                            clientf(C_R " (" C_G "updated" C_R ")" C_0);
                        } else if (auto_bump) {
                            clientf(C_R " (" C_W "doesn't match!" C_R ")" C_0);
                            auto_bump = 0;
                        }
                    }

                    break;
                }
            }
        }
    }
}

void parse_stars(char *line)
{
    DEBUG("parse_stars");

    if(!current_room)
        return;

	if(!strncmp("The wings shoot you quickly towards the ceiling, stopping just short of impact,", line, 79)){
		if(current_room->indoors!=1){
			current_room->indoors = 1;
			clientf(C_R " (" C_G "updated" C_R ")" C_0);
		}
	}
	if(!strncmp("A shimmering orb covers the city, preventing you from rising to the skies.", line, 74)){
		if(current_room->indoors!=4){
			ROOM_DATA *room;
			for(room = current_room->area->rooms;room;room=room->next_in_area){
				room->indoors = 4;
			}
			clientf(C_R " (" C_G "updated" C_R ")" C_0);
		}
	}
	if(!strncmp("Your wings cannot carry you over continents or planes.", line, 54)){
		if(current_room->indoors!=3){
			ROOM_DATA *room;
			for(room = current_room->area->rooms;room;room=room->next_in_area){
				room->indoors = 3;
			}
			clientf(C_R " (" C_G "updated" C_R ")" C_0);
		}
	}
}

int can_dash(ROOM_DATA * room)
{
    int dir;
    int number = 0;

    if (!dash_command)
        return 0;

    dir = room->pf_direction;

    /* First of all, check if we have more than one room. 
    if (dir <= 0 || !room->exits[dir] || room->exits[dir]->pf_direction != dir)
        return 0;*/

    /* Now check if we are in the Wilderness. */
    if (room->area && !strcmp(room->area->name,"The Wilderness."))
        return 0;

    /* Now check if we have a clear way, till the end. */
    while (room && (number < 11)) {
        if (room->pf_direction != dir && room->exits[dir])
            return 0;
        if (room->exits[dir] && room->exits[dir]->room_type->must_swim && !disable_swimming )
            return 0;
        number++;
        room = room->exits[dir];
    }

    if(number < (4 + celerity))
        return 0;

    return 1;
}

void go_next()
{
    EXIT_DATA *spexit;
    char buf[256];

    if (!current_room) {
        clientf(C_R "[I don't know where you are at the moment.]" C_0);
        auto_walk = 0;
    } else if (!current_room->pf_parent) {
        auto_walk = 0;
		dash_command = 0;
        clientff(C_R "(" C_G "Done." C_R ") " C_0);
    } else {
        if (current_room->pf_direction != -1) {
            if (door_closed) {
                g_snprintf(buf, sizeof(buf), "open %s", dir_name[current_room->pf_direction]);
                clientfv(buf);
                g_snprintf(buf, sizeof(buf), "open door %s\r\n", dir_small_name[current_room->pf_direction]);
                send_to_server(buf);
            } else if (door_locked) {
                door_locked = 0;
                door_closed = 0;
                auto_walk = 0;
                clientff(C_R "\r\n[Locked room " C_W "%s" C_R " of v" C_W "%d" C_R
                         ". Speedwalking disabled.]\r\n" C_0, dir_name[current_room->pf_direction],
                         current_room->vnum);
                show_prompt();
                return;
            } else {
                if (!gag_next_prompt)
                    clientff(C_R "(" C_r "%d - " C_D " %s" C_R ") " C_0, current_room->pf_cost - 1,
                             dir_name[current_room->pf_direction]);

                if (must_swim(current_room, current_room->pf_parent))
                    send_to_server("swim ");

                if (!must_swim(current_room, current_room->pf_parent) && dash_command
                    && can_dash(current_room))
                    send_to_server(dash_command);
                else {
                    if (mode == FOLLOWING || mode == CREATING)
                        add_queue(current_room->pf_direction);
                }

                send_to_server(dir_small_name[current_room->pf_direction]);
                send_to_server("\r\n");
            }
        } else {
			for (spexit = global_special_exits; spexit; spexit = spexit->next) {
                if (spexit->to == current_room->pf_parent && spexit->command) {
                    clientff(C_R "(" C_D "%s" C_R ") " C_0, spexit->command);
                    send_to_server(spexit->command);
                    send_to_server("\r\n");
                    break;
                }
            }
            for (spexit = current_room->special_exits; spexit; spexit = spexit->next) {
                if (spexit->to == current_room->pf_parent && spexit->command) {
                    clientff(C_R "(" C_D "%s" C_R ") " C_0, spexit->command);
                    send_to_server(spexit->command);
                    send_to_server("\r\n");
                    break;
                }
            }
        }

        auto_walk = 1;
    }
}

void add_exit_char(char *var, int dir)
{
    const char dir_chars[] = { ' ', '|', '/', '-', '\\', '|', '/', '-', '\\' };

    if ((*var == '|' && dir_chars[dir] == '-') || (*var == '-' && dir_chars[dir] == '|'))
        *var = '+';
    else if ((*var == '/' && dir_chars[dir] == '\\') || (*var == '\\' && dir_chars[dir] == '/'))
        *var = 'X';
    else
        *var = dir_chars[dir];
}

void fill_map(ROOM_DATA * room, AREA_DATA * area, int x, int y)
{
    static int xi[] = { 2, 0, 1, 1, 1, 0, -1, -1, -1, 2, 2, 2, 2, 2 };
    static int yi[] = { 2, -1, -1, 0, 1, 1, 1, 0, -1, 2, 2, 2, 2, 2 };
    int i;

    if (room->area != area)
        return;

    if (room->mapped)
        return;

    if (x < 0 || x >= MAP_X + 1 || y < 0 || y >= MAP_Y + 1)
        return;

    room->mapped = 1;
    /* We'll have to clean all these room->mapped too. */
    room->area->needs_cleaning = 1;

    if (map[x][y])
        return;

    map[x][y] = room;

    for (i = 1; dir_name[i]; i++) {
        if (!room->exits[i])
            continue;

        if (room->exit_stops_mapping[i])
            continue;

        if (room->exit_length[i] > 0 && !room->use_exit_instead[i]) {
            int j;
            int real_x, real_y;

            for (j = 1; j <= room->exit_length[i]; j++) {
                real_x = x + (xi[i] * j);
                real_y = y + (yi[i] * j);

                if (real_x < 0 || real_x >= MAP_X + 1 || real_y < 0 || real_y >= MAP_Y + 1)
                    continue;

                add_exit_char(&extra_dir_map[real_x][real_y], i);
            }
        }

        if (room->use_exit_instead[i]) {
            int ex = room->use_exit_instead[i];
            fill_map(room->exits[i], area, x + (xi[ex] * (1 + room->exit_length[i])),
                     y + (yi[ex] * (1 + room->exit_length[i])));
        } else if (xi[i] != 2)
            fill_map(room->exits[i], area, x + (xi[i] * (1 + room->exit_length[i])),
                     y + (yi[i] * (1 + room->exit_length[i])));
    }
}

/* One little nice thing. */
int has_exit(ROOM_DATA * room, int direction)
{
    if (room) {
        if (room->exits[direction]) {
            if (room->exit_stops_mapping[direction])
                return 4;
            else if (room->area != room->exits[direction]->area)
                return 3;
            else if (room->pf_highlight && room->pf_direction == direction)
                return 5;
            else
                return 1;
        } else if (room->detected_exits[direction])
            return 2;
    }

    return 0;
}

/*
 *
 * [ ]- [ ]- [ ]
 *  | \  | /
 *
 * [ ]- [*]- [ ]
 *  | /  | \
 *
 * [ ]  [ ]  [ ]
 *
 */

void set_exit(short *ex, ROOM_DATA * room, int dir)
{
    /* Careful. These are |=, not != */

    if (room->exits[dir]) {
        *ex |= EXIT_NORMAL;

        if (room->exits[dir]->area != room->area)
            *ex |= EXIT_OTHER_AREA;
    } else if (room->detected_exits[dir])
        *ex |= EXIT_UNLINKED;

    if (room->locked_exits[dir])
        *ex |= EXIT_LOCKED;

    if (room->exit_stops_mapping[dir])
        *ex |= EXIT_STOPPING;

    if (room->pf_highlight && room->pf_parent == room->exits[dir])
        *ex |= EXIT_PATH;
}

void set_all_exits(ROOM_DATA * room, int x, int y)
{
    int i;

    /* East, southeast, south. (current element) */
    set_exit(&map_new[x][y].e, room, EX_E);
    set_exit(&map_new[x][y].se, room, EX_SE);
    set_exit(&map_new[x][y].s, room, EX_S);

    /* Northwest. (northwest element) */
    if (x && y) {
        set_exit(&map_new[x - 1][y - 1].se, room, EX_NW);
    }

    /* North, northeast. (north element) */
    if (y) {
        set_exit(&map_new[x][y - 1].s, room, EX_N);
        set_exit(&map_new[x][y - 1].se_rev, room, EX_NE);
    }

    /* West, southwest. (west element) */
    if (x) {
        set_exit(&map_new[x - 1][y].e, room, EX_W);
        set_exit(&map_new[x - 1][y].se_rev, room, EX_SW);
    }

    /* In, out. */
	map_new[x][y].In |= (room->exits[EX_IN]) ? 1 : 0;
	map_new[x][y].Out |= (room->exits[EX_OUT]) ? 1 : 0;
 //   map_new[x][y].In_out |= (room->exits[EX_IN] || room->exits[EX_OUT]) ? 1 : 0;
    map_new[x][y].up |= (room->exits[EX_U]) ? 1 : 0;
    map_new[x][y].down |= (room->exits[EX_D]) ? 1 : 0;

    /* Check for unlinked exits here, and set a warning if needed. */
    if (mode == CREATING)
        for (i = 1; dir_name[i]; i++)
            if (room->detected_exits[i] && !room->exits[i] && !room->locked_exits[i]) {
                map_new[x][y].warn = 2;
                warn_unlinked = 1;
            }
}

/* Remake of fill_map. */
void fill_map_new(ROOM_DATA * room, int x, int y)
{
    const int xi[] = { 2, 0, 1, 1, 1, 0, -1, -1, -1, 2, 2, 2, 2, 2 };
    const int yi[] = { 2, -1, -1, 0, 1, 1, 1, 0, -1, 2, 2, 2, 2, 2 };
    int i;

    if (map_new[x][y].room || room->mapped)
        return;

    if (x < 0 || x >= MAP_X || y < 0 || y >= MAP_Y)
        return;

    room->mapped = 1;
    room->area->needs_cleaning = 1;
    map_new[x][y].room = room;
    map_new[x][y].color = room->room_type->color;
    map_new[x][y].color2 = room->room_type->color2;
    /* Unset? Make it obvious. Don't work over unset background colors, those are fine. */
    if (mode == CREATING && (!map_new[x][y].color || room->room_type == null_room_type)) {
        warn_environment = 1;
        map_new[x][y].warn = 1;
    }

    if (!map_new[x][y].color2)
        map_new[x][y].color2 = C_0;

    if (!map_new[x][y].color)
        map_new[x][y].color = C_R;

    set_all_exits(room, x, y);

    for (i = 1; dir_name[i]; i++) {
        if (!room->exits[i] || room->exit_stops_mapping[i]
            || ((room->exits[i]->area != room->area) && !room->exit_joins_areas[i]))
            continue;

        /* Normal exit. */
        if (!room->use_exit_instead[i] && !room->exit_length[i]) {
            if (xi[i] != 2 && yi[i] != 2)
                fill_map_new(room->exits[i], x + xi[i], y + yi[i]);
            continue;
        }

        /* Exit changed. */
        {
            int new_i, new_x, new_y;

            new_i = room->use_exit_instead[i] ? room->use_exit_instead[i] : i;

            if (xi[new_i] != 2 && yi[new_i] != 2) {
                int j;

                new_x = x + xi[new_i] * (1 + room->exit_length[i]);
                new_y = y + yi[new_i] * (1 + room->exit_length[i]);

                fill_map_new(room->exits[i], new_x, new_y);

                for (j = 1; j <= room->exit_length[i]; j++) {
                    new_x = x + (xi[new_i] * j);
                    new_y = y + (yi[new_i] * j);

                    if (new_x < 0 || new_x >= MAP_X || new_y < 0 || new_y >= MAP_Y)
                        break;

                    /*
                     * This exit will be displayed instead of the center
                     * of a room.
                     */

                    if (i == EX_N || i == EX_S)
                        map_new[new_x][new_y].extra_s = 1;

                    else if (i == EX_E || i == EX_W)
                        map_new[new_x][new_y].extra_e = 1;

                    else if (i == EX_SE || i == EX_NW)
                        map_new[new_x][new_y].extra_se = 1;

                    else if (i == EX_NE || i == EX_SW)
                        map_new[new_x][new_y].extra_se_rev = 1;
                }
            }
        }
    }
}

/* Total remake of show_map. */
/* All calls to strcat/sprintf have been replaced with byte by byte
 * processing. The result has been a dramatical increase in speed. */

void show_map_new(ROOM_DATA * room)
{
    AREA_DATA *a;
    ROOM_DATA *r;
    ELEMENT *tag;
    char map_buf[65536], buf[512], *p, *s, *s2;
    char vnum_buf[1024];
    int x, y, len, len2, loop, i;
    int use_mxp = 0;

    DEBUG("show_map_new");

    if (!room)
        return;

    //get_timer( );

    for (x = 0; x < MAP_X; x++)
        for (y = 0; y < MAP_Y; y++)
            memset(&map_new[x][y], 0, sizeof(MAP_ELEMENT));

    /* Pathfinder - Go around and set which rooms to highlight. */
    for (r = room->area->rooms; r; r = r->next_in_area)
        r->pf_highlight = 0;

    if (room->pf_parent) {
        for (r = room, loop = 0; r->pf_parent && loop < 500; r = r->pf_parent, loop++)
            r->pf_highlight = 1;
    }

    warn_misaligned = 0;
    warn_environment = 0;
    warn_unlinked = 0;

    /* From the current *room, place all other rooms on the map. */
    fill_map_new(room, MAP_X / 2, MAP_Y / 2);

    /* Build it up. */

    /* Are we able to get a SECURE mode on MXP? */
    if (!disable_mxp_map && mxp_tag(TAG_LOCK_SECURE))
        use_mxp = 1;

    /* Upper banner. */

    map_buf[0] = 0;
    p = map_buf;

    g_snprintf(vnum_buf, sizeof(vnum_buf), "v%d", room->vnum);

    s = C_0 "(--" C_C;
    while (*s)
        *(p++) = *(s++);

    s = room->area->name, len = 0;
    while (*s)
        *(p++) = *(s++), len++;

    s = C_0;
    while (*s)
        *(p++) = *(s++);

    len2 = MAP_X * 4 - 7 - strlen(vnum_buf);
    for (x = len; x < len2; x++)
        *(p++) = '-';
    s = C_C;
    while (*s)
        *(p++) = *(s++);

    s = vnum_buf;
    while (*s)
        *(p++) = *(s++);
    s = C_0 "--)\r\n";
    while (*s)
        *(p++) = *(s++);
    *p = 0;

    /* The map, row by row. */
    for (y = 0; y < MAP_Y; y++) {
        /* (1) [o]- */
        /* (2)  | \ */

        /* (1) */

        if (y) {
            *(p++) = ' ';

            for (x = 0; x < MAP_X; x++) {
                if (x) {
                    if (map_new[x][y].room) {

                        if (!map_new[x][y].room->underwater)
                            s = map_new[x][y].color;
                        else
                            s = C_C;
                            if(highlight_indoors){
                                if(map_new[x][y].room->indoors){
                                    s = C_R;
                                }else{
                                    s = C_G;
                                }
                            }
                        while (*s)
                            *(p++) = *(s++);

                        if (strcmp(map_new[x][y].color2, C_0))
                            s = map_new[x][y].color2;

                        while (*s)
                            *(p++) = *(s++);

                        if (map_new[x][y].room == current_room) {
                            s = C_W "(";
                            while (*s)
                                *(p++) = *(s++);
                        } else if ((map_new[x][y].In || map_new[x][y].Out) && map_new[x][y].up && !map_new[x][y].down) {
                            *(p++) = '^';
                        } else if ((map_new[x][y].In || map_new[x][y].Out) && map_new[x][y].down && !map_new[x][y].up) {
                            *(p++) = 'v';
                        } else if (map_new[x][y].In && !map_new[x][y].Out && map_new[x][y].up && map_new[x][y].down) {
                            *(p++) = '^';
                        } else if (map_new[x][y].Out && !map_new[x][y].In && map_new[x][y].up && map_new[x][y].down) {
                            *(p++) = '^';
                        } else
                            *(p++) = '[';

                        if (use_mxp) {
                            if (map_new[x][y].room->person_here)
                                g_snprintf(vnum_buf, sizeof(vnum_buf), "<mppers v=\"%d\" r=\"%s\" t=\"%s\" p=\"%s\">",
                                        map_new[x][y].room->vnum, map_new[x][y].room->name,
                                        map_new[x][y].room->room_type->name, map_new[x][y].room->person_here);
                            else
                                g_snprintf(vnum_buf, sizeof(vnum_buf), "<mpelm v=\"%d\" r=\"%s\" t=\"%s\">",
                                        map_new[x][y].room->vnum, map_new[x][y].room->name,
                                        map_new[x][y].room->room_type->name);
                            s = vnum_buf;
                            while (*s)
                                *(p++) = *(s++);
                        }

                        if (strcmp(map_new[x][y].color2, C_0))
                            s = map_new[x][y].color2;

                        while (*s)
                            *(p++) = *(s++);

                        if (map_new[x][y].room == current_room)
                            s = C_B "i";
                        else if (map_new[x][y].warn == 1)
                            s = C_G "!";
                        else if (map_new[x][y].warn == 2)
                            s = C_r "!";
                        else if (map_new[x][y].room->tags) {

                            int found_map_tag = 0;
                            for (tag = map_new[x][y].room->tags; tag; tag = tag->next)
                                for (i = 0; map_tags[i]; i++)
                                    if (!strcmp((char *) tag->p, map_tags[i])) {
                                        s = map_tag_signs[i];
                                        found_map_tag = 1;
                                    }

                            if (!found_map_tag)
                                s = C_g "m";
                        } else if (map_new[x][y].room->person_here)
                            s = C_Y "P";
                        else if (map_new[x][y].room->mark_here)
                            s = C_B "*";
                        else if (map_new[x][y].In && map_new[x][y].Out && map_new[x][y].up && map_new[x][y].down)
                            s = C_g "X";
                        else if (map_new[x][y].In && map_new[x][y].Out)
                            s = C_g "8";
                        else if (map_new[x][y].In)
                            s = C_g "+";
                        else if (map_new[x][y].Out)
                            s = C_g "-";
                        else if (map_new[x][y].up && map_new[x][y].down)
                            s = C_g "=";
                        else if (map_new[x][y].up)
                            s = C_g "^";
                        else if (map_new[x][y].down)
                            s = C_g "v";
                        else if (map_new[x][y].room->wormhole_here)
                            s = C_B "w";
                        else if (use_mxp) {
                            if (strcmp(map_new[x][y].color2, C_0)) {
                                g_snprintf(vnum_buf, sizeof(vnum_buf), "%s%s ",
                                        background_foreground(map_new[x][y].color2), map_new[x][y].color2);
                                s = vnum_buf;
                            } else
                                s = C_d " ";
                        } else {
                            s = " ";
                        }
                        while (*s)
                            *(p++) = *(s++);

                        if (use_mxp) {
                            if (map_new[x][y].room->person_here)
                                s = "</mppers>";
                            else
                                s = "</mpelm>";
                            while (*s)
                                *(p++) = *(s++);
                        }

                        if (!map_new[x][y].room->underwater)
                            s = map_new[x][y].color;
                        else
                            s = C_C;
                            if(highlight_indoors){
                                if(map_new[x][y].room->indoors){
                                    s = C_R;
                                }else{
                                    s = C_G;
                                }
                            }

                        while (*s)
                            *(p++) = *(s++);

                        if (map_new[x][y].room == current_room)
                            s = C_W ")" C_0;
                        else if ((map_new[x][y].In || map_new[x][y].Out) && map_new[x][y].up && !map_new[x][y].down)
                            s = "^" C_0;
                        else if ((map_new[x][y].In || map_new[x][y].Out) && map_new[x][y].down && !map_new[x][y].up)
                            s = "v" C_0;
						else if (map_new[x][y].In && !map_new[x][y].Out && map_new[x][y].up && map_new[x][y].down) 
							s = "v" C_0;
						else if (map_new[x][y].Out && !map_new[x][y].In && map_new[x][y].up && map_new[x][y].down) 
							s = "v" C_0;
                        else
                            s = "]" C_0;

                        while (*s)
                            *(p++) = *(s++);
                    } else {
                        *(p++) = ' ';

                        if (map_new[x][y].extra_e && map_new[x][y].extra_s)
                            *(p++) = '+';
                        else if (map_new[x][y].extra_se && map_new[x][y].extra_se_rev)
                            *(p++) = 'X';
                        else if (map_new[x][y].extra_e)
                            *(p++) = '-';
                        else if (map_new[x][y].extra_s)
                            *(p++) = '|';
                        else if (map_new[x][y].extra_se)
                            *(p++) = '\\';
                        else if (map_new[x][y].extra_se_rev)
                            *(p++) = '/';
                        else
                            *(p++) = ' ';

                        *(p++) = ' ';
                    }
                }

                /* Exit color. */
                if (map_new[x][y].e & EXIT_OTHER_AREA)
                    s = C_W, s2 = C_0;
                else if (map_new[x][y].e & EXIT_STOPPING)
                    s = C_R, s2 = C_0;
                else if (map_new[x][y].e & EXIT_LOCKED)
                    s = C_r, s2 = C_0;
                else if (map_new[x][y].e & EXIT_UNLINKED)
                    s = C_D, s2 = C_0;
                else if (map_new[x][y].e & EXIT_PATH)
                    s = C_B, s2 = C_0;
                else
                    s = "", s2 = "";
                while (*s)
                    *(p++) = *(s++);

                *(p++) = map_new[x][y].e ? '-' : ' ';

                while (*s2)
                    *(p++) = *(s2++);
            }

            *(p++) = '\r';
            *(p++) = '\n';
        }

        /* (2) */

        for (x = 0; x < MAP_X; x++) {
            *(p++) = ' ';
            if (x) {
                /* Exit color. */
                if (map_new[x][y].s & EXIT_OTHER_AREA)
                    s = C_W, s2 = C_0;
                else if (map_new[x][y].s & EXIT_STOPPING)
                    s = C_R, s2 = C_0;
                else if (map_new[x][y].s & EXIT_LOCKED)
                    s = C_r, s2 = C_0;
                else if (map_new[x][y].s & EXIT_UNLINKED)
                    s = C_D, s2 = C_0;
                else if (map_new[x][y].s & EXIT_PATH)
                    s = C_B, s2 = C_0;
                else
                    s = "", s2 = "";
                while (*s)
                    *(p++) = *(s++);

                *(p++) = map_new[x][y].s ? '|' : ' ';

                while (*s2)
                    *(p++) = *(s2++);

                *(p++) = ' ';
            }

            /* Exit color. */
            if ((map_new[x][y].se | map_new[x][y].se_rev) & EXIT_OTHER_AREA)
                s = C_W, s2 = C_0;
            else if ((map_new[x][y].se | map_new[x][y].se_rev) & EXIT_STOPPING)
                s = C_R, s2 = C_0;
            else if ((map_new[x][y].se | map_new[x][y].se_rev) & EXIT_LOCKED)
                s = C_r, s2 = C_0;
            else if ((map_new[x][y].se | map_new[x][y].se_rev) & EXIT_UNLINKED)
                s = C_D, s2 = C_0;
            else if ((map_new[x][y].se | map_new[x][y].se_rev) & EXIT_PATH)
                s = C_B, s2 = C_0;
            else
                s = "", s2 = "";
            while (*s)
                *(p++) = *(s++);

            *(p++) =
                (map_new[x][y].se ? (map_new[x][y].se_rev ? 'X' : '\\') : map_new[x][y].se_rev ? '/' : ' ');

            while (*s2)
                *(p++) = *(s2++);
        }

        *(p++) = '\r';
        *(p++) = '\n';
    }

    /* Lower banner. */
    s = "(--";
    while (*s)
        *(p++) = *(s++);

    if (use_mxp) {
        if (current_room)
            g_snprintf(buf, sizeof(buf), " %s | ", current_room->name);
        else
            g_snprintf(buf, sizeof(buf), " (we're lost) | ");
        s = buf;
        len = 0;

        while (*s)
            *(p++) = *(s++), len++;

        g_snprintf(vnum_buf, sizeof(vnum_buf), "<custom_alias>");
        s = vnum_buf;
        while (*s)
            *(p++) = *(s++);

        g_snprintf(buf, sizeof(buf), C_b_g);
        s = buf;
        while (*s)
            *(p++) = *(s++);

        g_snprintf(buf, sizeof(buf), "%s", custom_alias);
        s = buf;
        while (*s)
            *(p++) = *(s++), len++;

        g_snprintf(vnum_buf, sizeof(vnum_buf), "</custom_alias>");
        s = vnum_buf;
        while (*s)
            *(p++) = *(s++);

        g_snprintf(buf, sizeof(buf), " ");
        s = buf;
        while (*s)
            *(p++) = *(s++), len++;

        g_snprintf(buf, sizeof(buf), C_0);
        s = buf;
        while (*s)
            *(p++) = *(s++);

        //      clientff("%d\r\n", len);

    } else {
        if (current_room)
            g_snprintf(buf, sizeof(buf), " %s | ", current_room->name);
        else
            g_snprintf(buf, sizeof(buf), " (we're lost) | ");
        s = buf, len = 0;
        while (*s)
            *(p++) = *(s++), len++;
    }

    // s = buf;
    // while( *s )
    // *(p++) = *(s++), len++;
    for (x = len; x < MAP_X * 4 - 7 - (mode == CREATING) * 6; x++)
        *(p++) = '-';

    /* Map correctness indicators */
    if (!my_smiley) {
        if (mode == CREATING) {
            s = warn_unlinked ? "-" C_R "l" C_0 : "-" C_C "*" C_0;
            while (*s)
                *(p++) = *(s++);
            s = warn_environment ? "-" C_R "s" C_0 : "-" C_C "*" C_0;
            while (*s)
                *(p++) = *(s++);
            s = warn_misaligned ? "-" C_R "a" C_0 : "-" C_C "*" C_0;
            while (*s)
                *(p++) = *(s++);
        }
    } else {
        if (mode == CREATING) {
            s = warn_unlinked ? "-" C_R ":(" C_0 : "-" C_g ":)" C_0;
            while (*s)
                *(p++) = *(s++);
            s = warn_environment ? "-" C_R ":(" C_0 : "-" C_g ":)" C_0;
            while (*s)
                *(p++) = *(s++);
            s = warn_misaligned ? "-" C_R ":(" C_0 : "-" C_g ":)" C_0;
            while (*s)
                *(p++) = *(s++);
        }
    }

    s = "--)\r\n";
    while (*s)
        *(p++) = *(s++);

    *p = 0;

    /* Clear up our mess. */
    for (a = areas; a; a = a->next)
        if (a->needs_cleaning) {
            for (r = a->rooms; r; r = r->next_in_area)
                r->mapped = 0;
            a->needs_cleaning = 0;
        }

    /* Show it away. */
    clientf(map_buf);

    /* And return MXP to default. */
    if (use_mxp)
        mxp_tag(TAG_DEFAULT);
}

/* This will get the room that would be shown on the map somewhere. */
ROOM_DATA *get_room_at(int dir, int length)
{
    const int xi[] = { 2, 0, 1, 1, 1, 0, -1, -1, -1, 2, 2, 2, 2, 2 };
    const int yi[] = { 2, -1, -1, 0, 1, 1, 1, 0, -1, 2, 2, 2, 2, 2 };
    AREA_DATA *a;
    ROOM_DATA *r;
    int x, y;

    if (!current_room)
        return NULL;

    for (x = 0; x < MAP_X; x++)
        for (y = 0; y < MAP_Y; y++)
            memset(&map_new[x][y], 0, sizeof(MAP_ELEMENT));

    /* Convert the angle/length to a relative position. */
    x = xi[dir], y = yi[dir];
    if (x == 2 || y == 2)
        return NULL;
    x *= length, y *= length;

    /* Convert them from relative to absolute. */
    x = (MAP_X / 2) + x;
    y = (MAP_Y / 2) + y;

    if (x < 0 || y < 0 || x >= MAP_X || y >= MAP_Y)
        return NULL;

    fill_map_new(current_room, MAP_X / 2, MAP_Y / 2);

    /* Clear up our mess. */
    for (a = areas; a; a = a->next)
        if (a->needs_cleaning) {
            for (r = a->rooms; r; r = r->next_in_area)
                r->mapped = 0;
            a->needs_cleaning = 0;
        }

    return map_new[x][y].room;
}

/* One big ugly thing. */
/* It was way too ugly, so it was replaced. */
void show_map(ROOM_DATA * room)
{
    ROOM_DATA *troom;
    char big_buffer[4096];
    char dir_map[MAP_X * 2 + 4][MAP_Y * 2 + 4];
    char *dir_color_map[MAP_X * 2 + 4][MAP_Y * 2 + 4];
    char buf[128], room_buf[128];
    int x, y;
    char *dir_colors[] = { "", "", C_D, C_W, C_R, C_B };
    int d1, d2;
    int loop;

    DEBUG("show_map");

    get_timer();
    get_timer();
//   debugf( "--map--" );

    big_buffer[0] = 0;

//   debugf( "1: %d", get_timer( ) );

    for (troom = room->area->rooms; troom; troom = troom->next_in_area)
        troom->mapped = 0;

    for (x = 0; x < MAP_X + 1; x++)
        for (y = 0; y < MAP_Y + 1; y++)
            map[x][y] = NULL, extra_dir_map[x][y] = ' ';

    for (x = 0; x < MAP_X * 2 + 2; x++)
        for (y = 0; y < MAP_Y * 2 + 2; y++)
            dir_map[x][y] = ' ', dir_color_map[x][y] = "";

    /* Pathfinder - Go around and set which rooms to highlight. */
    for (troom = room->area->rooms; troom; troom = troom->next_in_area)
        troom->pf_highlight = 0;

    if (room->pf_parent) {
        for (troom = room, loop = 0; troom && loop < 500; troom = troom->pf_parent, loop++)
            troom->pf_highlight = 1;
    }
//   debugf( "2: %d", get_timer( ) );

    /* Build rooms. */
    fill_map(room, room->area, (MAP_X + 1) / 2, (MAP_Y + 1) / 2);

//   debugf( "3: %d", get_timer( ) );

    /* Build directions. */
    for (y = 0; y < MAP_Y + 1 - 1; y++) {
        for (x = 0; x < MAP_X + 1 - 1; x++) {
            /* [ ]-  (01) */
            /*  | X  (11) */

            /* d1 and d2 should be: 1-normal, 2-only detected, 3-another area, 4-stopped mapping, 5-pathfinder. */
            dir_map[x * 2][y * 2] = ' ';

            /* South. */
            d1 = map[x][y] ? has_exit(map[x][y], EX_S) : 0;
            d2 = map[x][y + 1] ? has_exit(map[x][y + 1], EX_N) : 0;

            if (d1 || d2)
                dir_map[x * 2][y * 2 + 1] = '|';

            dir_color_map[x * 2][y * 2 + 1] = dir_colors[d1 > d2 ? d1 : d2];

            /* East. */
            d1 = map[x][y] ? has_exit(map[x][y], EX_E) : 0;
            d2 = map[x + 1][y] ? has_exit(map[x + 1][y], EX_W) : 0;

            if (d1 || d2)
                dir_map[x * 2 + 1][y * 2] = '-';

            dir_color_map[x * 2 + 1][y * 2] = dir_colors[d1 > d2 ? d1 : d2];

            /* Southeast. (\) */
            d1 = map[x][y] ? has_exit(map[x][y], EX_SE) : 0;
            d2 = map[x + 1][y + 1] ? has_exit(map[x + 1][y + 1], EX_NW) : 0;

            if (d1 || d2)
                dir_map[x * 2 + 1][y * 2 + 1] = '\\';

            dir_color_map[x * 2 + 1][y * 2 + 1] = dir_colors[d1 > d2 ? d1 : d2];

            /* Southeast. (/) */
            d1 = map[x + 1][y] ? has_exit(map[x + 1][y], EX_SW) : 0;
            d2 = map[x][y + 1] ? has_exit(map[x][y + 1], EX_NE) : 0;

            if (d1 || d2) {
                if (dir_map[x * 2 + 1][y * 2 + 1] == ' ')
                    dir_map[x * 2 + 1][y * 2 + 1] = '/';
                else
                    dir_map[x * 2 + 1][y * 2 + 1] = 'X';
            }

            if (strcmp(dir_color_map[x * 2 + 1][y * 2 + 1], C_D)
                && dir_colors[d1 > d2 ? d1 : d2][0] != 0)
                dir_color_map[x * 2 + 1][y * 2 + 1] = dir_colors[d1 > d2 ? d1 : d2];
        }
    }

//   debugf( "4: %d", get_timer( ) );

    /* And now combine them. */
    strcat(big_buffer, "/--" "\33[1;36m");
    strcat(big_buffer, room->area->name);
    strcat(big_buffer, C_0);
    for (x = 2 + strlen(room->area->name); x < (MAP_X - 2 + 1) * 4 + 1; x++)
        strcat(big_buffer, "-");
    strcat(big_buffer, "\\\r\n");

//   debugf( "5: %d", get_timer( ) );

    for (y = 0; y < MAP_Y - 1 + 1; y++) {
        if (y > 0) {
            /* [o]- */

            /* *[*o*]*- */

            strcat(big_buffer, " ");
            for (x = 0; x < MAP_X - 1 + 1; x++) {
                if (x == 0)
                    g_snprintf(buf, sizeof(buf), "%s%c%s", dir_color_map[x * 2 + 1][y * 2],
                            dir_map[x * 2 + 1][y * 2], dir_color_map[x * 2 + 1][y * 2][0] != 0 ? C_0 : "");
                else {
                    char *color;
                    char *center;
                    char str[2];

                    if (!map[x][y])
                        color = "";
                    else if (map[x][y]->underwater)
                        color = "\33[1;36m";
                    else
                        color = map[x][y]->room_type->color;
                    if (!color) {
                        color = C_R;
                        // warn
                    }

                    if (!map[x][y]) {
                        /* Ugh! */
                        g_snprintf(str, sizeof(str), "%c", extra_dir_map[x][y]);
                        center = str;
                    } else if (map[x][y] == current_room)
                        center = C_B "i";
                    else if (map[x][y]->exits[EX_IN] || map[x][y]->exits[EX_OUT])
                        center = C_r "o";
                    else
                        center = " ";

                    if (map[x][y] == current_room) {
                        g_snprintf(room_buf, sizeof(room_buf), "%s%s%s%s%s", color, map[x][y] ? "(" : " ", center, color,
                                map[x][y] ? ")" C_0 : " ");
                    } else {
                        g_snprintf(room_buf, sizeof(room_buf), "%s%s%s%s%s", color, map[x][y] ? "[" : " ", center, color,
                                map[x][y] ? "]" C_0 : " ");

                    }

                    g_snprintf(buf, sizeof(buf), "%s%s%c%s", room_buf, dir_color_map[x * 2 + 1][y * 2],
                            dir_map[x * 2 + 1][y * 2], dir_color_map[x * 2 + 1][y * 2][0] != 0 ? C_0 : "");
                }
                strcat(big_buffer, buf);
            }
            strcat(big_buffer, "\r\n");
        }
        strcat(big_buffer, " ");

        for (x = 0; x < MAP_X - 1 + 1; x++) {
            /*  | X */

            if (x == 0)
                g_snprintf(buf, sizeof(buf), "%s%c%s", dir_color_map[x * 2 + 1][y * 2 + 1],
                        dir_map[x * 2 + 1][y * 2 + 1],
                        dir_color_map[x * 2 + 1][y * 2 + 1][0] != 0 ? C_0 : "");
            else
                g_snprintf(buf, sizeof(buf), " %s%c%s %s%c%s", dir_color_map[x * 2][y * 2 + 1],
                        dir_map[x * 2][y * 2 + 1],
                        dir_color_map[x * 2][y * 2 + 1][0] != 0 ? C_0 : "",
                        dir_color_map[x * 2 + 1][y * 2 + 1], dir_map[x * 2 + 1][y * 2 + 1],
                        dir_color_map[x * 2 + 1][y * 2 + 1][0] != 0 ? C_0 : "");
            strcat(big_buffer, buf);
        }
        strcat(big_buffer, "\r\n");
    }

//   debugf( "6: %d", get_timer( ) );

    strcat(big_buffer, "\\--");
    g_snprintf(buf, sizeof(buf), " Your map ");
    strcat(big_buffer, buf);
    for (x = 2 + strlen(buf); x < (MAP_X - 2 + 1) * 4 + 1; x++)
        strcat(big_buffer, "-");
    strcat(big_buffer, "/\r\n");

    /* Now let's send this big monster we've just created... */
    clientf(big_buffer);
}

static size_t my_fwrite(void *buffer, size_t size, size_t nmemb, void
                     *stream)
{
    struct FtpFile *out = (struct FtpFile *) stream;
    if (out && !out->stream) {
        /* open file for writing */
        out->stream = fopen(out->filename, "wb");
        if (!out->stream)
            return -1;          /* failure, can't open file to write */
    }
    return fwrite(buffer, size, nmemb, out->stream);
}

int save_progress(void *clientp, double t,      /* dltotal */
                  double d,     /* dlnow */
                  double ultotal, double ulnow)
{
    /* clientff("t:%g, d: %g, (%g %%)\r\n", ultotal,ulnow, d * 100.0 / t); */
    thread.total_filesize = t;
    thread.progress = d;
    if (thread.are_we_uploading)
        thread.progress_percent = (int) (ulnow * 100 / ultotal);
    else
        thread.progress_percent = (int) (d * 100 / t);

    return 0;
}

void finish_download()
{

    if (thread.check_up_on_us == 1) {
        clientff("\r\n" C_W "[" C_B "Map" C_W "]: New map downloaded, reloading ours...\r\n" C_0);
        destroy_map();
        load_map(thread.filename, "none");
        convert_vnum_exits();
        clear_recycle_bin();
        fill_recycle_bin();

        check_map();
        clientff(C_W "[" C_B "Map" C_W "]: Okay, done! You're good to go.\r\n" C_0);
        do_vmap_follow("");
        thread.check_up_on_us = 0;

        /* And download the maplog then */
        if (thread.filename)
            g_free(thread.filename);
        if (thread.url)
            g_free(thread.url);
        thread.filename = g_strdup_printf("maplog");
        thread.url = g_strdup_printf("http://vadisystems.com/maps/maplog");

        g_thread_create(download_file, NULL, FALSE, NULL);

        /* Coming from "vmap update" */
    } else if (thread.check_up_on_us == 2) {
        int latest_map_version = 0;

        FILE *f = fopen(".latest_map_version", "r");
        if (fscanf(f, "%d", &latest_map_version) == EOF) {
            fclose(f);
            return;
        }
        fclose(f);

        /* Just to be on the safe side, we'll be paranoid */
        if (map_version == latest_map_version) {
            clientff("\r\n" C_W "[" C_B "Map" C_W "]: Your map is up to date (version %d)." C_0,
                     latest_map_version);
            show_prompt();
            thread.check_up_on_us = 0;
            return;
        }

        if (g_file_test("IMap.old", G_FILE_TEST_EXISTS)) {
            if (g_unlink("IMap.old")) {
                clientff(C_W "[" C_B "Map" C_W "]: Couldn't remove the backup file. Aborting.\r\n" C_0);
                return;
            }

            /* Backup the map. */
            if (g_rename("IMap", "IMap.old")) {
                clientff(C_W "[" C_B "Map" C_W
                         "]: Couldn't backup the old map for some reason. Aborting.\r\n" C_0);
                return;
            }

        }

        if (thread.filename)
            g_free(thread.filename);
        if (thread.url)
            g_free(thread.url);
        thread.filename = g_strdup_printf("IMap");
        thread.url = g_strdup_printf("http://vadisystems.com/maps/IMap");

        clientff("\r\n" C_W "[" C_B "Map" C_W
                 "]: Downloading the latest map (ours is version %d, latest is %d)." C_0,
                 map_version, latest_map_version);
        show_prompt();

        g_thread_create(download_file, NULL, FALSE, NULL);
    }

}

gpointer download_file(gpointer null)
{
    CURL *curl;
    CURLcode res;

    struct FtpFile ftpfile = {
        thread.filename,        /* name to store the file as if succesful */
        NULL
    };

    thread.downloading = 1;
    thread.download_status = 1;
    thread.are_we_uploading = 0;

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, thread.url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_fwrite);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpfile);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, FALSE);
        curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, save_progress);

        /* Timeout after 15 minutes. */
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 15 * 60);

        /* Switch off full protocol/debug output */
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0);

        res = curl_easy_perform(curl);

        /* always cleanup */
        curl_easy_cleanup(curl);

        if (CURLE_OK != res) {
            /* we failed */

            if (res == CURLE_OPERATION_TIMEDOUT) {
                clientff(C_W "[" C_B "Map" C_W
                         "]: The file (%s) was taking too long to download (more than 15 mins). Aborting." C_0, thread.filename);
            } else {
                clientff(C_W "[" C_B "Map" C_W
                         "]: Curl error (%s) happened. Try downloading the file again?\r\n" C_0,
                         curl_easy_strerror(res));
            }
            thread.download_status = 0;

            /* Restore the backup of the map if we were getting it */

            if (!strcmp(thread.filename, "IMap") && g_file_test("IMap.old", G_FILE_TEST_EXISTS)) {
                if (g_rename("IMap.old", "IMap")) {
                    clientff(C_W "[" C_B "Map" C_W "]: Restoring from the backup map meanwhile...\r\n" C_0);
                                            destroy_map();
                                            clear_recycle_bin();
                                            fill_recycle_bin();
                                            load_map(thread.filename, "none");
                                            convert_vnum_exits();
                                            check_map();
                                            clientff(C_W "[" C_B "Map" C_W "]: Okay, done! You're good to go.\r\n" C_0);
                                            do_vmap_follow("");
                }

            }

        }
    }

    if (ftpfile.stream)
        fclose(ftpfile.stream); /* close the local file */

    curl_global_cleanup();

    thread.downloading = 0, thread.total_filesize = 0, thread.progress = 0, thread.progress_percent = 0;

    if (thread.download_status != 0) {
        if (!strcmp(thread.filename, "IMap")) {
            thread.check_up_on_us = 1;
        } else if (!strcmp(thread.filename, ".latest_map_version")) {
            thread.check_up_on_us = 2;
        } else {
            clientff("\r\n" C_W "[" C_B "Map" C_W "]: Our file (%s) downloaded okay." C_0, thread.filename);
            show_prompt();
            thread.check_up_on_us = 3;
        }

        finish_download();

    }

    return null;
}

static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream)
{
    size_t retcode = fread(ptr, size, nmemb, stream);
    return retcode;
}

gpointer upload_file(gpointer null)
{
    CURL *curl;
    CURLcode res;
    FILE *hd_src;
    struct stat file_info;

    thread.downloading = 1;
    thread.are_we_uploading = 1;

/*
    struct curl_slist *headerlist = NULL;
    char *buf_2 = g_strdup_printf("RNTO IMap-%s", (char *) get_variable("a_name"));
    char *buf_1 = g_strdup_printf("RNFR %s-uploading", (char *) get_variable("a_name"));
*/

    /* get the file size of the local file */
    if (stat(thread.filename, &file_info)) {
        clientff(C_W "[" C_B "Map" C_W
                 "]: Couldnt open '%s' for uploading (%s).\n", thread.filename, strerror(errno));
        return null;
    }

    hd_src = fopen(thread.filename, "rb");

    curl = curl_easy_init();
    if (curl) {
        /* build a list of commands to pass to libcurl */
/*
        headerlist = curl_slist_append(headerlist, buf_1);
        headerlist = curl_slist_append(headerlist, buf_2);
*/

        curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);

        /* enable uploading */
        curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
        curl_easy_setopt(curl, CURLOPT_URL, thread.url);
        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_ANYSAFE);
        curl_easy_setopt(curl, CURLOPT_USERPWD, "incoming@vadisystems.com:test");

        /* pass in that last of FTP commands to run after the transfer */
/*
        curl_easy_setopt(curl, CURLOPT_POSTQUOTE, headerlist);
*/

        /* now specify which file to upload */
        curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, FALSE);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0);
        curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, save_progress);

        curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t) file_info.st_size);

        /* Now run off and do what you've been told! */
        res = curl_easy_perform(curl);

        /* clean up the FTP commands list */
/*
        curl_slist_free_all(headerlist);
*/

        /* always cleanup */
        curl_easy_cleanup(curl);

        thread.downloading = 0, thread.total_filesize = 0, thread.progress = 0, thread.progress_percent = 0;

        fclose(hd_src);         /* close the local file */
        curl_global_cleanup();

        if (CURLE_OK != res) {
            /* we failed */

            if (res == CURLE_OPERATION_TIMEDOUT) {
                clientff(C_W "[" C_B "Map" C_W
                         "]: The file (%s) was taking too long to upload. Aborting." C_0, thread.filename);
            } else {
                clientff(C_W "[" C_B "Map" C_W
                         "]: Curl error (%s) happened. Try uploading the file again?\r\n" C_0,
                         curl_easy_strerror(res));
            }
            thread.download_status = 0;

        } else {
            clientff("\r\n" C_W "[" C_B "Map" C_W "]: Done! The %s was uploaded successfully."
                     C_0, !strcmp(thread.filename, "my-changelog") ? "changelog" : !strcmp(thread.filename,
                                                                                           "IMap") ? "map" :
                     "file");
            show_prompt();

            /* Upload the changelog too, just don't get in a loop */

            if (!strcmp(thread.filename, "IMap")) {
                if (thread.filename)
                    g_free(thread.filename);
                if (thread.url)
                    g_free(thread.url);

                thread.filename = g_strdup_printf("my-changelog");
                thread.url =
                    g_strdup_printf("ftp://vadisystems.com/IMap-changelog-%s",
                                    (char *) get_variable("a_name"));

                clientff("\r\n" C_W "[" C_B "Map" C_W "]: Uploading the changelog..." C_0);
                show_prompt();
                g_thread_create(upload_file, NULL, FALSE, NULL);
            }
        }
    }

    return null;
}

void download(char *arg)
{

    char *area_name, *url;

    if (thread.downloading) {
        clientff(C_W "[" C_B "Map" C_W "]: We're already %s a file (" C_0 "%s" C_W
                 "). Wait until that one finishes up.\r\n",
                 thread.are_we_uploading ? "uploading" : "downloading", thread.filename);
        return;
    }

    area_name = strtok(arg, " ");
    url = strtok(NULL, " ");

    if (!area_name) {
        clientfv("File name missing.");
        return;
    }

    if (!url) {
        clientfv("URL missing.");
        return;
    }

    if (thread.filename)
        g_free(thread.filename);
    if (thread.url)
        g_free(thread.url);
    thread.filename = g_strdup_printf("%s", area_name);
    thread.url = g_strdup_printf("%s", url);

    clientff(C_W "[" C_B "Map" C_W "]: Okay, downloading '%s'.\r\n" C_0, thread.filename);
    show_prompt();

    g_thread_create(download_file, NULL, FALSE, NULL);

}

void do_vmap_update(char *arg)
{

    if (thread.downloading) {
        clientff(C_W "[" C_B "Map" C_W "]: We're already %s a file (" C_0
                 "%s" C_W "). Wait until that one finishes up.\r\n" C_0,
                 thread.are_we_uploading ? "uploading" : "downloading", thread.filename);
        return;
    }

    if (strcmp(arg, "force")) {

        /* Windows 9x compatibility */

        if (g_file_test(".latest_map_version", G_FILE_TEST_EXISTS)) {
            if (g_unlink(".latest_map_version")) {
                clientff(C_W "[" C_B "Map" C_W "]: Couldn't remove the map version file. Aborting the update.\r\n" C_0);
                return;
            }
        }

        if (thread.filename)
            g_free(thread.filename);
        if (thread.url)
            g_free(thread.url);
        thread.filename = g_strdup_printf(".latest_map_version");
        thread.url = g_strdup_printf("http://vadisystems.com/maps/.latest_map_version");

        clientff(C_W "[" C_B "Map" C_W "]: Checking if we got the latest map...\r\n" C_0);
        g_thread_create(download_file, NULL, FALSE, NULL);

        /* Force update? */
    } else {
        if (thread.filename)
            g_free(thread.filename);
        if (thread.url)
            g_free(thread.url);
        thread.filename = g_strdup_printf("IMap");
        thread.url = g_strdup_printf("http://vadisystems.com/maps/IMap");

        clientff(C_W "[" C_B "Map" C_W "]: Force-updating our map.\r\n" C_0);
        show_prompt();

        g_thread_create(download_file, NULL, FALSE, NULL);
    }

}

void do_vmap_upload(char *arg)
{

    FILE *fl;

    if (thread.downloading) {
        clientff(C_W "[" C_B "Map" C_W "]: We're already %s a file (" C_0 "%s" C_W
                 "). Wait until that one finishes up.\r\n",
                 thread.are_we_uploading ? "uploading" : "downloading", thread.filename);
        return;
    }

    /* Handle the changelog from the mapper */
    fl = fopen("my-changelog", "w");

    if (!fl)
        return;

    if (arg[0])
        fprintf(fl, "%s\r\n", arg);
    else {
        clientfv("Please say what changes you made with " C_g "vmap upload <message>" C_W " and upload again."
                 C_0);
        return;
    }

    fclose(fl);

	/* Save the map to the file before uploading */
	do_vmap_save("");

/* Upload the map */
    if (thread.filename)
        g_free(thread.filename);
    if (thread.url)
        g_free(thread.url);

    thread.filename = g_strdup_printf("IMap");
    thread.url = g_strdup_printf("ftp://vadisystems.com/IMap-%s", (char *) get_variable("a_name"));

    clientff(C_W "[" C_B "Map" C_W "]: Uploading the map...\r\n" C_0);
    g_thread_create(upload_file, NULL, FALSE, NULL);

}

void show_floating_map(ROOM_DATA * room)
{
    if ((!floating_map_enabled && !floating_normal_map_enabled) || !room)
        return;

    /* Normal map */
    if (floating_normal_map_enabled) {
        clientff("\r\n");
        show_map_new(room);
        skip_newline_on_map = 0;
    }

    /* Mxp map */
    if (!mxp_tag(TAG_LOCK_SECURE))
        return;

	clientff("\r\n");
    mxp_tag(TAG_LOCK_SECURE);
	mxp("<DEST \"Mapper Window\" EOF></DEST>");
    mxp("<DEST \"Mapper Window\" X=0 Y=0>");
    mxp_tag(TAG_LOCK_SECURE);

    skip_newline_on_map = 1;
    show_map_new(room);
    skip_newline_on_map = 0;

    mxp_tag(TAG_LOCK_SECURE);
    mxp("</DEST>");
    mxp_tag(TAG_DEFAULT);
}

char *get_color(char *name)
{
    int i;

    for (i = 0; color_names[i].name; i++)
        if (!strcmp(name, color_names[i].name))
            return color_names[i].code;

    return NULL;
}

int save_settings(char *file)
{
    ROOM_DATA *r;
	AREA_DATA *a;
    ELEMENT *tag;
    FILE *fl;
    int i;

    fl = fopen(file, "w");

    if (!fl) {
        char buf[256];
        g_snprintf(buf, sizeof(buf), "Can't open '%s' to save your settings: %s.", file, strerror(errno));
        clientfv(buf);
        return 0;
    }

    fprintf(fl, "# Vadi Mapper configuration file.\r\n");

    for (i = 0; color_names[i].name; i++) {
        if (!strcmp(color_names[i].code, room_color)) {
            fprintf(fl, "Title-Color %s\r\n", color_names[i].name);
            break;
        }
    }

    fprintf(fl, "Disable-Swimming %s\r\n", disable_swimming ? "yes" : "no");
    fprintf(fl, "Disable-WhoList %s\r\n", disable_wholist ? "yes" : "no");
    fprintf(fl, "Disable-Alertness %s\r\n", disable_alertness ? "yes" : "no");
    fprintf(fl, "Disable-Locating %s\r\n", disable_locating ? "yes" : "no");
    fprintf(fl, "Disable-AreaName %s\r\n", disable_areaname ? "yes" : "no");
    fprintf(fl, "Automatically-eat-pear %s\r\n", auto_pear ? "yes" : "no");
    fprintf(fl, "Disable-MXPTitle %s\r\n", disable_mxp_title ? "yes" : "no");
    fprintf(fl, "Disable-MXPExits %s\r\n", disable_mxp_exits ? "yes" : "no");
    fprintf(fl, "Disable-MXPMap %s\r\n", disable_mxp_map ? "yes" : "no");
    fprintf(fl, "Disable-AutoLink %s\r\n", disable_autolink ? "yes" : "no");
    fprintf(fl, "Disable-SewerGrates %s\r\n", disable_sewer_grates ? "yes" : "no");
    fprintf(fl, "Disable-Mhaldorian-SewerGrates %s\r\n", disable_mhaldorian_sewer_grates ? "yes" : "no");
    fprintf(fl, "Enable-GodRooms %s\r\n", enable_godrooms ? "yes" : "no");
    fprintf(fl, "Custom-Alias %s\r\n", custom_alias);
    fprintf(fl, "Enable-Smileys %s\r\n", my_smiley ? "yes" : "no");
    fprintf(fl, "Room-display-limit %d\r\n", room_display_limit);
    fprintf(fl, "Auto-update %s\r\n", auto_update ? "yes" : "no");
    fprintf(fl, "Disable-IconTravel %s\r\n", disable_icon_travel ? "yes" : "no");
    fprintf(fl, "Disable-WarpTravel %s\r\n", disable_warp_travel ? "yes" : "no");
    fprintf(fl, "Disable-Duanathar %s\r\n", disable_duanathar ? "yes" : "no");
    fprintf(fl, "Alert-Wormhole %s\r\n", alert_wormhole ? "yes" : "no");
    fprintf(fl, "Scry-Command \"%s\"\r\n", scry_command);
    fprintf(fl, "Celerity %d\r\n", celerity);

    /* Save all room tags. */

    int dont_save = 0;

    fprintf(fl, "\r\n# Room Marks.\r\n\r\n");
    for (r = world; r; r = r->next_in_world) {
        if (r->tags) {
            /* Save them in reverse. */

            for (tag = r->tags; tag->next; tag = tag->next);
            for (; tag; tag = tag->prev) {
                for (i = 0; map_tags[i]; i++) {
                    if (!strcmp((char *) tag->p, map_tags[i]))
                        dont_save = 1;
                }

                if (!dont_save) {
                    fprintf(fl, "Mark %d \"%s\"\r\n", r->vnum, (char *) tag->p);
                } else
                    dont_save = 0;
            }
        }
    }


	/* Save all the disabled areas */

	fprintf(fl, "\r\n# Disabled Areas.\r\n\r\n");
	for(a = world->area; a; a = a->next){

		if(a->disabled){
			char *p, buf[256];
			strcpy(buf, a->name);
			p=strrchr(buf,'.');
			p=0;
			fprintf(fl, "Disable \"%s\"\r\n", buf);
		}

	}

    fflush(fl);
    fclose(fl);

    return 0;
}

int load_settings(char *file)
{
    FILE *fl;
    char line[1024];
    char option[256];
    char value[1024];
    char *p;
    int i;

    fl = fopen(file, "r");

    if (!fl) {
        char buf[256];
        g_snprintf(buf, sizeof(buf), "Can't open '%s' to load the map: %s.", file, strerror(errno));
        clientfv(buf);
        return 1;
    }

    while (1) {
        if (!fgets(line, 1024, fl))
            break;

        if (feof(fl))
            break;

        /* Strip newline. */
        {
            p = line;
            while (*p != '\n' && *p != '\r' && *p)
                p++;

            *p = 0;
        }

        if (line[0] == '#' || line[0] == 0 || line[0] == ' ')
            continue;

        p = get_string(line, option, 256);

        p = get_string(p, value, 1024);

        if (!strcmp(option, "Title-Color") || !strcmp(option, "Title-Colour")) {
            for (i = 0; color_names[i].name; i++) {
                if (!strcmp(value, color_names[i].name)) {
                    room_color = color_names[i].code;
                    room_color_len = color_names[i].length;
                    break;
                }
            }
        }

        else if (!strcmp(option, "Disable-Swimming")) {
            if (!strcmp(value, "yes"))
                disable_swimming = 1;
            else if (!strcmp(value, "no"))
                disable_swimming = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Auto-update")) {
            if (!strcmp(value, "yes"))
                auto_update = 1;
            else if (!strcmp(value, "no"))
                auto_update = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Room-display-limit")) {
            int i = atoi(value);
            if (i != 0)
                room_display_limit = i;
            else {
                debugf("Parse error in file '%s', defaulting to 30.", file);
                room_display_limit = 30;
            }
        }

        else if (!strcmp(option, "Disable-WhoList")) {
            if (!strcmp(value, "yes"))
                disable_wholist = 1;
            else if (!strcmp(value, "no"))
                disable_wholist = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-Alertness")) {
            if (!strcmp(value, "yes"))
                disable_alertness = 1;
            else if (!strcmp(value, "no"))
                disable_alertness = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-Locating")) {
            if (!strcmp(value, "yes"))
                disable_locating = 1;
            else if (!strcmp(value, "no"))
                disable_locating = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-AreaName")) {
            if (!strcmp(value, "yes"))
                disable_areaname = 1;
            else if (!strcmp(value, "no"))
                disable_areaname = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Automatically-eat-pear")) {
            if (!strcmp(value, "yes"))
                auto_pear = 1;
            else if (!strcmp(value, "no"))
                auto_pear = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-MXPTitle")) {
            if (!strcmp(value, "yes"))
                disable_mxp_title = 1;
            else if (!strcmp(value, "no"))
                disable_mxp_title = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-MXPExits")) {
            if (!strcmp(value, "yes"))
                disable_mxp_exits = 1;
            else if (!strcmp(value, "no"))
                disable_mxp_exits = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-MXPMap")) {
            if (!strcmp(value, "yes"))
                disable_mxp_map = 1;
            else if (!strcmp(value, "no"))
                disable_mxp_map = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-AutoLink")) {
            if (!strcmp(value, "yes"))
                disable_autolink = 1;
            else if (!strcmp(value, "no"))
                disable_autolink = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-SewerGrates")) {
            if (!strcmp(value, "yes"))
                disable_sewer_grates = 1;
            else if (!strcmp(value, "no"))
                disable_sewer_grates = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Disable-Mhaldorian-SewerGrates")) {
            if (!strcmp(value, "yes"))
                disable_mhaldorian_sewer_grates = 1;
            else if (!strcmp(value, "no"))
                disable_mhaldorian_sewer_grates = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

        else if (!strcmp(option, "Enable-GodRooms")) {
            if (!strcmp(value, "yes"))
                enable_godrooms = 1;
            else if (!strcmp(value, "no"))
                enable_godrooms = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

		else if (!strcmp(option, "Enable-Smileys")) {
            if (!strcmp(value, "yes"))
                my_smiley = 1;
            else if (!strcmp(value, "no"))
                my_smiley = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

		else if (!strcmp(option, "Custom-Alias")) {
            strcpy(custom_alias, value);
        }

		else if (!strcmp(option, "Disable-IconTravel")) {
            if (!strcmp(value, "yes"))
                disable_icon_travel = 1;
            else if (!strcmp(value, "no"))
                disable_icon_travel = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
        }

		else if (!strcmp(option, "Disable-WarpTravel")) {
            if (!strcmp(value, "yes"))
                disable_warp_travel = 1;
            else if (!strcmp(value, "no"))
                disable_warp_travel = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);
		}

		else if (!strcmp(option, "Disable-Duanathar")) {
            if (!strcmp(value, "yes"))
                disable_duanathar = 1;
            else if (!strcmp(value, "no"))
                disable_duanathar = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);

		}

                else if (!strcmp(option, "Alert-Wormhole")) {
            if (!strcmp(value, "yes"))
                alert_wormhole = 1;
            else if (!strcmp(value, "no"))
                alert_wormhole = 0;
            else
                debugf("Parse error in file '%s', expected 'yes' or 'no', got '%s' instead.", file, value);

		}else if(!strcmp(option, "Scry-Command")){
                    strcpy(scry_command, value);
                }else if (!strcmp(option, "Celerity")){
                    celerity = atoi(value);
                }else if (!strcmp(option, "Land-Mark")) {
            ROOM_DATA *room;
            ELEMENT *tag;
            int vnum = atoi(value);

            if (!vnum)
                debugf("Parse error in file '%s', expected a landmark vnum, got '%s' instead.", file, value);
            else {
                room = get_room(vnum);
                if (!room)
                    debugf("Warning! Unable to landmark room %d, it doesn't exist!", vnum);
                else {
                    /* Make sure it doesn't exist, first. */
                    for (tag = room->tags; tag; tag = tag->next)
                        if (!strcmp("mark", (char *) tag->p))
                            break;

                    if (!tag) {
                        /* Link it. */
                        tag = g_malloc0(sizeof(ELEMENT));
                        tag->p = strdup("mark");
                        link_element(tag, &room->tags);
                    }
                }
            }
        }

        else if (!strcmp(option, "Disable")) {
            AREA_DATA *area;
			char *c;

            if (!value[0])
                debugf("Parse error in file '%s', in a Disable line.", file);
            else {
				c=strchr(value, '.');
                                while(!c){
                                    char v[1024];
                                    p = get_string(p, v, 1024);
                                    strcat(value, " ");
                                    strcat(value, v);
                                    c=strchr(value,'.');
                                }
			        *c=0;
                area = get_area_by_name(value,0);
                if (!area)
                    debugf("Warning! Unable to mark area %s, it doesn't exist!", value);
                else {
                   area->disabled=1;
                }
            }
        }

		else if (!strcmp(option, "Mark")) {
            ROOM_DATA *room;
            ELEMENT *tag;
            char buf[256];
            int vnum;

            vnum = atoi(value);
            get_string(p, buf, 256);

            if (!vnum || !buf[0])
                debugf("Parse error in file '%s', in a Mark line.", file);
            else {
                room = get_room(vnum);
                if (!room)
                    debugf("Warning! Unable to mark room %d, it doesn't exist!", vnum);
                else {
                    /* Make sure it doesn't exist, first. */
                    for (tag = room->tags; tag; tag = tag->next)
                        if (!strcmp(buf, (char *) tag->p))
                            break;

                    if (!tag) {
                        /* Link it. */
                        tag = g_malloc0(sizeof(ELEMENT));
                        tag->p = strdup(buf);
                        link_element(tag, &room->tags);
                    }
                }
            }
        }

        else
            debugf("Parse error in file '%s', unknown option '%s'.", file, option);
    }

    fclose(fl);
    return 0;
}

void clear_recycle_bin()
{

	DEBUG("clear_recycle_bin");
    while (!g_queue_is_empty(recycle_bin)) {
        g_free(g_queue_pop_head(recycle_bin));
    }

    g_queue_clear(recycle_bin);
}

void recycle_room(int i)
{
	DEBUG("recycle_room");
    int *x = g_malloc(sizeof(int));
    *x = i;
    if(!g_queue_find_custom(recycle_bin, (gpointer) x, (GCompareFunc) compare_recycled_rooms))
        g_queue_push_head(recycle_bin, (gpointer) x);
}

void fill_recycle_bin()
{
	DEBUG("fill_recycle_bin");
    int i;

    for (i = 1; i <= last_vnum; i++) {
        if (!get_room(i))
            recycle_room(i);
    }
}

void do_area_save(char *area_name)
{
    EXIT_DATA *spexit;
    ROOM_DATA *room;
    AREA_DATA *area;
    FILE *fl;
    char buf[256];
    int i;

    DEBUG("do_area_save");

    for (area = areas; area; area = area->next) {
        if (!strcmp(area_name, area->name)) {
            break;
        }
    }

    if (!area || strcmp(area_name, area->name)) {
        clientfv("No area to be saved!");
        return;
    }

    fl = fopen(map_file_area, "w");

    if (!fl) {
        g_snprintf(buf, sizeof(buf), "Can't open '%s' to save map: %s.", map_file_area, strerror(errno));
        clientfv(buf);
        return;
    }

    fprintf(fl, "MAPFILE\n\n");

    fprintf(fl, "AREA\n");
    fprintf(fl, "Name: %s\n", area->name);
    fprintf(fl, "\n");

    for (room = area->rooms; room; room = room->next_in_area) {
        fprintf(fl, "ROOM v%d\n", room->vnum);
        fprintf(fl, "Name: %s\n", room->name);
        if (room->room_type != null_room_type)
            fprintf(fl, "Type: %s\n", room->room_type->name);
		fprintf(fl, "Indoors: %d\n", room->indoors);
        if (room->underwater)
            fprintf(fl, "Underwater\n");
        for (i = 1; dir_name[i]; i++) {
            if (room->exits[i]) {
                fprintf(fl, "E: %s %d\n", dir_name[i], room->exits[i]->vnum);
                if (room->exit_length[i])
                    fprintf(fl, "EL: %s %d\n", dir_name[i], room->exit_length[i]);
                if (room->exit_stops_mapping[i])
                    fprintf(fl, "ES: %s %d\n", dir_name[i], room->exit_stops_mapping[i]);
                if (room->use_exit_instead[i] && room->use_exit_instead[i] != i)
                    fprintf(fl, "UE: %s %s\n", dir_name[i], dir_name[room->use_exit_instead[i]]);
                if (room->exit_joins_areas[i])
                    fprintf(fl, "EJ: %s %d\n", dir_name[i], room->exit_joins_areas[i]);
            } else if (room->detected_exits[i]) {
                if (room->locked_exits[i])
                    fprintf(fl, "DEL: %s\n", dir_name[i]);
                else
                    fprintf(fl, "DE: %s\n", dir_name[i]);
            }
        }

        for (spexit = room->special_exits; spexit; spexit = spexit->next) {
            fprintf(fl, "SPE: %d %d \"%s\" \"%s\" %d\n", spexit->vnum, spexit->alias,
                    spexit->command ? spexit->command : "", spexit->message ? spexit->message : "",
                    spexit->cost);
        }

        fprintf(fl, "\n");
    }

    fprintf(fl, "\n\n");

    fprintf(fl, "EOF\n");
    fflush(fl);
    fclose(fl);

    clientff("Saved '%s'.\r\n", area_name);
}

void save_map(char *file)
{
    AREA_DATA *area, *a;
    ROOM_DATA *room, *r;
    EXIT_DATA *spexit;
    ROOM_TYPE *type;
    ELEMENT *tag;
    FILE *fl;
    char buf[256], *color, *background_color;
    int i, j, count = 0, count2 = 0, save = 0;

    DEBUG("save_map");

    fl = fopen(file, "w");

    if (!fl) {
        g_snprintf(buf, sizeof(buf), "Can't open '%s' to save map: %s.", file, strerror(errno));
        clientfv(buf);
        return;
    }

    fprintf(fl, "# This map is licensed under the Creative Commons Attribution Non-commercial Share Alike\n");
    fprintf(fl,
            "# License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/; \n");
    fprintf(fl,
            "# or, (b) send a letter to Creative Commons, 171 2nd Street, Suite 300, San Francisco, California, 94105, USA\n\n");

    /* Gather and record some statistics */

    for (r = world; r; r = r->next_in_world)
        count++;

    fprintf(fl, "# Map statistics:\n# Rooms: %d\n", count);
    count = 0;

    for (a = areas; a; a = a->next) {
        int notype = 0, unlinked = 0;
        count++;

        for (r = a->rooms; r; r = r->next_in_area) {
            if (!notype && r->room_type == null_room_type)
                notype = 1;
            if (!unlinked)
                for (i = 1; dir_name[i]; i++)
                    if (!r->exits[i] && r->detected_exits[i] && !r->locked_exits[i])
                        unlinked = 1;
        }

        if (!(notype || unlinked))
            count2++;
    }

    fprintf(fl, "# Areas: %d\n# Fully mapped areas: %d\n\n", count, count2);

    fprintf(fl, "\n\nMAP-VERSION %d\n", map_version);
    fprintf(fl, "MAPFILE\n\n");

    fprintf(fl,
            "\n\nROOM-TYPES\n\n" "# \"Name\" color none/[background-color] [swim] [underwater] [avoid]\n");

    for (type = room_types; type; type = type->next) {
        /* Find the color's name. */

        if (type->color) {
            for (j = 0; color_names[j].name; j++)
                if (!strcmp(color_names[j].code, type->color))
                    break;
            color = color_names[j].name;
            if (!color[0])
                color = "unset";
        } else
            color = "unset";

        if (type->color2) {
            for (j = 0; color_names[j].name; j++)
                if (!strcmp(color_names[j].code, type->color2))
                    break;
            background_color = color_names[j].name;
            if (!background_color[0])
                background_color = "none";
        } else
            background_color = "none";

        fprintf(fl, "Env: \"%s\" %s %s%s%s%s\n", type->name, color, background_color,
                type->must_swim ? " swim" : "", type->underwater ? " underwater" : "",
                type->avoid ? " avoid" : "");
    }

    fprintf(fl, "\n\nMESSAGES\n\n");

    for (spexit = global_special_exits; spexit; spexit = spexit->next) {
        fprintf(fl, "GSE: %d \"%s\" %s\n", spexit->vnum, spexit->command ? spexit->command : "",
                spexit->message);
    }

    fprintf(fl, "\n\n");

    count = 0;

    /* Now save the areas & rooms */

    for (area = areas; area; area = area->next, count++) {
        fprintf(fl, "AREA\n");
        fprintf(fl, "Name: %s\n", area->name);
        fprintf(fl, "\n");

        for (room = area->rooms; room; room = room->next_in_area) {
            fprintf(fl, "ROOM v%d\n", room->vnum);
            fprintf(fl, "Name: %s\n", room->name);
            if (room->room_type != null_room_type)
                fprintf(fl, "Type: %s\n", room->room_type->name);
			fprintf(fl, "Indoors: %hd\n", room->indoors);
            if (room->underwater)
                fprintf(fl, "Underwater\n");
            for (i = 1; dir_name[i]; i++) {
                if (room->exits[i]) {
                    fprintf(fl, "E: %s %d\n", dir_name[i], room->exits[i]->vnum);
                    if (room->exit_length[i])
                        fprintf(fl, "EL: %s %d\n", dir_name[i], room->exit_length[i]);
                    if (room->exit_stops_mapping[i])
                        fprintf(fl, "ES: %s %d\n", dir_name[i], room->exit_stops_mapping[i]);
                    if (room->use_exit_instead[i] && room->use_exit_instead[i] != i)
                        fprintf(fl, "UE: %s %s\n", dir_name[i], dir_name[room->use_exit_instead[i]]);
                    if (room->exit_joins_areas[i])
                        fprintf(fl, "EJ: %s %d\n", dir_name[i], room->exit_joins_areas[i]);
                } else if (room->detected_exits[i]) {
                    if (room->locked_exits[i])
                        fprintf(fl, "DEL: %s\n", dir_name[i]);
                    else
                        fprintf(fl, "DE: %s\n", dir_name[i]);
                }
            }

            for (spexit = room->special_exits; spexit; spexit = spexit->next) {
                fprintf(fl, "SPE: %d %d \"%s\" \"%s\" %d\n", spexit->vnum, spexit->alias,
                        spexit->command ? spexit->command : "", spexit->message ? spexit->message : "", spexit->cost);
            }

            fprintf(fl, "\n");
        }
        fprintf(fl, "\n\n");
    }

    g_snprintf(buf, sizeof(buf), "%d areas saved.", count);
    clientfv(buf);

    /* Save the marks */
    count = 0, count2 = 0;

    fprintf(fl, "# Map marks.\n");
    for (r = world; r; r = r->next_in_world) {
        if (r->tags) {
            /* Save them in reverse. */

            for (tag = r->tags; tag->next; tag = tag->next);
            for (; tag; tag = tag->prev) {
                for (i = 0; map_tags[i]; i++) {
                    if (!strcmp((char *) tag->p, map_tags[i]))
                        save = 1;
                }

                if (save) {
                    fprintf(fl, "Mark %d \"%s\"\r\n", r->vnum, (char *) tag->p);
                }
                save = 0;
            }
        }
    }

    fprintf(fl, "\r\nEOF\n");

    fclose(fl);
}

void remake_vnum_exits()
{
    ROOM_DATA *room;
    int i;

    for (room = world; room; room = room->next_in_world) {
        for (i = 1; dir_name[i]; i++) {
            if (room->exits[i])
                room->vnum_exits[i] = room->exits[i]->vnum;
        }
    }
}

void write_string(char *string, FILE * fl)
{
    int len = 0;
    size_t result;

    if (!string) {
        result = fwrite(&len, sizeof(int), 1, fl);
        return;
    }

    len = strlen(string) + 1;

    result = fwrite(&len, sizeof(int), 1, fl);

    result = fwrite(string, sizeof(char), len, fl);
}

char *read_string(FILE * fl)
{
    char *string;
    int len;
    size_t result;

    result = fread(&len, sizeof(int), 1, fl);

    if (!len)
        return NULL;

    if (len > 256) {
        debugf("Warning! Attempting to read an overly long string.");
        return NULL;
    }

    string = malloc(len);
    result = fread(string, sizeof(char), len, fl);

    return string;
}

void save_binary_map(char *file)
{
    AREA_DATA *area;
    ROOM_DATA *room;
    EXIT_DATA *spexit;
    ROOM_TYPE *type;
    FILE *fl;
    int nr;
    size_t result;

    DEBUG("save_binary_map");

    fl = fopen(file, "wb");

    if (!fl)
        return;

    remake_vnum_exits();

    /* room envs. */
    /* Format: int, N*ROOM_TYPE, ... */

    /* Count them. */
    for (nr = 0, type = room_types; type; type = type->next)
        nr++;

    result = fwrite(&nr, sizeof(int), 1, fl);
    for (type = room_types; type; type = type->next) {
        result = fwrite(type, sizeof(ROOM_TYPE), 1, fl);
        write_string(type->name, fl);
        write_string(type->color ? type->color : "unset", fl);
    }

    /* Global Special Exits. */
    /* Format: ..., int, N*EXIT_DATA, ... */

    /* Count them. */
    for (nr = 0, spexit = global_special_exits; spexit; spexit = spexit->next)
        nr++;

    result = fwrite(&nr, sizeof(int), 1, fl);
    for (spexit = global_special_exits; spexit; spexit = spexit->next) {
        result = fwrite(spexit, sizeof(EXIT_DATA), 1, fl);
        write_string(spexit->command, fl);
        write_string(spexit->message, fl);
    }

    /* Areas. */
    /* Format: ..., int, N*[AREA_DATA, int, M*[...]]. */

    /* Count them. */
    for (nr = 0, area = areas; area; area = area->next)
        nr++;

    result = fwrite(&nr, sizeof(int), 1, fl);
    for (area = areas; area; area = area->next) {
        result = fwrite(area, sizeof(AREA_DATA), 1, fl);
        write_string(area->name, fl);

        /* Rooms. */
        /* Format: int, M*[ROOM_DATA, int, P*EXIT_DATA] */

        for (nr = 0, room = area->rooms; room; room = room->next_in_area)
            nr++;

        result = fwrite(&nr, sizeof(int), 1, fl);
        for (room = area->rooms; room; room = room->next_in_area) {
            result = fwrite(room, sizeof(ROOM_DATA), 1, fl);
            write_string(room->name, fl);
            if (room->room_type)
                write_string(room->room_type->name, fl);

            /* Special Exits. */

            /* Count them. */
            for (nr = 0, spexit = room->special_exits; spexit; spexit = spexit->next)
                nr++;

            result = fwrite(&nr, sizeof(int), 1, fl);
            for (spexit = room->special_exits; spexit; spexit = spexit->next) {
                result = fwrite(spexit, sizeof(EXIT_DATA), 1, fl);
                write_string(spexit->command, fl);
                write_string(spexit->message, fl);
            }
        }
    }

    result = fwrite("x", sizeof(char), 1, fl);
    fclose(fl);
}

int load_binary_map(char *file)
{
    AREA_DATA area, *a;
    ROOM_DATA room, *r;
    EXIT_DATA spexit, *spe;
    ROOM_TYPE type, *t;
    FILE *fl;
    char check, *type_name;
    int types, areas, rooms, exits;
    int i, j, k;
    size_t result;

    DEBUG("load_binary_map");

    fl = fopen(file, "rb");

    if (!fl)
        return 1;

    /* room envs. */
    result = fread(&types, sizeof(int), 1, fl);
    for (i = 0; i < types; i++) {
        result = fread(&type, sizeof(ROOM_TYPE), 1, fl);

        type.name = read_string(fl);
        type.color = read_string(fl);
        if (!type.name || !type.color) {
            debugf("NULL entries where they shouldn't be!");
            return 1;
        }
        // change me // Really change me
        add_room_type(type.name, type.color, NULL       /*, type.must_swim, type.underwater,
                                                           0 ); //avoid */ );
        if (type.name)
            free(type.name);
        /* Don't free color. */
    }

    /* Global Special Exits. */
    result = fread(&exits, sizeof(int), 1, fl);
    for (i = 0; i < exits; i++) {
        result = fread(&spexit, sizeof(EXIT_DATA), 1, fl);
        spe = create_exit(NULL);

        spe->alias = spexit.alias;
        spe->cost = spexit.cost;
        spe->vnum = spexit.vnum;
        spe->command = read_string(fl);
        spe->message = read_string(fl);
    }

    /* Areas. */
    result = fread(&areas, sizeof(int), 1, fl);
    for (i = 0; i < areas; i++) {
        result = fread(&area, sizeof(AREA_DATA), 1, fl);
        a = create_area();

        a->disabled = area.disabled;
        a->name = read_string(fl);

        current_area = a;

        /* Rooms. */
        result = fread(&rooms, sizeof(int), 1, fl);
        for (j = 0; j < rooms; j++) {
            result = fread(&room, sizeof(ROOM_DATA), 1, fl);

            char error[256];
            r = create_room(room.vnum, error, 256);
            if (!r) {
                clientff("\r\n");
                clientfv(error);
                return 0;
            }

            if (r->vnum > last_vnum)
                last_vnum = r->vnum;

            r->underwater = room.underwater;
            memcpy(r->vnum_exits, room.vnum_exits, sizeof(int) * 13 + sizeof(short) * 13 * 6);
            r->name = read_string(fl);

            if (room.room_type) {
                type_name = read_string(fl);
                for (t = room_types; t; t = t->next)
                    if (!strcmp(type_name, t->name)) {
                        r->room_type = t;
                        break;
                    }
                free(type_name);
            }

            /* Special Exits. */
            result = fread(&exits, sizeof(int), 1, fl);
            for (k = 0; k < exits; k++) {
                result = fread(&spexit, sizeof(EXIT_DATA), 1, fl);
                spe = create_exit(r);

                spe->alias = spexit.alias;
                spe->cost = spexit.cost;
                spe->vnum = spexit.vnum;
                spe->command = read_string(fl);
                spe->message = read_string(fl);
            }
        }
    }

    result = fread(&check, sizeof(char), 1, fl);

    fclose(fl);

    if (check != 'x') {
        debugf("The binary IMap file is corrupted!");
        return 1;
    }

    return 0;
}

int check_map()
{
    DEBUG("check_map");

    return 0;
}

void convert_vnum_exits()
{
    ROOM_DATA *room, *r;
    EXIT_DATA *spexit;
    int i;

    DEBUG("convert_vnum_exits");

	for (spexit = global_special_exits; spexit; spexit = spexit->next) {
        if (spexit->vnum < 0)
            spexit->to = NULL;
        else {
            if (!(spexit->to = get_room(spexit->vnum))) {
                debugf("Can't link global special exit '%s' to %d.", spexit->command, spexit->vnum);
            }
        }
    }

    for (room = world; room; room = room->next_in_world) {
        /* Normal exits. */
        for (i = 1; dir_name[i]; i++) {
            if (room->vnum_exits[i]) {
                r = get_room(room->vnum_exits[i]);

                if (!r) {
                    debugf("Can't link room %d (%s) to %d.", room->vnum, room->name, room->vnum_exits[i]);
                    continue;
                }
                link_rooms(room, i, r);
                room->vnum_exits[i] = 0;
            }
        }


		/* Duanathar. */

		if(!room->indoors){
			for (spexit = global_special_exits; spexit; spexit = spexit->next) {
				if(!strcmp(spexit->command, "'duanathar"))
					link_special_exit(room, spexit, spexit->to);
			}
		}


        /* Special exits. */
        for (spexit = room->special_exits; spexit; spexit = spexit->next) {
            if (spexit->vnum < 0)
                spexit->to = NULL;
            else {
				if (!(spexit->to = get_room(spexit->vnum)))
					debugf("Can't link room %d (%s) to %d. (special exit)", room->vnum, room->name,
						   spexit->vnum);
				else{
					char buf[10];

					strcpy(buf, "worm warp");

					link_special_exit(room, spexit, spexit->to);
					if(spexit->command && !case_strcmp(spexit->command,buf)){
						create_warp_structure(room,spexit->to,spexit);
						if(spexit->to->warp){
							room->warp->reverse = spexit->to->warp;
							spexit->to->warp->reverse = room->warp;
						}
					}
				}
            }
        }
    }

}

int load_map(char *file, char *specific_area)
{
    AREA_DATA *area = NULL;
    ROOM_DATA *room = NULL;
    ROOM_TYPE *type;
    FILE *fl;
    char buf[256];
    char line[256];
    char *eof;
    int section = 0;
    int nr = 0, i;

    DEBUG("load_map");

    fl = fopen(file, "r");

    if (!fl) {
        g_snprintf(buf, sizeof(buf), "Can't open '%s' to load map: %s.", file, strerror(errno));
        clientfv(buf);
        return 1;
    }

    while (1) {
        eof = fgets(line, 256, fl);
        if (feof(fl))
            break;

        if (!strncmp(line, "EOF", 3))
            break;

        nr++;

        if (section == -1 && strncmp(line, "AREA", 4))
            clientff("Skipping line %d.\r\n", nr);

        if (!line[0] || (section == -1 && strncmp(line, "AREA", 4)))
            continue;

        if (!strncmp(line, "AREA", 4)) {
            section = 1;
        }

        else if (!strncmp(line, "ROOM v", 6)) {
            if (section == 0)
                continue;
            int vnum;

            if (!area) {
                debugf("Room out of area. Line %d.", nr);
                return 1;
            }

            vnum = atoi(line + 6);
            char error[256];
            room = create_room(vnum, error, 256);
            if (!room) {
                clientff("\r\n");
                clientfv(error);
                return 0;
            }
            if (vnum > last_vnum)
                last_vnum = vnum;
            section = 2;
        }

        else if (!strncmp(line, "ROOM-TYPES", 10)) {
            section = 3;
        } else if (!strncmp(line, "MAP-VERSION", 11)) {
            sscanf(line, "MAP-VERSION %d", &map_version);
        }

        else if (!strncmp(line, "MESSAGES", 8)) {
            section = 4;
        }

        /* Strip newline. */
        {
            char *p = line;
            while (*p != '\n' && *p != '\r' && *p)
                p++;

            *p = 0;
        }

        if (!strncmp(line, "Name: ", 6)) {
            if (section == 1) {
                // if ( area && area->name )
                // {
                // debugf( "Double name, on area. Line %d. We already have '%s'.", nr, area->name);
                // return 1;
                // }

                //If we're looking for a specific area, skip the room them
                char *a_name = line + 6;
                if (strcmp(specific_area, a_name) && strcmp(specific_area, "none")) {
                    section = -1;
                    continue;
                }
                area = create_area();
                current_area = area;

                area->name = strdup(line + 6);

            } else if (section == 2) {
                if (room->name) {
                    debugf("Double name, on room. Line %d.", nr);
                    return 1;
                }

                room->name = strdup(line + 6);
            }
        }

        else if (!strncmp(line, "Type: ", 6)) {
            /* Type: Road */
            for (type = room_types; type; type = type->next)
                if (!strncmp(line + 6, type->name, strlen(type->name))) {
                    room->room_type = type;
                    break;
                }
        }

		else if (!strncmp(line, "Indoors: ", 9)) {
			/* Indoors: 0 */
            short ind;

            if (section != 2) {
                debugf("Misplaced indoors line. Line %d.", nr);
                return 1;
            }

            sscanf(line, "Indoors: %hd", &ind);
			room->indoors = ind;
        }

        else if (!strncmp(line, "E: ", 3)) {
            /* E: northeast 14 */
            char buf[256];
            int dir;

            if (section != 2) {
                debugf("Misplaced exit line. Line %d.", nr);
                return 1;
            }

            sscanf(line, "E: %s %d", buf, &dir);

            if (!dir) {
                debugf("Syntax error at exit, line %d.", nr);
                return 1;
            }

            for (i = 1; dir_name[i]; i++) {
                if (!strcmp(buf, dir_name[i])) {
                    room->vnum_exits[i] = dir;
                    break;
                }
            }
        }

        else if (!strncmp(line, "DE: ", 4)) {
            /* DE: northeast */
            char buf[256];

            sscanf(line, "DE: %s", buf);

            for (i = 1; dir_name[i]; i++) {
                if (!strcmp(buf, dir_name[i])) {
                    room->detected_exits[i] = 1;
                    break;
                }
            }
        }

        else if (!strncmp(line, "DEL: ", 4)) {
            /* DEL: northeast */
            char buf[256];

            sscanf(line, "DEL: %s", buf);

            for (i = 1; dir_name[i]; i++) {
                if (!strcmp(buf, dir_name[i])) {
                    room->detected_exits[i] = 1;
                    room->locked_exits[i] = 1;
                    break;
                }
            }
        }

        else if (!strncmp(line, "EL: ", 4)) {
            /* EL: northeast 1 */
            char buf[256];
            int l;

            sscanf(line, "EL: %s %d", buf, &l);

            if (l < 1) {
                debugf("Syntax error at exit length, line %d.", nr);
                return 1;
            }

            for (i = 1; dir_name[i]; i++) {
                if (!strcmp(buf, dir_name[i])) {
                    room->exit_length[i] = l;
                    break;
                }
            }
        }

        else if (!strncmp(line, "ES: ", 4)) {
            /* ES: northeast 1 */
            char buf[256];
            int s;

            sscanf(line, "ES: %s %d", buf, &s);

            for (i = 1; dir_name[i]; i++) {
                if (!strcmp(buf, dir_name[i])) {
                    room->exit_stops_mapping[i] = s;
                    break;
                }
            }
        }

        else if (!strncmp(line, "EJ: ", 4)) {
            /* EJ: northeast 1 */
            char buf[256];
            int s;

            sscanf(line, "EJ: %s %d", buf, &s);

            for (i = 1; dir_name[i]; i++) {
                if (!strcmp(buf, dir_name[i])) {
                    room->exit_joins_areas[i] = s;
                    break;
                }
            }
        }

        else if (!strncmp(line, "UE: ", 4)) {
            /* UE in east */
            char buf1[256], buf2[256];
            int j;

            sscanf(line, "UE: %s %s", buf1, buf2);

            for (i = 1; dir_name[i]; i++) {
                if (!strcmp(dir_name[i], buf1)) {
                    for (j = 1; dir_name[j]; j++) {
                        if (!strcmp(dir_name[j], buf2)) {
                            room->use_exit_instead[i] = j;
                            break;
                        }
                    }
                    break;
                }
            }
        }

        else if (!strncmp(line, "SPE: ", 5)) {
            EXIT_DATA *spexit;

            char vnum[1024];
            char alias[1024];
            char cost[1024];
            char cmd[1024];
            char msg[1024];
            char *p = line + 5;

            /* SPE: -1 0 "pull lever" "You pull a lever, and fall below the ground." */

            p = get_string(p, vnum, 1024);
            p = get_string(p, alias, 1024);
            p = get_string(p, cmd, 1024);
            p = get_string(p, msg, 1024);
            p = get_string(p, cost, 1024);


			spexit = create_exit(room);

			if (vnum[0] == '-')
				spexit->vnum = -1;
			else
				spexit->vnum = atoi(vnum);

			if (alias[0])
				spexit->alias = atoi(alias);
			else
				spexit->alias = 0;

			if (cost[0])
				spexit->cost = atoi(cost);
			else
				spexit->cost = 0;

			if (cmd[0])
				spexit->command = strdup(cmd);
			else
				spexit->command = NULL;

			if (msg[0])
				spexit->message = strdup(msg);
			else
				spexit->message = NULL;

                        if (cost[0] && isdigit(cost[0]))
				spexit->cost = atoi(cost);
			else
				spexit->cost = 0;

        }

        else if (!strncmp(line, "Underwater", 10)) {
            if (section != 2) {
                debugf("Wrong section of an Underwater statement.");
                return 1;
            }

            room->underwater = 1;
        }

        /* Deprecated. Here only for backwards compatibility. */
        else if (!strncmp(line, "Marked", 6)) {
            ELEMENT *tag;

            if (section != 2) {
                debugf("Wrong section of a Marked statement.");
                return 1;
            }

            /* Make sure it doesn't exist, first. */
            for (tag = room->tags; tag; tag = tag->next)
                if (!strcmp("tag", (char *) tag->p))
                    break;

            if (!tag) {
                /* Link it. */
                tag = g_malloc0(sizeof(ELEMENT));
                tag->p = strdup(buf);
                link_element(tag, &room->tags);
            }
        }

        /* Old style - here only for backwards compatibility. */
        else if (!strncmp(line, "T: ", 3)) {
            ROOM_TYPE *type;
            char name[512];
            char color_name[512];
            char *color;
            char buf[512];
            char *p = line + 3;
            int cost_in, cost_out;
            int must_swim;
            int underwater;
            int avoid;

            if (section != 3) {
                debugf("Misplaced room env. Line %d.", nr);
                return 1;
            }

            p = get_string(p, name, 512);
            p = get_string(p, color_name, 512);
            p = get_string(p, buf, 512);
            cost_in = atoi(buf);
            p = get_string(p, buf, 512);
            cost_out = atoi(buf);
            p = get_string(p, buf, 512);
            must_swim = buf[0] == 'y' ? 1 : 0;
            p = get_string(p, buf, 512);
            underwater = buf[0] == 'y' ? 1 : 0;

            if (!name[0] || !color_name[0] || !cost_in || !cost_out) {
                debugf("Buggy Room-Type, line %d.", nr);
                return 1;
            }

            color = get_color(color_name);

            if (!color) {
                debugf("Unknown color name, line %d.", nr);
                return 1;
            }

            avoid = (cost_in == 5) ? 1 : 0;

            type = add_room_type(name, color, NULL);
            type->must_swim = must_swim;
            type->underwater = underwater;
            type->avoid = avoid;
        }

        else if (!strncmp(line, "Env: ", 5)) {
            ROOM_TYPE *type;
            char name[512];
            char color_name[512], background_color_name[512];
            char *color;
            char buf[512];
            char *p = line + 5;

            if (section != 3) {
                debugf("Misplaced room env. Line %d.", nr);
                return 1;
            }

            p = get_string(p, name, 512);
            p = get_string(p, color_name, 512);
            p = get_string(p, background_color_name, 512);

            if (!name[0] || !color_name[0]) {
                debugf("Buggy Room-Type (name and/or color missing), line %d.", nr);
                return 1;
            }

            color = get_color(color_name);
            if (!color && strcmp(color_name, "unset")) {
                debugf("Unknown color name, line %d.", nr);
                return 1;
            }

            type = add_room_type(name, color, NULL);

            if (strcmp(background_color_name, "none")) {
                char *background_color;
                background_color = get_color(background_color_name);

                if (!background_color && strcmp(background_color_name, "unset")) {
                    debugf("Unknown background color name, line %d.", nr);
                    return 1;
                }

                type = add_room_type(name, NULL, background_color);

            }

            p = get_string(p, buf, 512);
            while (buf[0]) {
                if (!strcmp(buf, "swim"))
                    type->must_swim = 1;
                if (!strcmp(buf, "underwater"))
                    type->underwater = 1;
                if (!strcmp(buf, "avoid"))
                    type->avoid = 1;

                p = get_string(p, buf, 512);
            }
        }

        else if (!strncmp(line, "GSE: ", 5)) {
            EXIT_DATA *spexit;

            char vnum[1024];
            char cmd[1024];
            char *p = line + 5;

            if (section != 4) {
                debugf("Misplaced global special exit. Line %d.", nr);
                return 1;
            }

            /* GSE: -1 "brazier" "You feel a moment of disorientation as you are summoned." */

            p = get_string(p, vnum, 1024);
            p = get_string(p, cmd, 1024);

            spexit = create_exit(NULL);

            if (vnum[0] == '-')
                spexit->vnum = -1;
            else
                spexit->vnum = atoi(vnum);

            if (cmd[0])
                spexit->command = strdup(cmd);
            else
                spexit->command = NULL;

            if (p[0])
                spexit->message = strdup(p);
            else
                spexit->message = NULL;
        }

        else if (!strncmp(line, "Mark ", 5)) {
            ROOM_DATA *room;
            ELEMENT *tag;
            char buf[256];
            int vnum;
            char *p = line + 5;

            p = get_string(p, buf, 256);
            vnum = atoi(buf);
            get_string(p, buf, 256);

            if (!vnum || !buf[0])
                debugf("Parse error in file '%s', in a Mark line.", file);
            else {
                room = get_room(vnum);
                if (!room)
                    debugf("Warning! Unable to mark room %d, it doesn't exist!", vnum);
                else {
                    /* Make sure it doesn't exist, first. */
                    for (tag = room->tags; tag; tag = tag->next)
                        if (!strcmp(buf, (char *) tag->p))
                            break;

                    if (!tag) {
                        /* Link it. */
                        tag = g_malloc0(sizeof(ELEMENT));
                        tag->p = strdup(buf);
                        link_element(tag, &room->tags);
                    }
                }
            }
        }
    }

    fclose(fl);

    /* Load our map marks and such */
    load_settings("config.mapper.txt");

    return 0;
}

int get_cost(ROOM_DATA * src, ROOM_DATA * dest)
{
    return (src->room_type->must_swim * 2 * !disable_swimming) + src->room_type->avoid * 3 +
        (dest->room_type->must_swim * 2 * !disable_swimming) + dest->room_type->avoid * 3 + 1;
}

void show_backpath(ROOM_DATA * current)
{
    ROOM_DATA *room = NULL;
    EXIT_DATA *spexit;
    int nr = 0;
    int wrap = 0;

    DEBUG("show_backpath");

    if (!current->pf_parent) {
        clientfv("Can't find a path from here.");
        return;
    }

    /* Find out the destination, if we can */
    for (room = current; room && room->pf_parent && nr < 500; room = room->pf_parent);

    if (room && nr < 500) {
        char room_name[strlen(room->name) + 1];
        strcpy(room_name, room->name);
        room_name[strlen(room_name) - 1] = '\0';
        /* Color codes mess up strlen */
        wrap = 25 + strlen(room_name);
        clientff(C_W "[" C_B "Map" C_W "]: The path to " C_D "%s" C_W " is: " C_G, room_name);
    } else {
        wrap = 21;
        clientff(C_W "[" C_B "Map" C_W "]: The path is: " C_G);
    }

    nr = 0;

    for (room = current; room && room->pf_parent && nr < 500; room = room->pf_parent) {
        if (wrap > 70) {
            clientff(C_D ",\r\n" C_G);
            wrap = 0;
        } else if (nr++) {
            clientff(C_D ", " C_G);
            wrap += 2;
        }

        /* We also have special exits. */
        if (room->pf_direction != -1) {
            if (must_swim(room, room->pf_parent))
                clientff(C_B);

            clientff("%s", dir_small_name[room->pf_direction]);
            wrap += strlen(dir_small_name[room->pf_direction]);
        } else {
            /* Find the special exit, leading to the parent. */
            for (spexit = room->special_exits; spexit; spexit = spexit->next) {
                if (spexit->to == room->pf_parent) {
                    if (spexit->command) {
                        clientff("%s", spexit->command);
                        wrap += strlen(spexit->command);
                    } else {
                        clientff("?");
                        wrap++;
                    }

                    break;
                }
            }
        }
    }

    if (room && room->pf_parent && nr == 500) {
        clientff(C_D ", " C_G "...");
    }

    clientff(C_D ".\r\n" C_0);
}

void show_path(ROOM_DATA * current)
{
    ROOM_DATA *room = NULL;
    EXIT_DATA *spexit;
    int nr = 0;
    int wrap = 0;

    DEBUG("show_path");

    if (!current->pf_parent) {
        clientfv("Can't find a path from here.");
        return;
    }

    /* Find out the destination, if we can */
    for (room = current; room && room->pf_parent && nr < 500; room = room->pf_parent);

    if (room && nr < 500) {
        char room_name[strlen(room->name) + 1];
        strcpy(room_name, room->name);
        room_name[strlen(room_name) - 1] = '\0';
        /* Color codes mess up strlen */
        wrap = 25 + strlen(room_name);
        clientff(C_W "[" C_B "Map" C_W "]: The path to " C_D "%s" C_W " is: " C_G, room_name);
    } else {
        wrap = 21;
        clientff(C_W "[" C_B "Map" C_W "]: The path is: " C_G);
    }

    nr = 0;

    for (room = current; room && room->pf_parent && nr < 500; room = room->pf_parent) {
        if (wrap > 70) {
            clientff(C_D ",\r\n" C_G);
            wrap = 0;
        } else if (nr++) {
            clientff(C_D ", " C_G);
            wrap += 2;
        }

        /* We also have special exits. */
        if (room->pf_direction != -1) {
            if (must_swim(room, room->pf_parent))
                clientff(C_B);

            clientff("%s", dir_small_name[room->pf_direction]);
            wrap += strlen(dir_small_name[room->pf_direction]);
        } else {
            /* Find the special exit, leading to the parent. */
            for (spexit = room->special_exits; spexit; spexit = spexit->next) {
                if (spexit->to == room->pf_parent) {
                    if (spexit->command) {
                        clientff("%s", spexit->command);
                        wrap += strlen(spexit->command);
                    } else {
                        clientff("?");
                        wrap++;
                    }

                    break;
                }
            }
        }
    }

    if (room && room->pf_parent && nr == 500) {
        clientff(C_D ", " C_G "...");
    }

    clientff(C_D ".\r\n" C_0);
}

void clean_paths()
{
    ROOM_DATA *room;

    /* Clean up the world. */
    for (room = world; room; room = room->next_in_world)
        room->pf_parent = NULL, room->pf_cost = 0, room->pf_direction = 0;

    for (pf_step = 0; pf_step < MAX_COST; pf_step++)
        pf_queue[pf_step] = NULL;

    pf_step = pf_rooms = 0;
}

int add_to_pathfinder(ROOM_DATA * room, int cost)
{
    ROOM_DATA **queue;

    if (room->pf_cost && room->pf_cost <= pf_step + cost)
        return 0;

	if (room->area->disabled)
		return 0;

    /* Unlink it if it's already somewhere. */
    if (room->pf_cost) {
        if (room->pf_next)
            room->pf_next->pf_prev = room->pf_prev;
        *(room->pf_prev) = room->pf_next, pf_rooms--;
    }

    if (cost < 1 || cost > MAX_COST - 1)
        debugf("FREAKING OUT!");

    room->pf_cost = pf_step + cost;

    /* Now add it to the queue. Wacky, eh? */
    queue = &pf_queue[(pf_step + cost) % MAX_COST];

    room->pf_next = *queue;
    room->pf_prev = queue;
    *queue = room;
    if (room->pf_next)
        room->pf_next->pf_prev = &room->pf_next;

    pf_rooms++;

    return 1;
}

void path_finder_2()
{
    ROOM_DATA *room;
    REVERSE_EXIT *rexit;
	int *hp, my_health, cost;

    DEBUG("path_finder_2");

	hp = get_variable("a_hp");
	my_health = *hp;

    while (pf_rooms) {
        for (room = pf_queue[pf_step % MAX_COST]; room; room = room->pf_next) {
            for (rexit = room->rev_exits; rexit; rexit = rexit->next) {
				if (my_health==0 && rexit->spexit && rexit->spexit->command)
					continue;
                if (disable_sewer_grates && rexit->spexit && rexit->spexit->command
                    && !strcmp(rexit->spexit->command, "enter grate"))
                    continue;
                                if (disable_mhaldorian_sewer_grates && rexit->spexit && rexit->spexit->command
                    && !strcmp(rexit->spexit->command, "enter grate") && !!strcmp(rexit->from->area->name, "Mhaldor."))
                    continue;
				if (disable_duanathar
                                        && rexit->spexit && rexit->spexit->command
                    && !strcmp(rexit->spexit->command, "'duanathar"))
                    continue;
				if (disable_icon_travel && rexit->spexit && rexit->spexit->command
                    && !strcmp(rexit->spexit->command, "touch icon"))
                    continue;
				if (disable_warp_travel && rexit->spexit && rexit->spexit->command
                    && !strcmp(rexit->spexit->command, "worm warp"))
                    continue;

                if (rexit->spexit){
                    cost = rexit->spexit->cost + 1;
                }else{
                    cost = get_cost(rexit->from, room);
		}
                if (add_to_pathfinder(rexit->from, cost)) {
                    rexit->from->pf_direction = rexit->direction;
                    if (!rexit->direction)
                        rexit->from->pf_direction = -1;
                    rexit->from->pf_parent = room;
                }
            }
            pf_rooms--;
        }

        pf_queue[pf_step % MAX_COST] = NULL;
        pf_step++;
    }
}

void i_mapper_mxp_enabled()
{
    floating_map_enabled = 0;

    mxp_tag(TAG_LOCK_SECURE);
    mxp("<!element mpelm '<send \"goto &v;|room look &v;\" "
        "hint=\"&r;|Vnum: &v;|Type: &t;\">' ATT='v r t'>");
    mxp("<!element mppers '<send \"goto &v;|room look &v;|who &p;\" "
        "hint=\"&p; (&r;)|Vnum: &v;|Type: &t;|Player: &p;\">' ATT='v r t p'>");
//   mxp( "<!element RmTitle '<font color=red>' TAG='RoomName'>" );
    mxp("<!ELEMENT custom_alias '<send href=\"%s\">'>", custom_alias);
    //   mxp( "<!element RmTitle '<font color=red>' TAG='RoomName'>" );
//   mxp( "<!element RmExits TAG='RoomExit'>" );
    mxp_tag(TAG_DEFAULT);
}

void i_mapper_module_init_data()
{
    struct stat map, mapbin, mapper;
    int binary;

    DEBUG("i_mapper_init_data");

    if (!g_thread_supported())
        g_thread_init(NULL);

    curl_global_init(CURL_GLOBAL_ALL);

	recycle_bin=NULL;
	recycle_bin = g_queue_new();


    scry_new_regex =
        g_regex_new
        ("^You dip your fingers into the cool water of the bowl, willing it to reveal the location of (\\w+)\\. An image of (.*) appears reflected within the bowl\\, shifting with the rippling water to display (.*)$",
         G_REGEX_OPTIMIZE, G_REGEX_MATCH_ANCHORED, NULL);


    null_room_type = add_room_type("Unknown", get_color("red"), NULL);
    add_room_type("Undefined", get_color("bright-red"), NULL);

    destroy_map();


    /* Check if we have a binary map that is newer than the map and mapper. */


    if (!stat(map_file_bin, &mapbin) && !stat(map_file, &map)
        && (!stat("vadi_mapper.dll", &mapper) || !stat("vadi_mapper.so", &mapper))
        && mapbin.st_mtime > map.st_mtime && mapbin.st_mtime > mapper.st_mtime)
        binary = 1;
    else
        binary = 0;

    get_timer();

    if (binary ? load_binary_map(map_file_bin) : load_map(map_file, "none")) {
        do_vmap_update("");
        mode = NONE;
        return;
    }

    convert_vnum_exits();
    check_map();
    //  debugf( "%sIMap loaded. (%d microseconds)",
    //         binary ? "Binary " : "", get_timer( ) );

    /* Only if one already exists, but is too old. */
    if (!binary && !stat(map_file_bin, &mapbin)) {
        debugf("Generating binary map.");
        save_binary_map(map_file_bin);
    }

    load_settings("config.mapper.txt");

    clear_recycle_bin();
    fill_recycle_bin();

    mode = GET_UNLOST;

    if (auto_update)
        add_timer("t_autoupdate", 10, t_autoupdate, 0, 0, 0);

}

/* Case insensitive versions of strstr and strcmp. */

#define LOW_CASE( a ) ( (a) >= 'A' && (a) <= 'Z' ? (a) - 'A' + 'a' : (a) )

int case_strstr(char *haystack, char *needle)
{
    char *h = haystack, *n = needle;

    while (*h) {
        if (LOW_CASE(*h) == LOW_CASE(*(needle))) {
            n = needle;

            while (1) {
                if (!(*n))
                    return 1;

                if (LOW_CASE(*h) != LOW_CASE(*n))
                    break;
                h++, n++;
            }
        }

        h++;
    }

    return 0;
}

/* Whyte: I don't trust strcasecmp to be too portable. */
int case_strcmp(const char *s1, const char *s2)
{
    while (*s1 && *s2) {
        if (LOW_CASE(*s1) != LOW_CASE(*s2))
            break;

        s1++;
        s2++;
    }

    return (*s1 || *s2) ? 1 : 0;
}


int case_strncmp(const char *s1, const char *s2, int count)
{
    int i = 0;
    while (*s1 && *s2 && count>i) {
        if (LOW_CASE(*s1) != LOW_CASE(*s2))
            break;

        s1++;
        s2++;
	i++;
    }
    return (count>i) ? 1 : 0;
}

void remove_players(TIMER * self)
{
    ROOM_DATA *r;

    for (r = world; r; r = r->next_in_world) {
        if (r->person_here) {
            free(r->person_here);
            r->person_here = NULL;
        }
    }
}

void remove_marks(TIMER * self)
{
    ROOM_DATA *r;

    for (r = world; r; r = r->next_in_world) {
        if (r->mark_here) {
            free(r->mark_here);
            r->mark_here = NULL;
        }
    }
}

void remove_wormholes(TIMER * self)
{
    ROOM_DATA *r;

    for (r = world; r; r = r->next_in_world) {
        if (r->wormhole_here) {
            free(r->wormhole_here);
            r->wormhole_here = NULL;
        }
    }
}

void mark_player(ROOM_DATA * room, char *name)
{
    ROOM_DATA *r;

    if (room->person_here) {
        free(room->person_here);
        room->person_here = NULL;
    }

    if (name) {
        /* Make sure it's nowhere else. */
        for (r = world; r; r = r->next_in_world)
            if (r->person_here && !strcmp(r->person_here, name)) {
                free(r->person_here);
                r->person_here = NULL;
            }

        room->person_here = strdup(name);
    } else
        room->person_here = strdup("Someone");

    add_timer("remove_players", 15, remove_players, 0, 0, 0);

    /* Force an update of the floating map. */
    last_room = NULL;
}

void mark_wormhole(char *roomname)
{
    ROOM_DATA *room, *found = NULL, *found2 = NULL, *found3 = NULL;

    for (room = world; room; room = room->next_in_world) {
        if (!strcmp(roomname, room->name)) {
            if (!found)
                found = room;
            else if (!found2)
                found2 = room;
            else if (!found3)
                found3 = room;
            else {
                break;
            }
        }
    }

    if (found) {
        if (found->wormhole_here) {
            free(found->wormhole_here);
            found->wormhole_here = NULL;
        }
        found->wormhole_here = strdup(found->name);
    }

    if (found2) {
        if (found2->wormhole_here) {
            free(found2->wormhole_here);
            found2->wormhole_here = NULL;
        }
        found2->wormhole_here = strdup(found2->name);
    }

    if (found3) {
        if (found3->wormhole_here) {
            free(found3->wormhole_here);
            found3->wormhole_here = NULL;
        }
        found3->wormhole_here = strdup(found3->name);
    }

    add_timer("remove_wormholes", 5 * 60, remove_wormholes, 0, 0, 0);

    /* Force an update of the floating map. */
    last_room = NULL;
}

void mark_room(ROOM_DATA * room)
{

    if (room->mark_here) {
        free(room->mark_here);
        room->mark_here = NULL;
    }

    room->mark_here = strdup("mark");

    add_timer("remove_marks", 15, remove_marks, 0, 0, 0);

    /* Force an update of the floating map. */
    last_room = NULL;
}

ROOM_DATA *locate_room(char *name, int area, char *player)
{
    ROOM_DATA *room, *found = NULL, *found2 = NULL, *found3 = NULL;
    char buf[256], *p;
    int more = 0;

    DEBUG("locate_room");

    strcpy(buf, name);

	if((p=strstr(buf, " (house)"))){
		*p='.';
		*(p+1)=0;
	}

    for (room = world; room; room = room->next_in_world) {
        if (!case_strcmp(buf, room->name)) {
            if (!found)
                found = room;
            else if (!found2)
                found2 = room;
            else if (!found3)
                found3 = room;
            else {
                more = 1;
                break;
            }
        }
    }
    if (!found) {
       buf[strlen(buf) - 1] = 0;
       strcat(buf, " (road).");
       for (room = world; room; room = room->next_in_world) {
            if (!case_strcmp(buf, room->name)) {
                if (!found)
                   found = room;
             else if (!found2)
                   found2 = room;
             else if (!found3)
                   found3 = room;
                else {
                     more = 1;
                     break;
                }
            }
        }
    }
    if (found) {
        /* Attempt to find out in which side of the arena it is. */
        if (locate_arena && !more && found->area) {
            if (case_strstr(found->area->name, "north")) {
                if (case_strstr(found->area->name, "east"))
                    clientf(C_D " (" C_R "NE" C_D ")" C_0);
                else if (case_strstr(found->area->name, "west"))
                    clientf(C_D " (" C_R "NW" C_D ")" C_0);
                else
                    clientf(C_D " (" C_R "N" C_D ")" C_0);
            } else if (case_strstr(found->area->name, "south")) {
                if (case_strstr(found->area->name, "east"))
                    clientf(C_D " (" C_R "SE" C_D ")" C_0);
                else if (case_strstr(found->area->name, "west"))
                    clientf(C_D " (" C_R "SW" C_D ")" C_0);
                else
                    clientf(C_D " (" C_R "S" C_D ")" C_0);
            } else if (case_strstr(found->area->name, "central")
                       || case_strstr(found->area->name, "palm orchard"))
                clientf(C_D " (" C_R "C" C_D ")" C_0);
            else if (case_strstr(found->area->name, "east"))
                clientf(C_D " (" C_R "E" C_D ")" C_0);
            else if (case_strstr(found->area->name, "west"))
                clientf(C_D " (" C_R "W" C_D ")" C_0);

            locate_arena = 0;
        }

        /* Show the vnum, after the line. */
        /* Just one room */
        if (!found2)
            clientff(" " C_D "(" C_G "%d" C_D "%s)" C_0, found->vnum, more ? C_D ", " C_G "..." C_D : "");
        /* All three */
        else if (found3)
            clientff(" " C_D "(" C_G "%d" C_D ", " C_G "%d" C_D "," C_G "%d" C_D "%s)" C_0,
                     found->vnum, found2->vnum, found3->vnum, more ? C_D ", " C_G "..." C_D : "");
        /* Just two */
        else if (found2)
            clientff(" " C_D "(" C_G "%d" C_D ", " C_G "%d" C_D "%s)" C_0, found->vnum, found2->vnum,
                     more ? C_D ", " C_G "..." C_D : "");
        else
            clientff("?");

        /* A farsight-like thing. */
        if (area && found->area) {
            char buf[256];
            g_snprintf(buf, sizeof(buf), C_W "[" C_B "Map" C_W "]: That room is in '%s'.\r\n" C_0, found->area->name);
            prefix_at(-1, buf);
        }

        /* Mark the room on the map, for a few moments. */
        if (!more && found) {
            mark_player(found, player);
        }

        if(goto_person && found){

            //TODO rethink output

            if(more){
                clientf("More than 3 rooms with that name found, aborting goto.");
            }else if(found2){
                clientfr("WARNING! MORE THAN ONE FITTING ROOM FOUND!");
            }
            if(!more){
                char buffer[10];
                sprintf(buffer, "%d", found->vnum);
                do_goto(buffer);
            }
            goto_person = 0;
        }
    }
    return found;
}

/* Tries to find all room names in one big string with multiple names */
void locate_room_fuzzy(char *name, int area, char *player)
{
    ROOM_DATA *room, *found = NULL, *found2 = NULL, *found3 = NULL;
    char buf[256], *p;
    int more = 0;

    DEBUG("locate_room_fuzzy");

    strcpy(buf, name);

	if((p=strstr(buf, " (house)"))){
		*p='.';
		*(p+1)=0;
	}

    for (room = world; room; room = room->next_in_world) {
        if (!strstr(buf, room->name)) {
            if (!found)
                found = room;
            else if (!found2)
                found2 = room;
            else if (!found3)
                found3 = room;
            else {
                more = 1;
                break;
            }
        }
    }
    if (!found) {
       buf[strlen(buf) - 1] = 0;
       strcat(buf, " (road).");
       for (room = world; room; room = room->next_in_world) {
            if (!strstr(buf, room->name)) {
                if (!found)
                   found = room;
             else if (!found2)
                   found2 = room;
             else if (!found3)
                   found3 = room;
                else {
                     more = 1;
                     break;
                }
            }
        }
    }

    if (found) {
        /* Attempt to find out in which side of the arena it is. */
        if (locate_arena && !more && found->area) {
            if (case_strstr(found->area->name, "north")) {
                if (case_strstr(found->area->name, "east"))
                    clientf(C_D " (" C_R "NE" C_D ")" C_0);
                else if (case_strstr(found->area->name, "west"))
                    clientf(C_D " (" C_R "NW" C_D ")" C_0);
                else
                    clientf(C_D " (" C_R "N" C_D ")" C_0);
            } else if (case_strstr(found->area->name, "south")) {
                if (case_strstr(found->area->name, "east"))
                    clientf(C_D " (" C_R "SE" C_D ")" C_0);
                else if (case_strstr(found->area->name, "west"))
                    clientf(C_D " (" C_R "SW" C_D ")" C_0);
                else
                    clientf(C_D " (" C_R "S" C_D ")" C_0);
            } else if (case_strstr(found->area->name, "central")
                       || case_strstr(found->area->name, "palm orchard"))
                clientf(C_D " (" C_R "C" C_D ")" C_0);
            else if (case_strstr(found->area->name, "east"))
                clientf(C_D " (" C_R "E" C_D ")" C_0);
            else if (case_strstr(found->area->name, "west"))
                clientf(C_D " (" C_R "W" C_D ")" C_0);

            locate_arena = 0;
        }

        /* Show the vnum, after the line. */
        /* Just one room */
        if (!found2)
            clientff(" " C_D "(" C_G "%d" C_D "%s)" C_0, found->vnum, more ? C_D ", " C_G "..." C_D : "");
        /* All three */
        else if (found3)
            clientff(" " C_D "(" C_G "%d" C_D ", " C_G "%d" C_D "," C_G "%d" C_D "%s)" C_0,
                     found->vnum, found2->vnum, found3->vnum, more ? C_D ", " C_G "..." C_D : "");
        /* Just two */
        else if (found2)
            clientff(" " C_D "(" C_G "%d" C_D ", " C_G "%d" C_D "%s)" C_0, found->vnum, found2->vnum,
                     more ? C_D ", " C_G "..." C_D : "");
        else
            clientff("?");

        /* A farsight-like thing. */
        if (area && found->area) {
            char buf[256];
            g_snprintf(buf, sizeof(buf), C_W "[" C_B "Map" C_W "]: That room is in '%s'.\r\n" C_0, found->area->name);
            prefix_at(-1, buf);
        }

        /* Mark the room on the map, for a few moments. */
        if (!more && found) {
            mark_player(found, player);
        }
    }
}

/* Like locate_vnum but will search current area only) */
gboolean locate_room_in_area(char *vnum_string, int max_size, char *roomname, char *player)
{
    ROOM_DATA *room, *found = NULL, *found2 = NULL, *found3 = NULL;
    char buf[256], *p;
    int more = 0;
	gboolean found_room = FALSE;

    DEBUG("locate_room_in_area");

    strcpy(buf, roomname);

	if((p=strstr(buf, " (house)"))){
		*p='.';
		*(p+1)=0;
	}

    for (room = current_area->rooms; room; room = room->next_in_area) {
        if (!strcmp(buf, room->name)) {
			found_room = TRUE;

            if (!found)
                found = room;
            else if (!found2)
                found2 = room;
            else if (!found3)
                found3 = room;
            else {
                more = 1;
                break;
            }
        }
    }
    if (!found) {
       buf[strlen(buf) - 1] = 0;
       strcat(buf, " (road).");
       for (room = current_area->rooms; room; room = room->next_in_area) {
            if (!strcmp(buf, room->name)) {
                            found_room = TRUE;

                if (!found)
                   found = room;
             else if (!found2)
                   found2 = room;
             else if (!found3)
                   found3 = room;
                else {
                     more = 1;
                     break;
                }
            }
        }
    }

    /* Mark the room on the map, for a few moments. */
    if (!more && found) {
        mark_player(found, player);
    }

    /* Just one room */
    if (!found2 && found)
        snprintf(vnum_string, max_size, " " C_D "(" C_G "%d" C_D "%s)" C_0, found->vnum,
                 more ? C_D "," C_G " ..." C_D : "");
    /* All three */
    else if (found3)
        snprintf(vnum_string, max_size,
                 " " C_D "(" C_G "%d" C_D "," C_G "%d" C_D ", " C_G "%d" C_D "%s)" C_0, found->vnum,
                 found2->vnum, found3->vnum, more ? C_D ", " C_G "..." C_D : "");
    /* Just two */
    else if (found2)
        snprintf(vnum_string, max_size, " " C_D "(" C_G "%d" C_D "," C_G "%d" C_D "%s)" C_0,
                 found->vnum, found2->vnum, more ? C_D "," C_G " ..." C_D : "");

    return found_room;
}

/* Given a title, returns the first room found */
ROOM_DATA *locate_room_by_name(char *roomname)
{
    ROOM_DATA *room, *found = NULL;
    char buf[256], *p;

    DEBUG("locate_room_by_name");

    g_strlcpy (buf, roomname, sizeof(buf));

	if((p=strstr(buf, " (house)"))){
		*p='.';
		*(p+1)=0;
	}

    for (room = world; room; room = room->next_in_world) {
        if (!case_strcmp(buf, room->name) && !found) {
            return room;
        }
    }
    buf[strlen(buf) - 1] = 0;
    strcat(buf, " (road).");
    for (room = world; room; room = room->next_in_world) {
		if (!case_strcmp(buf, room->name)) {
                   return room;
		}
	}

    return NULL;
}

//same as locate_vnum but compares only up to size chars
gboolean locate_vnum_chars(char *vnum_string, int max_size, char *roomname, char *player, int size)
{
    ROOM_DATA *room, *found = NULL, *found2 = NULL, *found3 = NULL;
    char buf[256], *p;
    int more = 0;
	gboolean found_room = FALSE;

    DEBUG("locate_vnum");

    strcpy(buf, roomname);

	if((p=strstr(buf, " (house)"))){
		*p='.';
		*(p+1)=0;
	}

    for (room = world; room; room = room->next_in_world) {
        if (!strncmp(buf, room->name,size)) {
			found_room = TRUE;

            if (!found)
                found = room;
            else if (!found2)
                found2 = room;
            else if (!found3)
                found3 = room;
            else {
                more = 1;
                break;
            }
        }
    }
    if (!found) {
       buf[strlen(buf) - 1] = 0;
       strcat(buf, " (road).");
       for (room = world; room; room = room->next_in_world) {
            if (!strncmp(buf, room->name,size)) {
                            found_room = TRUE;

                if (!found)
                   found = room;
             else if (!found2)
                   found2 = room;
             else if (!found3)
                   found3 = room;
                else {
                     more = 1;
                     break;
                }
            }
        }
    }

    /* Mark the room on the map, for a few moments. */
    if (!more && found) {
        mark_player(found, player);
    }

    /* Just one room */
    if (!found2 && found)
        snprintf(vnum_string, max_size, " " C_D "(" C_G "%d" C_D "%s)" C_0, found->vnum,
                 more ? C_D "," C_G " ..." C_D : "");
    /* All three */
    else if (found3)
        snprintf(vnum_string, max_size,
                 " " C_D "(" C_G "%d" C_D "," C_G "%d" C_D ", " C_G "%d" C_D "%s)" C_0, found->vnum,
                 found2->vnum, found3->vnum, more ? C_D ", " C_G "..." C_D : "");
    /* Just two */
    else if (found2)
        snprintf(vnum_string, max_size, " " C_D "(" C_G "%d" C_D "," C_G "%d" C_D "%s)" C_0,
                 found->vnum, found2->vnum, more ? C_D "," C_G " ..." C_D : "");

    return found_room;
}

/* Returns the nicely formatted room id, similar to locate_room but doesn't print it */
gboolean locate_vnum(char *vnum_string, int max_size, char *roomname, char *player)
{
    ROOM_DATA *room, *found = NULL, *found2 = NULL, *found3 = NULL;
    char buf[256], *p;
    int more = 0;
	gboolean found_room = FALSE;

    DEBUG("locate_vnum");

    strcpy(buf, roomname);

	if((p=strstr(buf, " (house)"))){
		*p='.';
		*(p+1)=0;
	}

    for (room = world; room; room = room->next_in_world) {
        if (!strcmp(buf, room->name)) {
			found_room = TRUE;

            if (!found)
                found = room;
            else if (!found2)
                found2 = room;
            else if (!found3)
                found3 = room;
            else {
                more = 1;
                break;
            }
        }
    }
    if (!found) {
       buf[strlen(buf) - 1] = 0;
       strcat(buf, " (road).");
       for (room = world; room; room = room->next_in_world) {
            if (!strcmp(buf, room->name)) {
                            found_room = TRUE;

                if (!found)
                   found = room;
             else if (!found2)
                   found2 = room;
             else if (!found3)
                   found3 = room;
                else {
                     more = 1;
                     break;
                }
            }
        }
    }

    /* Mark the room on the map, for a few moments. */
    if (!more && found) {
        mark_player(found, player);
    }

    /* Just one room */
    if (!found2 && found)
        snprintf(vnum_string, max_size, " " C_D "(" C_G "%d" C_D "%s)" C_0, found->vnum,
                 more ? C_D "," C_G " ..." C_D : "");
    /* All three */
    else if (found3)
        snprintf(vnum_string, max_size,
                 " " C_D "(" C_G "%d" C_D "," C_G "%d" C_D ", " C_G "%d" C_D "%s)" C_0, found->vnum,
                 found2->vnum, found3->vnum, more ? C_D ", " C_G "..." C_D : "");
    /* Just two */
    else if (found2)
        snprintf(vnum_string, max_size, " " C_D "(" C_G "%d" C_D "," C_G "%d" C_D "%s)" C_0,
                 found->vnum, found2->vnum, more ? C_D "," C_G " ..." C_D : "");


        if(goto_person && found){

            //TODO rethink output

            if(more){
                clientf("More than 3 rooms with that name found, aborting goto.");
            }else if(found2){
                clientfr("WARNING! MORE THAN ONE FITTING ROOM FOUND!");
            }
            if(!more){
                char buffer[10];
                sprintf(buffer, "%d", found->vnum);
                do_goto(buffer);
            }
            goto_person = 0;
        }
    return found_room;
}

void parse_etherseek(char *line){

  //You reach out into the ether, folding it aside to reveal Roroan. As you peer into the opening, the 
  //image of The Garden of the Beast swims into view, then fades away as the ether seeps back into place.

    static char buf[512], name[512];
    char buf2[512], *namestart, *nameend, *locationstart, *locationend;
    static int etherseek_message;

    DEBUG("parse_seek");

    if (!strncmp("You reach out into the ether, folding it aside", line, 46)){
        etherseek_message = 1;
        buf[0] = 0;
    }

    if (!etherseek_message)
        return;

    /* Add a space to the last line, in case there wasn't one. */
    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    if (strstr(buf, "as the ether seeps back into place.")) {
        etherseek_message = 0;

        namestart = strstr(buf, "to reveal ") + 10;

	nameend = strchr(namestart, '.');

	strncpy(name, namestart, nameend - namestart);

        locationstart = strstr(buf, ", the image of ") + 15;

        locationend = strstr(locationstart, " swims into view,");

        strncpy(buf2, locationstart, locationend - locationstart);

        strcat(buf2, ".");

        locate_room(buf2, 1, name);
    }
}

void parse_msense(char *line)
{
    char *end, *nameend;
    char buf[256], name[256];

    DEBUG("parse_msense");

    if (strncmp(line, "You seek out the mind of ", 25))
        return;

    line += 25;

    if (!(end = strstr(line, " appears in your mind.")))
        return;

    if(!(nameend = strstr(line, " and an image of ")))
	return;

    if (end < line)
        return;

    strncpy(name, line, nameend - line);
    name[nameend - line] = 0;
    nameend += 17;
    strncpy(buf, nameend, end - nameend);
    buf[end - nameend] = '.';
    buf[end - nameend + 1] = 0;

    /* We now have the room name in 'buf'. */
    locate_room(buf, 1, name);
}

void parse_detect(char *line, LINES * l)
{
    char name[256], buf[256], vnum_string[256];

    DEBUG("parse_detect");

	/*Reaching out with senses heightened by latent Shin energy, you seek the presence of Keneanung.
	You see that Keneanung is at Yen's Enchantments.*/

	if (strncmp(line, "Reaching out with senses heightened by latent Shin energy, you seek the presence", 80))
        return;

	g_strlcpy(buf, l->line[l->nr_of_lines], sizeof(buf));

    if (!cmp
            ("You see that ^ is at *",
             buf)) {
            extract_wildcard(1, name, 256);
            extract_wildcard(0, line, 256);

            if (locate_vnum(vnum_string, 256, line, name))
				suffix_at(l->nr_of_lines, vnum_string);
	}
}

void parse_allsight(char *line){

  //Allsight: Kytes enters Among the Eleusian Fields.
  //Allsight: Searnla leaves A verdant clearing laden with cherry blossoms.

    static char buf[512], name[512];
    char buf2[512], *p;
    static int as_message;

    DEBUG("parse_allsight");

    if (!cmp("Allsight: ^ enters *", line)
      ||!cmp("Allsight: ^ leaves *",line)){
        as_message = 1;
        extract_wildcard(1, name, 512);
        buf[0] = 0;
    }

    if (!as_message)
        return;

    /* Add a space to the last line, in case there wasn't one. */
    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    if (strstr(buf, ".")) {
        as_message = 0;

        p = strstr(buf, " enters ");

        if (!p){
          p = strstr(buf, " leaves ");

          if (!p)
            return;
	}
        strcpy(buf2, p + 8);

        p = buf2 + strlen(buf2);

        locate_room(buf2, 1, name);
    }
}

void parse_angel(char *line, LINES *l)
{

    //Your guardian angel senses Isilmeriel at Jaxarte Lake, on a health of 1974 and a mana of 3270.

    static char buf[512];
    char *namestart, *nameend, name[512], *locationend, location[512], vnum_string[512];
    static int angel_message;
    int i;

    DEBUG("parse_angel");

    if (!strncmp(line, "Your guardian angel senses ", 27)){
        angel_message = 1;
        buf[0] = 0;
    }

    if (!angel_message)
        return;

    /* Add a space to the last line, in case there wasn't one. */
    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    if (strstr(buf, ".")) {
        angel_message = 0;

        //get the name from the line
        namestart = buf+27;
        nameend = strstr(buf, " at ");
        if(!nameend)    //no end for the name found?
            return;
        strncpy(name,namestart,nameend-namestart);
        name[nameend-namestart]=0;

        //get the location
        locationend = strstr(buf, ", on ");
        if(!locationend)  //no end for the location found?
            return;
        strncpy(location,nameend + 4, locationend - (nameend + 4));
        location[locationend - (nameend + 4)] = '.';
        location[locationend - (nameend + 4) + 1] = 0;


        if(locate_vnum(vnum_string, 256, location, name)){
            for (i = 1; i <= l->nr_of_lines; i++) {
                if(!strncmp(buf,l->line[i], strlen(l->line[i]))){
                    char *foundPosition = strstr(buf, ", on ");
                    for(;foundPosition-buf>strlen(l->line[i]);i++){
                        foundPosition-=strlen(l->line[i]);
                    }
                    insert_at(i, foundPosition - buf, vnum_string);
                }
            }
        }
    }

}

void parse_trace(char *line){

    //Your guardian angel reports that Kard has moved to The harbour of Shallam.

    static char buf[512], name[512];
    char buf2[512], *p;
    static int trace_message;

    DEBUG("parse_trace");

    if (!strncmp("Your guardian angel reports that ", line, 33)){
        trace_message = 1;
        extract_wildcard(1, name, 512);
        buf[0] = 0;
    }

    if (!trace_message)
        return;

    /* Add a space to the last line, in case there wasn't one. */
    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    if (strstr(buf, ".")) {
        trace_message = 0;

        p = strstr(buf, " has moved to ");

        strcpy(buf2, p + 14);

        locate_room(buf2, 1, name);
    }
}

void parse_dragonsense(char *line, LINES * l)
{
    char buf[1024];
    int i;

    DEBUG("parse_dragonsense");

    if (strncmp(line, "Tapping into the unfathomable depths of your power, you ", 56))
        return;

    /* Grab the whole line, because it spans multiple lines */
    strcpy(buf, l->line[1]);
    for (i = 2; i <= l->nr_of_lines; i++) {
        strcat(buf, l->line[i]);
    }

    /* Lazy */
    if (!cmp("Tapping into the unfathomable depths of your power, you sense that ^ is at *", buf)) {
        char name[56], ending[256], roomname[256], vnum_string[256];
        char *p;

        extract_wildcard(1, name, 56);
        extract_wildcard(0, ending, 256);

        if ((p = strstr(ending, ", in"))) {
            strncpy(roomname, ending, p - ending);
            roomname[p - ending] = '.';
            roomname[p - ending + 1] = 0;
            if (locate_vnum(vnum_string, 256, roomname, name))
               suffix_at(i - 1, vnum_string);
        }
    }
}

void parse_herald(char *line, LINES * l)
{

    DEBUG("parse_herald");
    if (strncmp("(Order): ", line, 9))
        return;

    if (!cmp("(Order): ^'s Herald says, \"A shrine is being defiled at *", line)) {
        char name[56], room[256];

        extract_wildcard(1, name, 56);
        extract_wildcard(0, room, 256);

        room[strlen(room) - 2] = '.';
        room[strlen(room) - 1] = 0;
        locate_room(room, 2, name);
    }
}

void parse_farsee(char *line, LINES * l)
{
    char name[256], buf[256], room[256], vnum_string[256];

    DEBUG("parse_farsee");

    if (strncmp(line, "You close your eyes momentarily and extend the range of your vision, seeking out", 80))
        return;
	farseeing=0;
    /* Grab the last line only */
    g_strlcpy(buf, l->line[l->nr_of_lines], sizeof(buf));
    if (!cmp("You see that ^ is at *", buf)) {
        extract_wildcard(1, name, 256);
        extract_wildcard(0, room, 256);

        if (locate_room_in_area(vnum_string, sizeof(vnum_string), room, name))
            suffix_at(l->nr_of_lines, vnum_string);
    }else if(goto_person && !cmp("Though too far away to accurately perceive details, you see that ^ is in *", buf)){
        extract_wildcard(0, room, 256);
        if(!strncmp("the", room, 3))         //since "the" is stripped in survey parsing, strip it here too
            memmove(room, room+4, strlen(room));
        do_goto(room);
        goto_person = 0;
    }
}

void parse_sense(char *line, LINES * l)
{
    char name[256], buf[256], room[256], vnum_string[256];

    DEBUG("parse_sense");

    if (strncmp(line, "You close your eyes momentarily and extend the range of your vision, seeking out", 80))
        return;
    /* Grab the last line only */
    g_strlcpy(buf, l->line[l->nr_of_lines], sizeof(buf));
    if (!cmp("You see that ^ is at *", buf)) {
        extract_wildcard(1, name, 256);
        extract_wildcard(0, room, 256);

        if (locate_vnum(vnum_string, sizeof(vnum_string), room, name))
            suffix_at(l->nr_of_lines, vnum_string);
    }
}

void parse_window(char *line, LINES * l)
{
   char name[256], buf[256], room[256], vnum_string[256];

   DEBUG("parse_window");

   if (strncmp(line, "You create a window in your mind's eye and will the image of", 60))
        return;
   /* Grab the last line only */
   strcpy(buf, l->line[l->nr_of_lines]);
   if (!cmp("You see that ^ is at *", buf)) {
      extract_wildcard(1, name, 256);
      extract_wildcard(0, room, 256);
      if (locate_vnum(vnum_string, 256, room, name))
         suffix_at(l->nr_of_lines, vnum_string);
   }
}

void parse_scent(char *line)
{
    if (!strncmp(line, "You detect traces of scent from ", 32)) {
        locate_room(line + 32, 1, NULL);
    }
}

void parse_scry(char *line)
{
    char buf[256];
    char name[256];

    DEBUG("parse_scry");

    if (!sense_message) {
        if (!cmp
            ("You create a pool of water in the air in front of you, and look through it, sensing ^ at *",
             line)) {
            extract_wildcard(1, name, 256);
            extract_wildcard(0, line, 256);

            locate_room(line, 1, name);
        } else
            if (!strncmp
                (line, "You create a pool of water in the air in front of you, and look through it,", 75))
            sense_message = 1;
    }

    if (sense_message == 1) {
        /* Next line: "sensing Whyte at Antioch Runners tent." */
        /* Skip the first three words. */
        line = get_string(line, buf, 256);
        line = get_string(line, name, 256);
        line = get_string(line, buf, 256);

        locate_room(line, 1, name);
    }
}

void parse_scry_new(char *line)
{


    static char buf[2048];
    static int scry_message;
    GMatchInfo *match_info;
    char *player_name, *area, *room;

    DEBUG("parse_scry_new");

    //~ You dip your fingers into the cool water of the bowl, willing it to reveal the
    //~ location of Crair.
    //~ An image of Hashan appears reflected within the bowl, shifting with the rippling
    //~ water to display the Crossroads.

    if (!strncmp(line, "You dip your fingers into the cool water of the bowl", 52)){
        scry_message = 1;
        buf[0] = 0;
    }

    if (!scry_message)
        return;

    /* Add a space to the last line, in case there wasn't one. */
    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    g_regex_match(scry_new_regex, buf, 0, &match_info);
    if (g_match_info_matches(match_info)) {

        player_name = g_match_info_fetch(match_info, 1);
        area = g_match_info_fetch(match_info, 2);
        room = g_match_info_fetch(match_info, 3);
        locate_room(room, 0, player_name);
        scry_message = 0;
    }

}

void parse_ka(char *line)
{
    static char buf[1024];
    static int periods;
    int i;

    DEBUG("parse_ka");

    /* A shimmering image of *name* appears before you. She/he is at *room*. */

    if (!sense_message) {
        if (!strncmp(line, "A shimmering image of ", 22)) {
            buf[0] = 0;
            sense_message = 2;
            periods = 0;
        }
    }

    if (sense_message == 2) {
        for (i = 0; line[i]; i++)
            if (line[i] == '.')
                periods++;

        strcat(buf, line);

        if (periods == 2) {
            sense_message = 0;

            if ((line = strstr(buf, " is at ")))
                locate_room(line + 7, 1, NULL);
            else if ((line = strstr(buf, " is located at ")))
                locate_room(line + 15, 1, NULL);
        }
    }
}

void parse_seek(char *line)
{
    static char buf[1024];
    static int periods;
    char *end;
    int i;

    DEBUG("parse_seek");

    /* A shimmering image of *name* appears before you. She/he is at *room*. */

    if (!sense_message) {
        if (!strncmp(line, "You bid your guardian to seek out the lifeforce of ", 51)) {
            buf[0] = 0;
            sense_message = 3;
            periods = 0;
        }
    }

    if (sense_message == 3) {
        for (i = 0; line[i]; i++)
            if (line[i] == '.')
                periods++;

        strcat(buf, line);

        if (periods == 3) {
            sense_message = 0;

            if ((line = strstr(buf, " at "))) {
                line += 4;

                end = strchr(line, ',');
                if (!end)
                    return;

                strncpy(buf, line, end - line);
                buf[end - line] = '.';
                buf[end - line + 1] = 0;

                locate_room(buf, 1, NULL);
            }
        }
    }
}

void parse_shrinesight(char *line)
{

    DEBUG("parse_shrinesight");

    if (cmp("A ^ shrine at '*'", line))
        return;

    if (!cmp("A ^ shrine at '*'", line)) {
       char name[56], room[256];

       extract_wildcard(1, name, 56);
       extract_wildcard(0, room, 256);
       room[strlen(room)] = 0;
       locate_room(room, 0, name);
    }
}

void parse_fullsense(char *line)
{
    char *p;
    char buf[256];
    char name[256];
    char room[256];

    DEBUG("parse_fullsense");

    if (strncmp(line, "You sense ", 10) || strlen(line) > 128)
        return;

    strcpy(buf, line);

    p = strstr(buf, " at ");
    if (!p)
        return;
    *p = 0;
    p += 4;

    strcpy(name, buf + 10);
    strcpy(room, p);

    /* We now have the room name in 'buf'. */
    locate_room(room, 0, name);
}

void parse_scout(char *line)
{
    char buf[512], name[512], *p;
    ROOM_DATA *room = NULL;

    DEBUG("parse_scout");

    if (!strcmp(line, "You sense the following people in the area:")
        || !strcmp(line, "You sense the following creatures in the area:")
	|| !strcmp(line, "You detect the following people disturbing the tranquillity of the forest.")) {
        scout_list = 1;
        return;
    }

    if (!scout_list)
        return;

    p = strstr(line, " at ");
    if (!p)
        return;

    strcpy(buf, p + 4);
    strcat(buf, ".");
    get_string(line, name, 512);

    room=locate_room(buf, 0, name);
    if(room && room->area)
	clientff(" " C_R "(" C_G "%s" C_R ")" C_0, room->area->name);

}

void parse_view(char *line)
{
    char name[256], buf[256];

    DEBUG("parse_view");

    /* You see Name at Location. */

    if (strncmp("You see ", line, 8))
        return;

    line = get_string(line + 8, name, 256);
    line = get_string(line, buf, 256);

    if (strcmp("at", buf))
        return;

    locate_room(line, 0, name);
}

void parse_pursue(char *line)
{
    static char buf[512], name[512];
    char buf2[512], *p;
    static int pursue_message;

    DEBUG("parse_pursue");

    if (!cmp("You sense that ^ has entered *", line)) {
        pursue_message = 1;
        get_string(line + 15, name, 512);
        buf[0] = 0;
    }

    if (!pursue_message)
        return;

    /* Add a space to the last line, in case there wasn't one. */
    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    if (strstr(buf, ".")) {
        pursue_message = 0;

        p = strstr(buf, " has entered ");

        if (!p)
            return;

        strcpy(buf2, p + 13);

        p = buf2 + strlen(buf2);

        while (p > buf2) {
            if (*p == ',') {
                *(p++) = '.';
                *p = 0;
                break;
            }
            p--;
        }

        locate_room(buf2, 1, name);
    }
}

void parse_scan(char *line)
{
    char *p;
    char buf[256];
    char name[256];
    char room[256];

    DEBUG("parse_scan");

	/*You make out the figure of Jian at Ivy-covered archway to Minia.*/

    if (strncmp(line, "You make out the figure of ", 27) || strlen(line) > 128)
        return;

    strcpy(buf, line);

    p = strstr(buf, " at ");
    if (!p)
        return;
    *p = 0;
    p += 4;

    strcpy(name, buf + 27);
    strcpy(room, p);

    /* We now have the room name in 'room'. */
    locate_room(room, 0, name);
}

void parse_alertness(char *line)
{
    static char buf[4096];
    static int alertness_message;
    char buf2[4096];

    DEBUG("parse_alertness");

    if (disable_alertness)
        return;

    if (!cmp("Your enhanced senses inform you that *", line)) {
        buf[0] = 0;
        alertness_message = 1;
    }

    if (!alertness_message)
        return;

    hide_line();

    /* In case something goes wrong. */
    if (alertness_message++ > 3) {
        alertness_message = 0;
        return;
    }

    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    if (strstr(buf, "nearby.")) {
        /* We now have the full message in 'buf'. Parse it. */
        char player[256];
        char room_name[256];
        char *p, *p2;
        int found = 0;
        int i;

        alertness_message = 0;

        /* Your enhanced senses inform you that <player> has entered <room> nearby. */

        p = strstr(buf, " has entered");
        if (!p)
            return;
        *p = 0;

        strcpy(player, buf + 37);

        p2 = strstr(p + 1, " nearby");
        if (!p2)
            return;
        *p2 = 0;

        strcpy(room_name, p + 13);
        strcat(room_name, ".");

        alertness_message = 0;

        g_snprintf(buf2, sizeof(buf2), C_R "[" C_W "%s" C_R " - ", player);

        /* Find where that room is. */

        if (current_room) {
            if (!room_cmp(current_room->name, room_name)) {
                strcat(buf2, "here");
                found = 1;
            }

            for (i = 1; dir_name[i]; i++) {
                if (!current_room->exits[i])
                    continue;

                if (!room_cmp(current_room->exits[i]->name, room_name)) {
                    if (found)
                        strcat(buf2, ", ");

                    strcat(buf2, C_B);
                    strcat(buf2, dir_name[i]);
                    found = 1;
                }
            }
        }

            if (!found) {
                p=strrchr(room_name,'.');
                *p=0;
                strcat(room_name, " (road).");
                if (current_room) {
                    if (!room_cmp(current_room->name, room_name)) {
                        strcat(buf2, "here");
                        found = 1;
                    }

                    for (i = 1; dir_name[i]; i++) {
                        if (!current_room->exits[i])
                            continue;

                        if (!room_cmp(current_room->exits[i]->name, room_name)) {
                            if (found)
                                strcat(buf2, ", ");

                            strcat(buf2, C_B);
                            strcat(buf2, dir_name[i]);
                            found = 1;
                        }
                    }
                }

            if(!found){
                strcat(buf2, C_R);
                strcat(buf2, room_name);
            }
        }

        strcat(buf2, C_R "]\r\n" C_0);
        suffix(buf2);
    }
}

void parse_eventstatus(char *line)
{
    char buf[512], name[512];

    DEBUG("parse_eventstatus");

    if (!strncmp(line, "Current event: ", 15)) {
        evstatus_list = 1;
        return;
    }

    if (!evstatus_list)
        return;

    line = get_string(line, buf, 512);

    if (buf[0] == '-')
        return;

    if (!strcmp(buf, "Player"))
        return;

    if (buf[0] < 'A' || buf[0] > 'Z') {
        evstatus_list = 0;
        return;
    }

    strcpy(name, buf);

    /* Skip all numbers and other things, until we get to the room name. */
    while (1) {
        if (!line[0])
            return;

        if (line[0] < 'A' || line[0] > 'Z') {
            line = get_string(line, buf, 512);
            continue;
        }

        break;
    }

    strcpy(buf, line);
    strcat(buf, ".");

    locate_arena = 1;
    locate_room(buf, 0, name);
}

void parse_forestwatch(char *line){

  //Lianca has entered the forest at View of the forest.
  //Irion leaves the forest from Beneath the sycamore trees.

    static char buf[512], name[512];
    char buf2[512], *p;
    static int fw_message;

    DEBUG("parse_forestwatch");

    if (!cmp("^ has entered the forest at *", line)
      ||!cmp("The soul of ^ has entered the forest at *", line)
      ||!cmp("^ leaves the forest from *",line)
      ||!cmp("The soul of ^ leaves the forest from *",line)) {
        fw_message = 1;
        extract_wildcard(1, name, 512);
        buf[0] = 0;
    }

    if (!fw_message)
        return;

    /* Add a space to the last line, in case there wasn't one. */
    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    if (strstr(buf, ".")) {
        fw_message = 0;

        p = strstr(buf, " has entered the forest at ");

        if (!p){
          p = strstr(buf, " leaves the forest from ");

          if (!p)
            return;
	  strcpy(buf2, p + 24); 
	}else
          strcpy(buf2, p + 27);

        p = buf2 + strlen(buf2);

        locate_room(buf2, 1, name);
    }

}

void parse_grovetrack(char *line){

  
  //The leaves whisper, "Vincenzio has moved to Shadow-veiled thicket."

    static char buf[512], name[512];
    char buf2[512], *p;
    static int gt_message;

    DEBUG("parse_grovetrack");

    if (!strncmp("The leaves whisper, ", line, 20)) {
        gt_message = 1;
        get_string(line + 21, name, 512);
        buf[0] = 0;
    }

    if (!gt_message)
        return;

    /* Add a space to the last line, in case there wasn't one. */
    if (buf[0] && buf[strlen(buf) - 1] != ' ')
        strcat(buf, " ");

    strcat(buf, line);

    if (strstr(buf, ".")) {
        gt_message = 0;
	buf[strlen(buf)-1]=0;

        p = strstr(buf, " has moved to ");

        if (!p)
            return;

        strcpy(buf2, p + 14);

        locate_room(buf2, 0, name);
    }

}

void parse_wormholes_init(char *line){

	WARP_DATA *warp1, *warp2;

	DEBUG("parse_wormholes_init");

	if(strcmp(line, "You close your eyes and seek out all wormhole sources."))
		return;

	if(mode!=CREATING)
		return;

	if(!current_room){

		clientfv("Don't know where we are at the moment. QL or walk about first.");
		return;

	}

	for (warp1 = current_room->area->warps; warp1; warp1 = warp2) {
		warp2 = warp1->next;
		destroy_warp(warp1);
	}

}

void parse_wormholes(char *line)
{
    static int unfinished_message;
    static char name1[1024];
    //~ char destination[512];
    char *name2;
    ROOM_DATA *room2;

	DEBUG("parse_wormholes");

    if (!cmp("You sense a connection between *", line)) {
        name1[0] = 0;
        line += 31;
        unfinished_message = 1;
    }

    if (!unfinished_message)
        return;

    if (name1[0])
        strcat(name1, line);
    else
        strcpy(name1, line);

    if (strstr(name1, "."))
        unfinished_message = 0;
    else {
        /* Unfinished message - if it doesn't end in a space, add one. */
        if (name1[0] && name1[strlen(name1) - 1] != ' ')
            strcat(name1, " ");
        return;
    }

    name2 = strstr(name1, " and ");
    if (!name2)
        return;
    *name2 = '.';
    *(name2 + 1) = 0;

    name2 += 5; /* ??? */

    locate_room(name1, 0, NULL);
    suffix(" ->");
    locate_room(name2, 0, NULL);
    room2 = locate_room_by_name(name2);

	if(mode == CREATING){

		WARP_DATA *warp1, *warp2 = NULL;
		ROOM_DATA *room;

		room = locate_room_by_name(name1);

		if(room){

			warp1 = create_warp(room, room2);

			if(warp1){
				warp2 = create_warp(room2, room);
				warp1->reverse = warp2;
			}
			if(warp2)
				warp2->reverse = warp1;
		}
	}

    if (room2)
    clientff(" " C_D "(in " C_0 "%s" C_D ")" C_0, room2->area->name);

    mark_wormhole(name1);
    mark_wormhole(name2);
}

void parse_who(char* line)
{
    char* p;
    char name[256], place[256], vnum[256],cmd[256];
    int length;
    DEBUG("parse_who");

    if(!wholist||disable_wholist)
      return;       

    if(!strncmp("[Type MORE if you wish to continue reading. ", line, 44)){
        wholist++;
        return;
    }

    strcpy(name, line);
    p=strchr(name,' ');
    if(!p)
      return;
    *p=0;
    p=strchr(line,'(');
    if(!p)
      return;
    strcpy(place, p+1);
    p=strrchr(place,')');
    if(!p)
      return;
    *p=0;
    if(!strncmp("(house) ",place, 8)){
      p=place;
      strcpy(place, p+8);
    }
    hide_line();

    if(!case_strncmp(wholist_argument,"room",4)){//did we want to have a look, who's in a certain room?
      p=wholist_argument;
      strcpy(cmd,p+5);
      if(!cmd[0]){
        clientfv("No room for matching supplied. Have a look at 'wholist help'.");
	wholist_argument[0] = 0;
        return;
      }
      if(strlen(place)<strlen(cmd))
        length=strlen(place);
      else
        length=strlen(cmd);
      if(!case_strncmp(place,cmd,length)){
        clientff("%-20s%33s\r\n", name, place);
      } 
    }else{ //we just wanted to parse "who b"
      if(locate_vnum_chars(vnum,256,place, name,strlen(place)))
        clientff("%-20s%33s%s\r\n", name, place,vnum);
      else
        clientff("%-20s%33s\r\n", name, place);

     }
}

void parse_underwater(char *line)
{
    DEBUG("parse_underwater");

    /* 0 - No, 1 - Trying to get up, 2 - Up. */

    if (!strncmp(line, "You eat the pear and immediately your skin begins exuding tiny", 62)
        || !strcmp(line, "You are already surrounded by a pocket of air.")
        || !strcmp(line, "You are surrounded by a pocket of air and so must move normally through water.")
        || !strcmp(line, "You eat the pear and hear the sound of air rushing from your pores."))
        pear_defence = 2;

    if (!strcmp(line, "The pocket of air around you dissipates into the atmosphere.")
        || !strcmp(line, "The bubble of air around you dissipates."))
        pear_defence = 0;
}

void parse_special_exits(char *line)
{
    EXIT_DATA *spexit;

    DEBUG("parse_special_exits");

    if (!current_room)
        return;

    if (mode != FOLLOWING && mode != CREATING)
        return;

    /* Since this function is checked after parse_room, this is a good place. */
    if (mode == CREATING && capture_special_exit==1 && strcmp(line,"\0")) {
		capture_special_exit=2;
		strcpy(cse_message, line);
        return;
    }

    for (spexit = current_room->special_exits; spexit; spexit = spexit->next) {
        if (!spexit->message)
            continue;

        if (!cmp(spexit->message, line)) {
            if (spexit->to) {
                current_room = spexit->to;
                current_area = current_room->area;
                if(alert_wormhole && current_room->warp &&
                        current_room->warp->link &&
                        current_room->warp->link->to &&
                        current_room->warp->link->to->name &&
                        current_room->warp->link->to->area &&
                        current_room->warp->link->to->area->name){
                    char buf[256];
                    g_snprintf(buf, sizeof(buf),  C_R " (" C_B "Wormhole to %s (%s)" C_R ")" C_0,
                            current_room->warp->link->to->name,
                            current_room->warp->link->to->area->name);
                    clientff("%s", buf);
                }
                add_queue_top(-1);
            } else {
                mode = GET_UNLOST;
            }

            return;
        }
    }

    for (spexit = global_special_exits; spexit; spexit = spexit->next) {
        if (!spexit->message)
            continue;

        if (!cmp(spexit->message, line)) {
            if (spexit->to) {
                current_room = spexit->to;
                current_area = current_room->area;
                if(alert_wormhole && current_room->warp &&
                            current_room->warp->link &&
                            current_room->warp->link->to &&
                            current_room->warp->link->to->name &&
                            current_room->warp->link->to->area &&
                            current_room->warp->link->to->area->name){
                        char buf[256];
                        g_snprintf(buf, sizeof(buf),  C_R " (" C_B "Wormhole to %s (%s)" C_R ")" C_0,
                                current_room->warp->link->to->name,
                                current_room->warp->link->to->area->name);
                        clientff("%s", buf);
                }
                add_queue_top(-1);
            } else {
                mode = GET_UNLOST;
            }

            clientff(C_R " (" C_Y "%s" C_R ")" C_0, spexit->command ? spexit->command : "teleport");
            return;
        }
    }
}

void parse_sprint(char *line)
{
    static int sprinting;
    char buf[256];
	int rooms_sprinted=0;

    DEBUG("parse_sprint");

    if (mode != FOLLOWING && mode != CREATING)
        return;

	/* parse and exrtact sprinting Direction */
    if (!sprinting && !strncmp(line, "You look off to the ", 20)
        && strstr(line, " and dash speedily away.")) {
        int i;

        get_string(line + 20, buf, 256);

        sprinting = 0;

        for (i = 1; dir_name[i]; i++) {
            if (!strcmp(buf, dir_name[i])) {
                sprinting = i;
                break;
            }
        }

        return;
    }

    if (!strcmp("You kick your mount in the haunches.", line)) {
		if(auto_walk!=0){
			sprinting=current_room->pf_direction;
		} else{
			sprinting=gallop_direction;
		}
        return;
    }

    if (!strcmp("You kick your giraffe in the haunches and scream, \"RUN AWAY! RUN AWAY!\"", line)){
		if(auto_walk!=0){
			sprinting=current_room->pf_direction;
		} else{
			sprinting=gallop_direction;
		}
        return;
    }

    if (!sprinting)
        return;

    /* End of the sprint? */
	while(rooms_sprinted<10 && current_room->exits[sprinting] && current_room->exits[sprinting]->exits[sprinting]){
		current_room=current_room->exits[sprinting];
		rooms_sprinted++;
	}
	add_queue(sprinting);
	sprinting=0;
}

void parse_follow(char *line)
{

	DEBUG("parse_follow");


	/*You shadow Sage Asmodron Dicondron, wielder of Dark Arts west to The Crossroads.*/

    static char msg[256];
    static int in_message;
    char *to;

    if (mode != FOLLOWING || !current_room)
        return;

    if (!cmp("You follow *", line)) {
        in_message = 1;

        strcpy(msg, line);
	} else if (!cmp("You shadow *", line)){
        in_message = 1;

        strcpy(msg, line);
    } else if (in_message) {
        if (strlen(msg) > 0 && msg[strlen(msg) - 1] != ' ')
            strcat(msg, " ");
        strcat(msg, line);
    } else
        return;

    if (in_message && !strchr(msg, '.'))
        return;

    in_message = 0;

    if (!(to = strstr(msg, " to ")))
        return;

    {
        /* We now have our message, which should look like this... */
        /* You follow <TitledName> <direction> to <roomname> */

        ROOM_DATA *destination = NULL;
        char room[256], dir[256];
        char *d;
        int changed_area = 0;
        int i, dir_nr = 0;

        /* Snatch the room name, then look backwards for the direction. */
        strcpy(room, to + 4);
        if (!room[0]) {
            debugf("No room name found.");
            return;
        }

        d = to - 1;
        while (d > msg && *d != ' ')
            d--;
        get_string(d, dir, 256);

        /* We now have the room, and the direction. */

        for (i = 1; dir_name[i]; i++)
            if (!strcmp(dir, dir_name[i])) {
                dir_nr = i;
            }

        if (dir_nr)
            destination = current_room->exits[dir_nr];

        if (!destination || room_cmp(destination->name, room)) {
            /* Perhaps it's a special exit, so look for one. */
            EXIT_DATA *spexit;
			char *periodpos;

            for (spexit = current_room->special_exits; spexit; spexit = spexit->next) {
                if (!spexit->to || room_cmp(spexit->to->name, room))
                    continue;

                destination = spexit->to;
                clientff(C_R " (" C_Y "sp" C_R ")" C_0);
                break;
            }


            /* Or perhaps we're on a road, so add one to the title*/
            if(!spexit){
                periodpos=strchr(room,'.');
                *periodpos='\0';
                strcat(room, " (road).");

                for (spexit = current_room->special_exits; spexit; spexit = spexit->next) {
                    if (!spexit->to || room_cmp(spexit->to->name, room))
                        continue;

                    destination = spexit->to;
                    clientff(C_R " (" C_Y "sp" C_R ")" C_0);
                    break;
                }
            }


            /* Or perhaps we're just simply lost... */
            if (!destination || (!spexit && room_cmp(destination->name, room))) {
                clientff(C_R " (" C_G "lost" C_R ")" C_0);
                mode = GET_UNLOST;
                return;
            }


        }



        if (current_room->area != destination->area)
            changed_area = 1;

        current_room = destination;

        clientff(C_R " (" C_g "%d" C_R ")" C_0, current_room->vnum);
        if (changed_area) {
            current_area = current_room->area;
            clientff("\r\n" C_R "[Entered the %s]" C_0, current_area->name);
        }
    }
}

void parse_leap(char *line, LINES *l){

	char buf[87];
	int i;

    DEBUG("parse_leap");

    if (mode != FOLLOWING && mode != CREATING)
             return;

    if (strncmp(line, "You bunch your powerful muscles and launch yourself in a majestic leap", 70)) {
             return;
    }

	/* Grab the whole line, because it spans multiple lines */
	if(strlen(line)==71){
		strcpy(buf, l->line[1]);
		strcat(buf, l->line[2]);
	}else
		strcpy(buf, line);

    if (!cmp("You bunch your powerful muscles and launch yourself in a majestic leap *wards.", buf)) {
        char direction[10];
        extract_wildcard(0, direction, 10);

		for (i = 1; dir_name[i]; i++) {
			if (!strcmp(dir_name[i], direction)) {
                break;
            }
		}

		add_queue(i);
	}
}


void parse_bound(char *line, LINES *l){

	char buf[103];
	int i;

    DEBUG("parse_bound");

    if (mode != FOLLOWING && mode != CREATING)
             return;

	/*Summoning all your might, you take a couple of short steps before launching yourself to the south.*/

    if (strncmp(line, "Summoning all your might, you take a couple of short steps before launching", 75)) {
             return;
    }

	/* Grab the whole line, because it spans multiple lines */
	if(strlen(line)==75){
		strcpy(buf, l->line[1]);
		strcat(buf, l->line[2]);
	}else
		strcpy(buf, line);

    if (!cmp("Summoning all your might, you take a couple of short steps before launching yourself to the *.", buf)) {
        char direction[10];
        extract_wildcard(0, direction, 10);

		for (i = 1; dir_name[i]; i++) {
			if (!strcmp(dir_name[i], direction)) {
				if(current_room->exits[i])
					current_room=current_room->exits[i];
                break;
            }
		}

		add_queue(i);
	}
}


void parse_shadow(char *line){

	DEBUG("parse_shadow");

	if (mode != FOLLOWING && mode != CREATING)
             return;

	/*You begin to silently shadow Cirana's movements about the land.*/

	if (cmp("You begin to silently shadow *'s movements about the land.", line)){
		return;
	}else{
		add_queue(-2);
	}

}


void parse_tumble (char *line){

	DEBUG("parse_tumble");

	if (mode != FOLLOWING && mode != CREATING)
             return;

	/*You begin to tumble agilely to the south.*/

	if (cmp("You begin to tumble agilely to the *.", line)){
		return;
	}else{
        char direction[10];
		int i;
        extract_wildcard(0, direction, 10);

		for (i = 1; dir_name[i]; i++) {
			if (!strcmp(dir_name[i], direction)) {
                break;
            }
		}

		add_queue(i);
	}

}

void parse_wormseek (char *line){

	DEBUG("parse_wormseek");

	if (mode != FOLLOWING && mode != CREATING)
             return;

	/*You sense a wormhole leading to The Tanyard.*/

	if (cmp("You sense a wormhole leading to *", line)){
		return;
	}else{
        char roomname[256];
		ROOM_DATA *dest;
        extract_wildcard(0, roomname, 256);

		dest = locate_room_by_name(roomname);
		if(mode == CREATING){

			WARP_DATA *warp1, *warp2 = NULL;

			if(current_room){

				if(current_room->warp)
					destroy_warp(current_room->warp);

				warp1 = create_warp(current_room, dest);

				if(warp1){
					warp2 = create_warp(dest, current_room);
					warp1->reverse = warp2;
				}
				if(warp2)
					warp2->reverse = warp1;
			}
		}

		locate_room(roomname, 0, NULL);

		if(dest)
			clientff(" " C_D "(in " C_0 "%s" C_D ")" C_0, dest->area->name);

		mark_wormhole(current_room->name);
		mark_wormhole(roomname);

	}

}


void parse_mountkick(char *line, LINES *l){

	char buf[256];
	char *comma;
	int i;

	if (mode != FOLLOWING && mode != CREATING)
             return;

	/*Dracot gives a yell and slaps a rugged war horse, who powerfully kicks you with its hind legs and sends you sailing north with arms flailing.*/

	if(!strstr(line, "gives a yell and slaps"))
		return;

	debugf("recongized it");

	/* Grab the whole line, because it spans multiple lines */
	if(strlen(line)<81){
		strcpy(buf, l->line[1]);
		strcat(buf, l->line[2]);
	}else
		strcpy(buf, line);

	comma=strrchr(buf,',');

	if (!cmp(", who powerfully kicks you with its hind legs and sends you sailing * with arms flailing.", comma)) {
        char direction[10];
        extract_wildcard(0, direction, 10);

		for (i = 1; dir_name[i]; i++) {
			if (!strcmp(dir_name[i], direction)) {
                break;
            }
		}

		add_queue(i);
	}

}


void parse_autobump(char *line)
{
    /* 1 - Bump all around that room. */
    /* 2 - Wait for bumps to complete. */
    /* 3 - Search a new room. */

    if (!auto_bump)
        return;

    if (auto_bump == 2) {
        if (!strcmp("There is no exit in that direction.", line))
            bump_exits--;

        if (!bump_exits) {
            ROOM_DATA *r;
            int i;

            auto_bump = 3;

            r = bump_room, i = 0;
            /* Count rooms left. */
            while (r) {
                r = r->next_in_area;
                i++;
            }

            clientff(C_R "\r\n[Rooms left: %d.]\r\n" C_0, i - 1);

            bump_room = bump_room->next_in_area;

            if (!bump_room) {
                auto_bump = 0;
                clientfv("All rooms completed.");
            }

            clean_paths();
            add_to_pathfinder(bump_room, 1);
            path_finder_2();
            go_next();
        }
    }

}

void check_autobump()
{
    if (!auto_bump || !bump_room)
        return;

    /* 1 - Bump all around that room. */
    /* 2 - Wait for bumps to complete. */
    /* 3 - Search a new room. */

    if (auto_bump == 1) {
        bump_exits = 0;
        int i;

        send_to_server("survey\r\n");
        if(current_room){
            current_room->indoors = indoors;
        }
        clientff("\r\n");

        for (i = 1; dir_name[i]; i++) {
            if (bump_room->exits[i] || bump_room->locked_exits[i])
                continue;

            bump_exits++;
            send_to_server(dir_small_name[i]);
            send_to_server("\r\n");
        }

        auto_bump = 2;
    }

    if (auto_bump == 2)
        return;

    if (auto_bump == 3 && bump_room == current_room) {
        /* We're here! */

        auto_bump = 1;
        check_autobump();
    }
}

/* This function bundles all locating abilities. */
void parse_locating(char *line, LINES *l){

    if (farseeing)
        parse_farsee(line, l);
    else
        parse_sense(line, l);

    parse_who(line);
    parse_msense(line);
    parse_etherseek(line);
    parse_detect(line, l);
    parse_allsight(line);
    parse_angel(line, l);
    parse_trace(line);
    parse_dragonsense(line, l);
    parse_herald(line, l);
    parse_window(line, l);
    parse_scent(line);
    parse_scry(line);
    parse_scry_new(line);
    parse_ka(line);
    parse_seek(line);
    parse_scout(line);
    parse_view(line);
    parse_pursue(line);
    parse_forestwatch(line);
    parse_eventstatus(line);
    parse_grovetrack(line);

    /*wormhole management*/
    parse_wormholes_init(line);
    parse_wormholes(line);
    parse_wormseek(line);

    /* Is this a fullsense command? */
    parse_fullsense(line);
    parse_scan(line);
    parse_shrinesight(line);

}

/* This function bundles all movement types. */
void parse_specialMovement(char *line, LINES *l){

    /*Did we leap?*/
    parse_leap(line, l);

    /*Did we bound?*/
    parse_bound(line, l);

    /*Did we shadow?*/
    parse_shadow(line);

    /*tumbling?*/
    parse_tumble(line);

    /*moving against our will in all sorts*/
    parse_mountkick(line, l);

    /* Are we sprinting, now? */
    parse_sprint(line);

    /* Is this a special exit message? */
    parse_special_exits(line);

    /* Is this a follow message, if we're following someone? */
    parse_follow(line);

}

/* Here we gather additional information about our location */
void parse_locationInfo(char *line){

    /* Can we get the area name and room env from here? */
    parse_survey(line);

    /* Are we indoors? */
    parse_stars(line);

    /* Are we under water? */
    parse_underwater(line);

}

void parse_blocks(char *line){

const char *block_messages[] = { "You cannot move that fast, slow down!",
        "You cannot move until you have regained equilibrium.",
        "You are regaining balance and are unable to move.",
        "You'll have to swim to make it through the water in that direction.",
        "rolling around in the dirt a bit.",
        "You are asleep and can do nothing. WAKE will attempt to wake you.",
        "There is a door in the way.",
        "You must first raise yourself and stand up.",
        "You are surrounded by a pocket of air and so must move normally through water.",
        "You cannot fly indoors.",
        "You slip and fall on the ice as you try to leave.",
        "A rite of piety prevents you from leaving.",
        "A wall blocks your way.",
        "The hands of the grave grasp at your ankles and throw you off balance.",
        "You are unable to see anything as you are blind.",
        "The forest seems to close up before you and you are prevented from moving that ",
        "There is no exit in that direction.",
        "The ground seems to tilt and you fall with a thump.",
        "Your legs are tangled in a mass of rope and you cannot move.",
        "You must stand up to do that.",
        "Sticky strands of webbing prevent you from moving.",
        "You cannot walk through an enormous stone gate.",
        "You must regain your equilibrium first.",
        "You must regain balance first.",
        "You fumble about drunkenly.",
        "You try to move, but the quicksand holds you back.",
        "You struggle to escape the quicksand, but only manage to partly free yourself.",
        "A loud sucking noise can be heard as you try desperately to escape the mud.",
        "You struggle to escape the mud, but only manage to partly free yourself.",
        "A strange force prevents the movement.",
        "You are too stunned to be able to do anything.",
        "Now now, don't be so hasty!",
        "You are recovering equilibrium and cannot move yet.",
        "You must first raise yourself from the floor and stand up.",
        "You must be standing first.",
	"You carefully watch your footing and swing down from the treetops.",	//not an actual block, but we don't change the room

        NULL
    };
    int i;

    if (!cmp("*You land easily, back on the ground again.", line)) 
        add_queue(-1);

    /* did we get blocked? These are fully comparable. */
    for (i = 0; block_messages[i]; i++)
        if (!strcmp(line, block_messages[i])) {
            /* Remove last command from the queue. */
            if (q_top)
                q_top--;

            if (!q_top)
                del_timer("queue_reset_timer");

            break;
        }

    /* did we get blocked (part II)? These have things that may change and thus need special
       treatment. */
    if (!strncmp(line, "There is a door in the way, to the ", 35)
	|| !strncmp(line,"There's water ahead of you. You'll have to SWIM ",48)
	|| strstr(line, " stops you from moving that way.")) {
        /* Remove last command from the queue. */
        if (q_top)
            q_top--;

        if (!q_top)
            del_timer("queue_reset_timer");

    }
}

/* Here we manage, how the autowalker should react to certain lines */
void manage_autowalk(char *line){
    if (!strncmp(line,"There's water ahead of you. You'll have to SWIM ",48)) {
        if (disable_swimming) {
           clientf("\r\n" C_R
                   "[Hint: Swimming is disabled. Use 'vmap config swim' to turn it back on.]" C_0);
        }
    }

    if (auto_walk == 1 && (
       !strcmp(line, "You are recovering equilibrium and cannot move yet.") ||
       !strcmp(line, "You cannot move until you have regained equilibrium.") ||
       !strcmp(line, "You are regaining balance and are unable to move.") ||
       !strcmp(line, "You must regain your equilibrium first.") ||
       !strcmp(line, "You must regain balance first.")
       )){

        wait_balance = 1;

    }

    if (!strcmp(line, "You cannot move that fast, slow down!")
     || !strcmp(line, "Now now, don't be so hasty!")) {
        auto_walk = 2;
    }

    if (!strncmp(line, "There is a door in the way, to the ", 35)) {
        door_closed = 1;
        door_locked = 0;
        auto_walk = 2;
    }

    if (!strcmp(line, "This door has been magically locked shut.")
     || !strcmp(line, "The door is locked.")) {
        door_closed = 0;
        door_locked = 1;
        auto_walk = 2;
    }

    if (!strncmp(line, "You open the door to the ", 25)) {
        door_closed = 0;
        door_locked = 0;
        auto_walk = 2;
    }

    if (!strncmp(line, "You unlock the ", 15)) {
        door_closed = 1;
        door_locked = 0;
        auto_walk = 2;
    }

    if (!strcmp(line, "You are not carrying a key for this door.")) {
        door_closed = 0;
        door_locked = 0;
    }

    if (!strncmp(line, "There is no door to the ", 24)) {
        door_closed = 0;
        door_locked = 0;
        auto_walk = 2;
    }

    if(wait_balance && (!strcmp(line, "You have recovered balance on all limbs.")
                    || !strcmp(line,"You have recovered equilibrium."))){
        auto_walk = 2;
        wait_balance = 0;
    }
}

void process_line(char *line, int len, LINES * l)
{
    DEBUG("process_line");

    /* Is this a sense/seek command? */
    if (!disable_locating) {
        parse_locating(line, l);
    }

    parse_locationInfo(line);

    /* parse special movement types */
    parse_specialMovement(line, l);

    parse_blocks(line);

    /* Gag/replace the alertness message, with something easier to read. */
    parse_alertness(line);

    parse_autobump(line);

    if (auto_walk) {
        manage_autowalk(line);
    }
}

void i_mapper_process_server_prompt(LINE * l)
{
    DEBUG("i_mapper_process_server_prompt");

    if (parsing_room)
        parsing_room = 0;

    if (sense_message)
        sense_message = 0;

    if (scout_list)
        scout_list = 0;

    if (evstatus_list)
        evstatus_list = 0;

    if (pet_list)
        pet_list = 0;

    if (wholist)
        wholist--;

    if (wholist_argument[0] && !wholist)
        wholist_argument[0] = 0;

	if (capture_special_exit){
        cse_message[0] = 0;
		capture_special_exit=1;
	}

    if (get_unlost_exits) {
        ROOM_DATA *r, *found = NULL;
        int count = 0, i;

        get_unlost_exits = 0;

        if (!current_room)
            return;

        for (r = world; r; r = r->next_in_world) {
            if (!strcmp(r->name, current_room->name)) {
                int good = 1;
                for (i = 1; dir_name[i]; i++) {
                    if (((r->exits[i] || r->detected_exits[i])
                         && !get_unlost_detected_exits[i]) || (!(r->exits[i] || r->detected_exits[i])
                                                               && get_unlost_detected_exits[i])) {
                        good = 0;
                        break;
                    }
                }

                if (good) {
                    if (!found)
                        found = r;

                    count++;
                }
            }
        }

        if (!found) {
            prefix(C_R "[No perfect matches found.]\r\n" C_0);
            mode = GET_UNLOST;
        } else {
            current_room = found;
            current_area = current_room->area;
            mode = FOLLOWING;

            if (!count) {
                prefix(C_W "Map: Impossible error.\r\n");
                return;
            }

            prefixf(C_R "[Match probability: %d%%]\r\n" C_0, 100 / count);
        }
    }

    if (check_for_duplicates) {
        check_for_duplicates = 0;

        if (current_room && mode == CREATING) {
            ROOM_DATA *r;
            int count = 0, far_count = 0, i;

            for (r = world; r; r = r->next_in_world)
                if (!strcmp(current_room->name, r->name) && r != current_room) {
                    int good = 1;

                    for (i = 1; dir_name[i]; i++) {
                        int e1 = r->exits[i] ? 1 : r->detected_exits[i];
                        int e2 = current_room->exits[i] ? 1 : current_room->detected_exits[i];

                        if (e1 != e2) {
                            good = 0;
                            break;
                        }
                    }

                    if (good) {
                        if (r->area == current_room->area)
                            count++;
                        else
                            far_count++;
                    }
                }

            if (count || far_count) {
                prefixf(C_R
                        "[Warning: Identical rooms found. %d in this area, %d in other areas.]\r\n"
                        C_0, count, far_count);
            }
        }
    }

    if (current_room && (!pear_defence && auto_pear) && (current_room->underwater || current_room->room_type->underwater)) {
        pear_defence = 1;
        send_to_server("outr pear\r\neat pear\r\n");
        clientf(C_W "(" C_G "outr/eat pear" C_W ") " C_0);
    }

    if ((mode == FOLLOWING && auto_walk == 2)||auto_bump) {
        go_next();
    }

    check_autobump();

    if (last_room != current_room && current_room) {
        show_floating_map(current_room);
        last_room = current_room;
    }

    if (gag_next_prompt) {
        hide_line();
        gag_next_prompt = 0;
    }

    if (thread.downloading == 1) {
        clientff(C_D " (%d%%)" C_0, thread.progress_percent);
    }

    DEBUG("left i_mapper_process_server_prompt");
}

void i_mapper_process_server_paragraph(LINES * l)
{
    int line;

    if(!composing){

        for (line = 1; line <= l->nr_of_lines; line++) {
            set_line(line);

            if(!strcmp(" -*-  Composer  -*-                  (*help for help)",
                l->line[line])){

                composing = 1;
                break;
            }

            process_line(l->line[line], l->len[line], l);

            if ((auto_walk && (!strcmp("You cannot move that fast, slow down!", l->line[line])
                               || !strcmp("Now now, don't be so hasty!", l->line[line])
                                                       || !strcmp("You are recovering equilibrium and cannot move yet.", l->line[line])))
                            || (auto_bump && (!strcmp("There is no exit in that direction.",l->line[line])))) {
                hide_line();
                gag_next_prompt = 1;
            }
        }

        /* Is this a room? Parse it. */
        parse_room(l);
    }
    
    set_line(-1);
    i_mapper_process_server_prompt(NULL);
}

AREA_DATA *get_area_by_name(char *string, int areanumber)
{
    AREA_DATA *area = NULL, *a;
	char *p;

	DEBUG("get_area_by_name");

    if (!string[0]) {
        if (!current_area) {
            clientfv("I don't know what area are you in - walk about so we can get unlost, and try again.");
            return NULL;
        } else
            return current_area;
    }

	strcat(string,".\0");

	for (a = areas; a; a = a->next)
        if (!case_strcmp((char *)a->name, string)) {
			if (!area){
                area = a;
			}else {

				clientfv("Eep! Multiple areas with the exactly same name found!");

                return NULL;
            }
        }

	p=strchr(string, '.');
	*p=0;

	if(!area)
		for (a = areas; a; a = a->next)
			if (case_strstr((char *)a->name, string)) {
				if (!area){
					area = a;
					/* return the first found area */
					if(areanumber==1)
						break;
				}else {
					if(areanumber==0){
						/* More than one area of that name and none specified. */
						/* List them all, and return none. */

						int counter=2;

						clientfv("Multiple matches found:");

						clientff("1 - %s\r\n", area->name);
						clientff("2 - %s\r\n", a->name);
						for (area = a->next; area; area = area->next)
							if (case_strstr((char *)area->name, string)) {
								counter++;
								clientff("%d - %s\r\n", counter, area->name);
							}

						clientff("\r\n<command> <part of areaname> <number> [additional arguments] specifies the area.\r\n");

						return NULL;
					}else{

						/* More than one area of that name and one specified. */
						/* Return the area. */

						if(areanumber==2){
							return a;
						}else{
							int counter=2;
							for (area = a->next; area; area = area->next)
								if (case_strstr((char *)area->name, string)) {
									counter++;
									if(counter==areanumber)
										return area;
								}

						}

					}
				}
        }

    if (!area) {
        clientfv("No area names match that.");
        return NULL;
    }

	return area;
}

/* Map commands. */

void do_vmap(char *arg)
{
    ROOM_DATA *room;

    if (arg[0] && isdigit(arg[0])) {
        if (!(room = get_room(atoi(arg)))) {
            clientfv("No room with that vnum found.");
            return;
        } else {
            mark_room(room);
        }
    } else {
        if (current_room)
            room = current_room;
        else {
            clientfv("I don't know where we are at the moment.");
            return;
        }
    }

    show_map_new(room);
}

void do_vmap_old(char *arg)
{
    ROOM_DATA *room;

    if (arg[0] && isdigit(arg[0])) {
        if (!(room = get_room(atoi(arg)))) {
            clientfv("No room with that vnum found.");
            return;
        }
    } else {
        if (current_room)
            room = current_room;
        else {
            clientfv("I don't know where we are right now.");
            return;
        }
    }

    show_map(room);
}

void do_vmap_help(char *arg)
{
    clientfv("Module: Mapper. Commands:");
    clientf(" vm help      - This help.\r\n"
            " area help    - Area commands help.\r\n"
            " room help    - Room commands help.\r\n"
            " exit help    - Room exit commands help.\r\n"
            " vm load      - Load map.\r\n"
            " vm save      - Save map.\r\n"
            " vm none      - Disable following or mapping.\r\n"
            " vm follow    - Enable following.\r\n"
            " vm create    - Enable mapping.\r\n"
            " vm path #    - Build directions to vnum #.\r\n"
            " vm status    - Show general information about the map.\r\n"
            " vm color     - Change the color of the room title.\r\n"
            " vm file      - Set the file for map load and map save.\r\n"
            " vm teleport  - Manage global special exits.\r\n"
            " vm queue     - Show the command queue.\r\n"
            " vm queue cl  - Clear the command queue.\r\n"
            " vm config    - Configure the mapper.\r\n"
            " vm window    - Open a floating MXP-based window.\r\n"
            " vmap         - Generate a map, from the current room.\r\n"
            " vmap #       - Generate a map centered on vnum #.\r\n"
            " mark list    - Show all the landmarks, in the world.\r\n"
            " goto/mstop   - Begins, or stops speedwalking.\r\n"
            " mp           - Pauses and unpauses speedwalking.\r\n");
}

void do_vmap_edit(char *arg)
{

    /* Force an update of the floating map. */
    last_room = NULL;

    if (edit_only && mode == CREATING) {
        clientfv("We're already in the editing mode.");
    } else if (mode == CREATING && !edit_only) {
        edit_only = 1;
        clientfv("Switched from creating to editing mode.");
    } else {

        if (current_room) {

            edit_only = 1;
            mode = CREATING;
            clientfv("Editing mode enabled.");

            char error[256];
            current_room = create_room(-1, error, 256);
            if (!current_room) {
                clientff("\r\n");
                clientfv(error);
                clientfv("Switching back to following mode.");
                mode = FOLLOWING;
                return;
            }

            clientfv("Please do 'ql' to update this room.");

            if (world == NULL) {
                create_area();
                current_area = areas;
                current_area->name = strdup("New area");
            }

            current_room->name = strdup("New room.");
            q_top = 0;

            /* Enviroment not set? Update automatically. */
            if (current_room && current_room->room_type == null_room_type)
                send_to_server("survey\r\n");

			/* Indoors not set? Update automatically. */
            if (current_room && current_room->indoors == 2)
               if(current_room){
                  current_room->indoors = indoors;
               }

        } else {
            clientfv("We're lost right now. Walk about to find ourselves first before mapping.");
            clientfv("In case it's really what you need, use 'vmap edit newroom'.");
            return;
        }
    }
}

void do_vmap_create(char *arg)
{
    /* Force an update of the floating map. */
    last_room = NULL;

    if (edit_only) {
        clientfv("Switched from editing mode to creating.");
        edit_only = 0;
    }

    if (!strcmp(arg, "newroom")) {
        clientfv("Mapping on. Please 'look', to update this room.");

        if (world == NULL) {
            create_area();
            current_area = areas;
            current_area->name = strdup("New area");
        }

        char error[256];
        current_room = create_room(-1, error, 256);
        if (!current_room) {
            clientff("\r\n");
            clientfv(error);
            return;
        }

        current_room->name = strdup("New room.");
        q_top = 0;
    } else if (!current_room) {
        clientfv("We're lost right now. Walk about to find ourselves first before mapping.");
        clientfv("In case it's really what you need, use 'vmap create newroom' (to start a whole new map).");
        return;
    } else {
        clientfv("Mapping on.");
        /* Enviroment not set? Update automatically. */
        if (current_room && current_room->room_type == null_room_type)
            send_to_server("survey\r\n");
		/* Indoors not set? Update automatically. */
        if (current_room && current_room->indoors == 2)
           if(current_room){
              current_room->indoors = indoors;
           }
    }

    mode = CREATING;
}

void do_vmap_follow(char *arg)
{
    /* Force an update of the floating map. */
    last_room = NULL;

    if (!current_room) {
        mode = GET_UNLOST;
        clientfv("Will try to find ourselves on the map.");
        add_queue(-1);
        send_to_server("ql\r\n");
    } else if (mode == FOLLOWING) {
        clientfv("We're already tracking!");
    } else {
        mode = FOLLOWING;
        clientfv("Tracking enabled.");
    }
}

void do_vmap_none(char *arg)
{
    if (mode == CREATING) {
        mode = FOLLOWING;
        clientfv("Mapping mode disabled, will be tracking now.");
    } else if (mode == FOLLOWING) {
        mode = NONE;
        clientfv("Tracking mode disabled.");
    } else if (mode == GET_UNLOST) {
        mode = NONE;
        clientfv("Won't try and find ourselves anymore.");
    } else
        clientfv("The mapper isn't mapping or tracking already.");

}

void do_vmap_off(char *arg)
{
    if (mode == CREATING) {
        mode = FOLLOWING;
        clientfv("Mapping mode disabled, will be tracking now.");
    } else if (mode == FOLLOWING) {
        mode = NONE;
        clientfv("Tracking mode disabled.");
    } else if (mode == GET_UNLOST) {
        mode = NONE;
        clientfv("Won't try and find ourselves anymore.");
    } else
        clientfv("The mapper isn't mapping or tracking already.");

}

void do_vmap_save(char *arg)
{
    if (!strcmp(arg, "binary")) {
        save_binary_map(map_file_bin);
        return;
    }

    if (areas)
        save_map(map_file);
    else
        clientfv("No areas to save.");
}

void do_vmap_load(char *arg)
{
    char buf[256];

    if (!strncmp("partial", arg, 7)) {
        char *buf1, *buf2;
        char buf23[256];
        buf1 = strtok(arg, " ");
        buf1 = strtok(NULL, " ");
        buf2 = strtok(NULL, " ");
        strcpy(buf23, buf2);
        clientff("  -%s-  -%s-\r\n", buf1, buf23);
        if (!buf1[0]) {
            clientfv("No area name to load specified.");
            return;
        } else if (!buf2[0]) {
            clientfv("No file name to load given.");
            return;
        } else {
            strcat(buf1, ".");

            AREA_DATA *area, *next_area;

            for (area = areas; area; area = area->next) {
                next_area = area->next;
                if (!strcmp(buf1, area->name)) {
                    //clientff("Deleting '%s'.\r\n", area->name);
                    destroy_area(area);
                }
            }
            clientff("Okey, buf23 2 is -%s-\r\n", buf23);
            clientff("Loading \"%s\".\r\n", buf1);
        }
        clientff("Okey, buf23 3 is -%s-\r\n", buf23);
        if (load_map(buf23, buf1)) {
            //destroy_map( );
            clientfv("Couldn't load map.");
            do_vmap_update("");
        } else {
            g_snprintf(buf, sizeof(buf), "Partial map loaded. (%d miliseconds)", get_timer() / 1000);
            clientfv(buf);
            convert_vnum_exits();
            g_snprintf(buf, sizeof(buf), "Vnum exits converted. (%d miliseconds)", get_timer() / 1000);
            clientfv(buf);
            check_map();
            //load_settings( "config.mapper.txt" );

            mode = GET_UNLOST;
        }

        return;
    }

    destroy_map();
    get_timer();
    if (!strcmp(arg, "binary") ? load_binary_map(map_file_bin) : load_map(map_file, "none")) {
        do_vmap_update("");
        //destroy_map();
        clientfv("Couldn't load map.");
    } else {
        g_snprintf(buf, sizeof(buf), "Map loaded. (%d ms)", get_timer() / 1000);
        clientfv(buf);
        convert_vnum_exits();
        g_snprintf(buf, sizeof(buf), "Vnum exits converted. (%d ms)", get_timer() / 1000);
        clientfv(buf);
        check_map();
        load_settings("config.mapper.txt");

        mode = GET_UNLOST;
    }

    clear_recycle_bin();
    fill_recycle_bin();
}

void do_vmap_backpath(char *arg)
{
    ROOM_DATA *room = NULL;
    ROOM_DATA *temp_room = current_room;
    ELEMENT *tag;
    char buf[256];

    /* Force an update of the floating map. */
    last_room = NULL;

    if (!current_room) {
        clientfv("We're currently lost. Need to find out where we are (ql, walk about) first.");
        return;
    }

    if (!arg[0]) {
        clientfv("Where would you like to show the path to?");
        return;
    }

    clean_paths();

    while (arg[0]) {
        int neari = 0;

        arg = get_string(arg, buf, 256);

        if (!strcmp(buf, "near")) {
            arg = get_string(arg, buf, 256);
            neari = 1;
        }

        /* Vnum. */
        if (isdigit(buf[0])) {
            if (!(room = get_room(atoi(buf)))) {
                clientff(C_R "[No room with the vnum '%s' was found.]\r\n" C_0, buf);
                clean_paths();
                return;
            }

            current_room = room;

            if (!neari) {
                add_to_pathfinder(temp_room, 1);
            } else {
                for (neari = 1; dir_name[neari]; neari++)
                    if (temp_room->exits[neari]) {
                        add_to_pathfinder(temp_room->exits[neari], 1);
                    }
            }

        }
        /* Mark or area. */
        else {

            if (!strcmp(buf, "mark")) {
                arg = get_string(arg, buf, 256);

                int found = 0;

                current_room = room;

                for (room = world; room; room = room->next_in_world)
                    for (tag = room->tags; tag; tag = tag->next)
                        if (!case_strcmp(buf, (char *) tag->p)) {
                            if (!neari) {
                                add_to_pathfinder(temp_room, 1);
                            } else {
                                for (neari = 1; dir_name[neari]; neari++)
                                    if (room->exits[neari]) {
                                        add_to_pathfinder(room->exits[neari], 1);
                                    }
                            }
                            found = 1;
                        }

                if (!found) {
                    clientff(C_W "[" C_B "Map" C_W "]: No room with the mark '%s' was found.\r\n" C_0, buf);
                    clean_paths();
                    return;
                }

                /* area */
            } else if (!strcmp(buf, "area")) {

                int found = 0, first = 0;
                clean_paths();

                for (room = world; room; room = room->next_in_world) {
                    if (case_strstr((char *) room->area->name, arg)) {
                        if (case_strstr((char *) room->area->name, current_area->name)) {
                            int len = strlen(current_area->name);
                            char buf[len];
                            strcpy(buf, current_area->name);
                            buf[len - 1] = '\0';
                            clientff(C_W "[" C_B "Map" C_W "]: We're already in " C_D "%s" C_W
                                     " though.\r\n" C_0, buf);
                            return;
                        }

                        if (!first)
                            first = 1;

                        add_to_pathfinder(temp_room, 1);
                        found = 1;
                    }

                }
                if (!found) {
                    clientfv("Can't find that area on the map.");
                    clean_paths();
                    return;
                }

            }

        }
    }

    get_timer();
    path_finder_2();

    if (current_room)
        show_backpath(current_room);
    else
        clientfv("I don't know where we are, so I can't show a path.");

    current_room = temp_room;
}

void do_vmap_path(char *arg)
{
    ROOM_DATA *room;
    ELEMENT *tag;
    char buf[256];

    /* Force an update of the floating map. */
    last_room = NULL;

    if (!current_room) {
        clientfv("We're currently lost. Need to find out where we are (ql, walk about) first.");
        return;
    }

    clean_paths();

    if (!arg[0]) {
        clientfv("Where do you want to show the path to?");
        return;
    }

    int neari = 0;

    arg = get_string(arg, buf, 256);

    if (!strcmp(buf, "near")) {
        arg = get_string(arg, buf, 256);
        neari = 1;
    }

    /* Vnum. */
    if (isdigit(buf[0])) {
        if (!(room = get_room(atoi(buf)))) {
            clientff(C_R "[No room with the vnum '%s' was found.]\r\n" C_0, buf);
            clean_paths();
            return;
        }

        if (!neari) {
            add_to_pathfinder(room, 1);
        } else {
            for (neari = 1; dir_name[neari]; neari++)
                if (room->exits[neari]) {
                    add_to_pathfinder(room->exits[neari], 1);
                }
        }

    }
    /* mark or area */
    else {

        if (!strcmp(buf, "mark")) {
            arg = get_string(arg, buf, 256);

            int found = 0;

            for (room = world; room; room = room->next_in_world)
                for (tag = room->tags; tag; tag = tag->next)
                    if (!case_strcmp(buf, (char *) tag->p)) {
                        if (!neari) {
                            add_to_pathfinder(room, 1);
                        } else {
                            for (neari = 1; dir_name[neari]; neari++)
                                if (room->exits[neari]) {
                                    add_to_pathfinder(room->exits[neari], 1);
                                }
                        }
                        found = 1;
                    }

            if (!found) {
                clientff(C_W "[" C_B "Map" C_W "]: No room with the mark '%s' was found.\r\n" C_0, buf);
                clean_paths();
                return;
            }

            /* area */
        } else if (!strcmp(buf, "area")) {

            int found = 0, first = 0;
            clean_paths();

            for (room = world; room; room = room->next_in_world) {
                if (case_strstr((char *) room->area->name, arg)) {
                    if (case_strstr((char *) room->area->name, current_area->name)) {
                        int len = strlen(current_area->name);
                        char buf[len];
                        strcpy(buf, current_area->name);
                        buf[len - 1] = '\0';
                        clientff(C_W "[" C_B "Map" C_W "]: We're already in " C_D "%s" C_W
                                 " though.\r\n" C_0, buf);
                        return;
                    }

                    if (!first)
                        first = 1;

                    add_to_pathfinder(room, 1);
                    found = 1;
                }

            }
            if (!found) {
                clientfv("Can't find that area on the map.");
                clean_paths();
                return;
            }

        }

    }

    path_finder_2();

    if (current_room)
        show_path(current_room);
    else
        clientfv("I don't know where we are, so I can't show a path.");
}

void print_recycled_room(gpointer data, gpointer user_data)
{
    int x = *(int *) data;

    clientff(C_g "%d" C_D ", ", x);
}

void count_recycled_room(gpointer data, gpointer user_data)
{
    recycled_rooms_count++;
}

int compare_recycled_rooms(gpointer data, gpointer user_data)
{
    int casted_data = *(int *) data;
    int casted_user_data = *(int *) user_data;

    if(casted_data==casted_user_data)
        return 0;
    else
        return 1;
} 

void do_vmap_status(char *arg)
{
    AREA_DATA *a;
    ROOM_DATA *r;
    char buf[256];
    int count = 0, count2 = 0;
    int i;

    DEBUG("do_vmap_status");

    clientfv("General information:");

    g_snprintf(buf, sizeof(buf), "Your map version: " C_G "%d" C_0 ", mapper version: " C_G "%d" C_0 "." C_G "%d" C_0 "." C_G "%d" ".\r\n",
            map_version, mapper_version_major, mapper_version_minor, mapper_version_bugfix);
    clientf(buf);

    for (r = world; r; r = r->next_in_world)
        count++, count2 += sizeof(ROOM_DATA);

    g_snprintf(buf, sizeof(buf), "Rooms: " C_G "%d" C_0 ", using " C_G "%.3G" C_0 " MB of memory.\r\n", count,
            count2 * .000000953674316);
    clientf(buf);

    count = 0, count2 = 0;

    for (a = areas; a; a = a->next) {
        int notype = 0, unlinked = 0;
        count++;

        for (r = a->rooms; r; r = r->next_in_area) {
            if (!notype && r->room_type == null_room_type)
                notype = 1;
            if (!unlinked)
                for (i = 1; dir_name[i]; i++)
                    if (!r->exits[i] && r->detected_exits[i] && !r->locked_exits[i])
                        unlinked = 1;
        }

        if (!(notype || unlinked))
            count2++;
    }

    g_snprintf(buf, sizeof(buf), "Areas: " C_G "%d" C_0 ", of which fully mapped: " C_G "%d" C_0 ".\r\n", count, count2);
    clientf(buf);

/*
    g_snprintf(buf, sizeof(buf), "Room structure size: " C_G "%d" C_0 " bytes.\r\n", (int) sizeof(ROOM_DATA));
    clientf(buf);

    g_snprintf(buf, sizeof(buf), "Hash table size: " C_G "%d" C_0 " chains.\r\n", MAX_HASH);
    clientf(buf);
*/

    g_snprintf(buf, sizeof(buf), "Map size, x: " C_G "%d" C_0 " y: " C_G "%d" C_0 ".\r\n", MAP_X, MAP_Y);
    clientf(buf);

/*
    for (i = 0; color_names[i].name; i++) {
        if (!strcmp(color_names[i].code, room_color))
            break;
    }

    g_snprintf(buf, sizeof(buf), "Room title color: %s%s" C_0 ", code length " C_G "%d" C_0 ".\r\n", room_color,
            color_names[i].name ? color_names[i].name : "unknown", room_color_len);

    clientf(buf);
*/
    clientff("\r\n");
    clientfv("Other information:");

    g_snprintf(buf, sizeof(buf), "Latest room number: " C_G "%d" C_0 ".\r\n", last_vnum);
    clientf(buf);
    g_snprintf(buf, sizeof(buf), "Amount of rooms in recycle bin: " C_G);
    clientf(buf);

    if (g_queue_is_empty(recycle_bin))
        clientff(C_D "(none)" C_0);
    else {
        recycled_rooms_count = 0;
        g_queue_foreach(recycle_bin, (GFunc) count_recycled_room, NULL);
        clientff(C_D "%d" C_0, recycled_rooms_count);
    }

    clientff("\r\n" C_g);

}

void do_vmap_recycle_list(char *arg)
{
    clientff("Rooms in recycle bin: " C_G);

    if (g_queue_is_empty(recycle_bin))
        clientff(C_D "(none)" C_0);
    else {
        recycled_rooms_count = 0;
        g_queue_foreach(recycle_bin, (GFunc) print_recycled_room, NULL);
    }

    clientff(C_0 "\r\n");
}

void do_vmap_color(char *arg)
{
    char buf[256];
    int i;

    arg = get_string(arg, buf, 256);

    for (i = 0; color_names[i].name; i++) {
        if (!strcmp(color_names[i].name, buf)) {
            room_color = color_names[i].code;
            room_color_len = color_names[i].length;

            g_snprintf(buf, sizeof(buf), "Room title color changed to: " C_0 "%s%s" C_R ".", room_color, color_names[i].name);
            clientfv(buf);
            clientfv("Use 'vmap config save' to make it permanent.");

            return;
        }
    }

    clientfv("Possible room-title colors:");

    for (i = 0; color_names[i].name; i++) {
        g_snprintf(buf, sizeof(buf), "%s - " C_0 "%s%s" C_0 ".\r\n",
                !strcmp(color_names[i].code, room_color) ? C_R : "", color_names[i].code,
                color_names[i].name);
        clientf(buf);
    }

    clientfv("Use 'vmap color <name>' to change it.");
}

void do_vmap_bump(char *arg)
{
    if (!current_room) {
        clientfv("I don't know where we are right now.");
        return;
    }

    if (!strcmp(arg, "skip")) {
        if (bump_room && bump_room->next_in_area) {
            bump_room = bump_room->next_in_area;
            auto_bump = 3;
            clientfv("Skipped one room.");
        } else {
            clientfv("Skipping a room is not possible.");
            return;
        }
    }

    else if (!strcmp(arg, "continue")) {
        if (bump_room) {
            clientfv("Bumping continues.");

            auto_bump = 3;
        } else {
            clientfv("Unable to continue bumping.");
            return;
        }
    }

    else if (!strcmp(arg, "stop") || auto_bump) {
        auto_bump = 0;
        clientfv("Bumping ended.");
        return;
    }

    else {
        clientfv("Bumping begins.");

        auto_bump = 3;
        bump_room = current_room->area->rooms;
    }

    clean_paths();
    add_to_pathfinder(bump_room, 1);
    path_finder_2();
    go_next();
}

void do_vmap_queue(char *arg)
{
    char buf[256];
    int i;

    if (arg[0] == 'c') {
        q_top = 0;
        clientfv("Queue cleared.");
        del_timer("queue_reset_timer");
        return;
    }

    if (q_top) {
        clientfv("Command queue:");

        for (i = 0; i < q_top; i++) {
            g_snprintf(buf, sizeof(buf), C_R " %d: %s.\r\n" C_0, i, queue[i] < 0 ? "look" : dir_name[queue[i]]);
            clientf(buf);
        }
    } else
        clientfv("Queue empty.");
}

void do_vmap_config(char *arg)
{
    char option[256];
    int i;
    struct config_option {
        char *option;
        int *value;
        char *true_message;
        char *false_message;
    } options[] = {
        {
        "swim", &disable_swimming, "Swimming disabled - will now walk over water.",
                "Swimming enabled."}, {
        "autoupdate", &auto_update, "Will check for map updates at system start.",
                "Won't auto-check for map updates at system start."}, {
        "wholist", &disable_wholist, "Parsing disabled.", "Parsing enabled"}, {
        "alertness", &disable_alertness, "Parsing disabled.", "Parsing enabled"}, {
        "locate", &disable_locating, "Locating disabled.",
                "Vnums will now be displayed on locating abilities."}, {
        "showarea", &disable_areaname, "The area name will no longer be shown.",
                "The area name will be shown after room titles."}, {
        "title_mxp", &disable_mxp_title, "MXP tags will no longer be used to mark room titles.",
                "MXP tags will be used around room title."}, {
        "exits_mxp", &disable_mxp_exits, "MXP tags will no longer be used to mark room exits.",
                "MXP tags will be used around room exits."}, {
        "map_mxp", &disable_mxp_map, "MXP tags will no longer be used on a map.",
                "MXP tags will now be used on a map."}, {
        "autolink", &disable_autolink, "Auto-linking disabled.", "Auto-linking enabled."}, {
        "sewer_grates", &disable_sewer_grates, "Pathfinding through sewer grates disabled.",
                "Pathfinding through sewer grates enabled."},{ 
         "mhal_sewer_grates", &disable_mhaldorian_sewer_grates, "Pathfinding through Mhaldorian sewer grates disabled.",
            "Pathfinding through Mhaldorian sewer grates enabled."},{
        "auto_pear", &auto_pear, "Auto-use of pear is enabled.",
            "Auto-use of pear is disabled."},{
        "god_rooms", &enable_godrooms, "Parsing titles with GodRooms enabled.",
                "Parsing titles with GodRooms disabled."},{
        "icon_travel", &disable_icon_travel, "Pathfinding through Icons disabled.",
            "Pathfinding through Icons enabled."},{
        "warp_travel", &disable_warp_travel, "Pathfinding through warps disabled.",
           "Pathfinding through warps enabled."},{
        "duanathar", &disable_duanathar, "Not using Duanathar.",
            "Using Duanathar."},{
        "highlight_indoors", &highlight_indoors, "Highlighting indoor rooms now.",
            "Not highlighting indoor rooms anymore."},{
        "alert_wormhole", &alert_wormhole, "Alerting wormholes now.",
            "Not alerting wormholes anymore."},{
        NULL, NULL, NULL, NULL}
    };

    arg = get_string(arg, option, 256);

    if (!strcmp(option, "save")) {
        if (save_settings("config.mapper.txt"))
            clientfv("Unable to open the file for writing.");
        else
            clientfv("All settings saved.");
        return;
    } else if (!strcmp(option, "load")) {
        if (load_settings("config.mapper.txt"))
            clientfv("Unable to open the file for reading.");
        else
            clientfv("All settings reloaded.");
        return;
    } else if(!strcmp(option, "scry_command")){
        strcpy(scry_command, arg);
        clientff(C_W"Set the scry_command to '" C_R "%s" C_W "'.\r\n" C_0, scry_command);
        strcat(scry_command, " ");
        return;
    } else if(!strcmp(option, "celerity")){	
        celerity = atoi(arg);
        clientff(C_W"Your celerity level is now '" C_R "%d" C_W "'.\r\n" C_0, celerity);    
        return;
    }

    if (option[0]) {
        for (i = 0; options[i].option; i++) {
            if (strcmp(options[i].option, option))
                continue;

            *options[i].value = *options[i].value ? 0 : 1;
            if (*options[i].value)
                clientfv(options[i].true_message);
            else
                clientfv(options[i].false_message);

        if (save_settings("config.mapper.txt"))
            clientfv("Unable to open the settings file for writing.");

            return;
        }
    }

    clientfv("Available " C_g "vm config" C_W " commands:");
    clientf(" vm config save              - Save all settings.\r\n"
            " vm config load              - Reload the previously saved settings.\r\n"
            " vm config swim              - Toggle swimming.\r\n"
            " vm config wholist           - Parse and replace the 'who' list.\r\n"
            " vm config alertness         - Parse and replace alertness messages.\r\n"
            " vm config locate            - Append vnums to various locating abilities.\r\n"
            " vm config showarea          - Show the current area after a room title.\r\n"
            " vm config title_mxp         - Mark the room title with MXP tags.\r\n"
            " vm config exits_mxp         - Mark the room exits with MXP tags.\r\n"
            " vm config map_mxp           - Use MXP tags on map generation.\r\n"
            " vm config autolink          - Link rooms automatically when mapping.\r\n"
            " vm config mhal_sewer_grates - Toggle pathfinding through Mhaldorian sewer\r\n"
            "                               grates.\r\n"
            " vm config sewer_grates      - Toggle pathfinding through sewer grates.\r\n"
            " vm config autoupdate        - Automatically check for map updates at system\r\n"
            "                               start.\r\n"
            " vm config auto_pear         - Disable if you have the fishscale tunic artefact.\r\n"
            " vm config icon_travel       - Toggle pathfinding through Icons.\r\n"
            " vm config warp_travel       - Toggle pathfinding through warps.\r\n"
            " vm config duanathar         - Toggle the use of Duanathar.\r\n"
            " vm config highlight_indoors - Toggle the highlighting of indoor rooms.\r\n"
            " vm config alert_wormhole    - Shows wormhole in your location.\r\n"
            " vm config scry_command <com>- Sets the command used with goto person.\r\n"
            " vm config celerity <0|1|2>  - Sets your level of celerity.\r\n");
}

void do_vmap_orig(char *arg)
{
    ROOM_DATA *room, *r;
    int rooms = 0, original_rooms = 0;

    for (room = world; room; room = room->next_in_world) {
        /* Add one to the room count. */
        rooms++;

        /* And check if there is any other room with this same name. */
        original_rooms++;
        for (r = world; r; r = r->next_in_world)
            if (!strcmp(r->name, room->name) && r != room) {
                original_rooms--;
                break;
            }
    }

    if (!rooms) {
        clientfv("The area is empty...");
        return;
    }

    clientff(C_R "[Rooms: " C_G "%d" C_R "  Original rooms: " C_G "%d" C_R "  Originality: " C_G
             "%d%%" C_R "]\r\n" C_0, rooms, original_rooms,
             original_rooms ? original_rooms * 100 / rooms : 0);
}

void do_vmap_file(char *arg)
{
    if (!arg[0]) {
        strcpy(map_file, "IMap");
        strcpy(map_file_bin, "IMap.bin");
    } else {
        strcpy(map_file, arg);
        strcpy(map_file_bin, arg);
        strcat(map_file_bin, ".bin");
    }

    clientff(C_R "[File for map load/save set to '%s'.]\r\n" C_0, map_file);
}

/* Snatched from do_exit_special. */
void do_vmap_teleport(char *arg)
{
    EXIT_DATA *spexit;
    char cmd[256];
    char buf[256];
    int i, nr = 0;

    DEBUG("do_vmap_teleport");

    if (mode != CREATING && strcmp(arg, "list")) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!strncmp(arg, "help", strlen(arg))) {
        clientfv("Syntax: map teleport <command> [exit] [args]");
        clientfv("Commands: list, add, create, destroy, name, message, link");
        return;
    }

    arg = get_string(arg, cmd, 256);

    if (!strcmp(cmd, "list")) {
        clientfv("Global special exits:");
        for (spexit = global_special_exits; spexit; spexit = spexit->next) {
            g_snprintf(buf, sizeof(buf),
                    C_B "%3d" C_0 " - L: " C_G "%4d" C_0 " N: '" C_g "%s" C_0 "' M: '" C_g "%s" C_0
					"'\r\n", nr++, spexit->vnum, spexit->command ? spexit->command : "", spexit->message ? spexit->message : "");
            clientf(buf);
        }

        if (!nr)
            clientf(" - None.\r\n");
    }

    else if (!strcmp(cmd, "add")) {
        char name[256];

        arg = get_string(arg, name, 256);

        if (!arg[0]) {
            clientfv("Syntax: map teleport add <name> <message>");
            return;
        }

        spexit = create_exit(NULL);
        spexit->command = strdup(name);
        spexit->message = strdup(arg);

        clientfv("Global special exit created.");
    }

    else if (!strcmp(cmd, "create")) {
        create_exit(NULL);
        clientfv("Global special exit created.");
    }

    else if (!strcmp(cmd, "destroy")) {
        int done = 0;

        if (!arg[0] || !isdigit(arg[0])) {
            clientfv("What global special exit do you wish to destroy?");
            return;
        }

        nr = atoi(arg);

        for (i = 0, spexit = global_special_exits; spexit; spexit = spexit->next) {
            if (i++ == nr) {
                destroy_exit(spexit);
                done = 1;
                break;
            }
        }

        if (done)
            g_snprintf(buf, sizeof(buf), "Global special exit %d destroyed.", nr);
        else
            g_snprintf(buf, sizeof(buf), "Global special exit %d was not found.", nr);
        clientfv(buf);
    }

    else if (!strcmp(cmd, "message")) {
        int done = 0;

        /* We'll store the number in cmd. */
        arg = get_string(arg, cmd, 256);

        if (!isdigit(cmd[0])) {
            clientfv("Set message on what global special exit?");
            return;
        }

        nr = atoi(cmd);

        for (i = 0, spexit = global_special_exits; spexit; spexit = spexit->next) {
            if (i++ == nr) {
                if (spexit->message)
                    free(spexit->message);

                spexit->message = strdup(arg);
                done = 1;
                break;
            }
        }

        if (done)
            g_snprintf(buf, sizeof(buf), "Message on global exit %d changed to '%s'", nr, arg);
        else
            g_snprintf(buf, sizeof(buf), "Can't find global special exit %d.", nr);

        clientfv(buf);
    }

    else if (!strcmp(cmd, "name")) {
        int done = 0;

        /* We'll store the number in cmd. */
        arg = get_string(arg, cmd, 256);

        if (!isdigit(cmd[0])) {
            clientfv("Set name on what global special exit?");
            return;
        }

        nr = atoi(cmd);

        for (i = 0, spexit = global_special_exits; spexit; spexit = spexit->next) {
            if (i++ == nr) {
                if (spexit->command)
                    free(spexit->command);

                if (arg[0])
                    spexit->command = strdup(arg);
                done = 1;
                break;
            }
        }

        if (done) {
            if (arg[0])
                g_snprintf(buf, sizeof(buf), "Name on global exit %d changed to '%s'", nr, arg);
            else
                g_snprintf(buf, sizeof(buf), "Name on global exit %d cleared.", nr);
        } else
            g_snprintf(buf, sizeof(buf), "Can't find global special exit %d.", nr);

        clientfv(buf);
    }

    else if (!strcmp(cmd, "link")) {
        /* E special link 0 -1 */
        char vnum[256];

        /* We'll store the number in cmd. */
        arg = get_string(arg, cmd, 256);

        if (!isdigit(cmd[0])) {
            clientfv("Link which global special exit?");
            return;
        }

        nr = atoi(cmd);

        for (i = 0, spexit = global_special_exits; spexit; spexit = spexit->next) {
            if (i++ == nr) {
                get_string(arg, vnum, 256);

                if (vnum[0] == '-') {
                    ROOM_DATA *to;

                    to = spexit->to;

                    spexit->vnum = -1;
                    spexit->to = NULL;

                    g_snprintf(buf, sizeof(buf), "Link cleared on exit %d.", nr);
                    clientfv(buf);
                    return;
                } else if (isdigit(vnum[0])) {
                    spexit->vnum = atoi(vnum);
                    spexit->to = get_room(spexit->vnum);
                    if (!spexit->to) {
                        clientfv("A room whith that vnum was not found.");
                        spexit->vnum = -1;
                        return;
                    }
                    g_snprintf(buf, sizeof(buf), "Global special exit %d linked to '%s'.", nr, spexit->to->name);
                    clientfv(buf);
                    return;
                } else {
                    clientfv("Link to which vnum?");
                    return;
                }
            }
        }

        g_snprintf(buf, sizeof(buf), "Can't find global special exit %d.", nr);
        clientfv(buf);
    }

    else {
        clientfv("Unknown command... Try 'vmap teleport help'.");
    }
}

void do_vmap_window(char *arg)
{
    if (!arg[0]) {
        clientfv("What type of a map window do you want? You can use a 'normal' or an 'mxp' one.");
        return;
    }

    if (!strcmp(arg, "normal")) {
        if (floating_normal_map_enabled) {
            floating_normal_map_enabled = 0;
            clientfv("normal map disabled. Don't forget to close the window!");
        } else {
            floating_normal_map_enabled = 1;
            clientfv("Normal window map enabled, enjoy.");
        }

        show_floating_map(current_room);
    } else if (!strcmp(arg, "mxp")) {
        if (floating_map_enabled) {
            floating_map_enabled = 0;
            clientfv("mxp map disabled. Don't forget to close the window!");
            return;
        }

        if (!mxp_tag(TAG_LOCK_SECURE)) {
            floating_map_enabled = 0;
            clientfv("Can't get the mxp map up - maybe your client doesn't support MXP?");
            return;
        }

        floating_map_enabled = 1;
        mxp("<FRAME \"Mapper Window\" Left=\"-57c\" Top=\"2c\" Height=\"22c\""
            " Width=\"55c\" FLOATING SCROLLING=\"no\" >");
        mxp("<DEST \"Mapper Window\" X=0 Y=0>");
        mxp_tag(TAG_LOCK_SECURE);
        mxp("<!element mpelm '<send \"goto &v;|room look &v;\" "
            "hint=\"&r;|Vnum: &v;|Type: &t;\">' ATT='v r t'>");
        mxp("<!element mppers '<send \"goto &v;|room look &v;|qw &p;\" "
            "hint=\"&p; (&r;)|Vnum: &v;|Type: &t;|Player: &p;\">' ATT='v r t p'>");
        mxp("<!ELEMENT custom_alias '<send href=\"%s\">'>", custom_alias);
        mxp("</DEST>");
        mxp_tag(TAG_DEFAULT);
        show_floating_map(current_room);
        clientfv("Floating mxp map enabled, enjoy.");
    } else {
        clientfv("What type of a map window do you want? You can use a 'normal' or an 'mxp' one.");
    }
}

/* Area commands. */

void do_area_help(char *arg)
{
    clientfv("Module: Mapper. Area commands:");
    clientf(" area list         - List all areas, or a given pattern.\r\n"
            " area check        - Check for problems in the current areas.\r\n"
            " area switch       - Switch the area of the current room.\r\n"
            " area off <ar>     - Toggle pathfinding in the given area.\r\n"
            " area orig         - Check the originality of the area.\r\n"
            " area info         - List all rooms in the given area.\r\n"
            " area destroy <ar> - Destroy a given area.\r\n" );
}

// This could be cleaned up and beautified.
void do_area_check(char *arg)
{
    ROOM_DATA *room;
    AREA_DATA *area=NULL;
    char buf[256];
    int detected_only = 0, unknown_type = 0;
    int rooms = 0, i;

    DEBUG("do_area_check");
    if(!strcmp(arg,"")){

      if (!current_area) {
          clientfv("I don't know what area are you in - walk about so we can get unlost, and try again.");
          return;
      }

      area=current_area;
    }else{
      for(area = areas; area; area = area->next){
        if(!case_strncmp(arg, area->name,strlen(arg))){
          break;
        }
      }
      if(!area){
        clientfv("Area not fount.");
        return;
      }
    }

    clientfv("Unlinked or no-environment rooms:");

    for (room = area->rooms; room; room = room->next_in_area) {
        int unlinked = 0, notype = 0, locked = 0;

        if (room->room_type == null_room_type)
            notype = 1;
        for (i = 1; dir_name[i]; i++)
            if (!room->exits[i] && room->detected_exits[i]) {
                if (room->locked_exits[i])
                    locked = 1;
                else
                    unlinked = 1;
            }

        if (unlinked || notype) {
            g_snprintf(buf, sizeof(buf), " - %s (" C_G "%d" C_0 ")", room->name, room->vnum);
            if (unlinked)
                strcat(buf, C_B " (" C_G "has unlinked exits" C_B ")" C_0);
            else if (locked)
                strcat(buf, C_B " (" C_G "locked" C_B ")" C_0);
            if (notype)
                strcat(buf, C_B " (" C_G "no enviroment set" C_B ")" C_0);
            strcat(buf, "\r\n");
            clientf(buf);
            rooms++;
        }
    }
    if (!rooms)
        clientff(" - None, the area is complete.\r\n");

    rooms = 0;

    g_snprintf(buf, sizeof(buf), "Area: %s", area->name);
    clientfv(buf);

    for (room = area->rooms; room; room = room->next_in_area) {
        rooms++;

        if (room->room_type == null_room_type)
            unknown_type++;

        for (i = 1; dir_name[i]; i++)
            if (room->detected_exits[i] && !room->exits[i] && !room->locked_exits[i])
                detected_only++;
    }

    g_snprintf(buf, sizeof(buf),
            C_R "Rooms: " C_G "%d" C_R "  Unlinked exits: " C_G "%d" C_R "  Unknown type rooms: "
            C_G "%d" C_R ".", rooms, detected_only, unknown_type);
    clientfv(buf);
}

void do_area_orig(char *arg)
{
    ROOM_DATA *room, *r;
    int rooms = 0, original_rooms = 0;

    if (!current_area) {
        clientfv("I don't know what area are you in - walk about so we can get unlost, and try again.");
        return;
    }

    for (room = current_area->rooms; room; room = room->next_in_area) {
        /* Add one to the room count. */
        rooms++;

        /* And check if there is any other room with this same name. */
        original_rooms++;
        for (r = current_area->rooms; r; r = r->next_in_area)
            if (!strcmp(r->name, room->name) && r != room) {
                original_rooms--;
                break;
            }
    }

    if (!rooms) {
        clientfv("The area is empty...");
        return;
    }

    clientff(C_R "[Rooms: " C_G "%d" C_R "  Original rooms: " C_G "%d" C_R "  Originality: " C_G
             "%d%%" C_R "]\r\n" C_0, rooms, original_rooms,
             original_rooms ? original_rooms * 100 / rooms : 0);
}

void do_area_list(char *arg)
{
    AREA_DATA *area;
    ROOM_DATA *room;
    const int align = 37;
    char spcs[256];
    char buf[1024];
    char* areaname;
    char* partial;
    char* h;
    int space = 0;
    int right = 1;
    int i;
    int check_for=0; //0=no check, 1=complete, 2=partial, 3=off

    DEBUG("do_area_list");

    partial = strrchr(arg, ' ');
    if(!partial){
      partial=arg;
      h=partial;
    }else
      h=partial+1;
    if(partial && h){
      if(!case_strcmp(h,"complete")){
        check_for=1;
        *partial=0;
      }
      else if(!case_strcmp(h,"partial")){
        check_for = 2;
        *partial = 0 ;
      }
      else if(!case_strcmp(h,"off")){
        check_for = 3;
        *partial = 0;
      }
    }

    areaname = arg;
    if(*areaname==' ')
      areaname++;
    clientfv("Areas:");
    for (area = areas; area; area = area->next) {
        int unlinked = 0, notype = 0;
        if(areaname[0]){
            if(!case_strstr(area->name,areaname))
                continue;
        }
        for (room = area->rooms; room; room = room->next_in_area) {
            if (!notype && room->room_type == null_room_type)
                notype = 1;
            if (!unlinked)
                for (i = 1; dir_name[i]; i++)
                    if (!room->exits[i] && room->detected_exits[i] && !room->locked_exits[i])
                        unlinked = 1;
        }
        if((notype||unlinked)&&check_for==1)
          continue;
        if(!notype && !unlinked && check_for==2)
          continue;
        if(!area->disabled && check_for==3)
          continue;
        if (!right) {
            space = align - strlen(area->name);
            spcs[0] = '\r', spcs[1] = '\n', spcs[2] = 0;
            right = 1;
        } else {
            for (i = 0; i < space; i++)
                spcs[i] = ' ';
            spcs[i] = 0;
            right = 0;
        }
        clientf(spcs);

        g_snprintf(buf, sizeof(buf), " (%s%c" C_0 ") %s%s%s", (notype || unlinked) ? C_R : C_B,
                (notype
                 || unlinked) ? 'x' : '*', area == current_area ? C_W : (area->disabled ? C_D : ""),
                area->name, area == current_area ? C_0 : "");
        clientf(buf);
    }
    clientf("\r\n\r\n");
    clientf("  " C_B "*" C_0 " - Mapped entirely.                      " C_R "x" C_0
            " - Mapped partially.\r\n");
}

void do_area_switch(char *arg)
{
    AREA_DATA *area;
    char buf[256];

    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!current_room) {
        clientfv("No current room to change.");
        return;
    }

    if(arg){
      for(area=areas;area;area=area->next){
          if(!case_strncmp(arg,area->name,strlen(arg)))
              break;
      }

    }else{
      /* Move in a circular motion. */
      area = current_room->area->next;
      if (!area)
          area = areas;
    }

    if (current_room->area)
        unlink_from_area(current_room);
    link_to_area(current_room, area);
    current_area = current_room->area;
 

    g_snprintf(buf, sizeof(buf), "Area switched to '%s'.", current_area->name);
    clientfv(buf);
}

void do_area_update(char *arg)
{
    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (update_area_from_survey) {
        update_area_from_survey = 0;
        clientfv("Disabled.");
    } else {
        update_area_from_survey = 1;
        clientfv("Type survey. The current area's name will be updated.");
    }
}

void do_area_off(char *arg)
{
    AREA_DATA *area;
    char *d, number_arg[256], buf[256];
	int number;

	d=(arg+strlen(arg));
	while (d > arg && *d != ' ')
				d--;
	get_string(d, number_arg, 256);

	number=atoi(number_arg);

	if(number)
		*d=0;

	area=get_area_by_name(arg,number);

    if (!area)
        return;

    area->disabled = area->disabled ? 0 : 1;

    if (area->disabled)
        g_snprintf(buf, sizeof(buf), "Pathfinding in '%s' disabled.", area->name);
    else
        g_snprintf(buf, sizeof(buf), "Pathfinding in '%s' re-enabled.", area->name);

    clientfv(buf);
}

/* Room commands. */

void do_room_help(char *arg)
{
    clientfv("Module: Mapper. Room commands:");
    clientf(" room switch  - Switch current room to another vnum.\r\n"
            " room area    - prints the area of the current or the given room.\r\n"
            " room look    - Show info on the current room.\r\n"
            " room find    - Find all rooms that contain something in their name.\r\n"
            " room destroy - Unlink and destroy a room.\r\n"
            " room list    - List all rooms in the current area.\r\n"
            " room underw  - Set the room as underwater.\r\n"
            " room mark    - Set or clear a landmark on a vnum, or current room.\r\n"
            " room env     - Sets the rooms environment.\r\n"
            " room merge   - Combine two identical rooms into one.\r\n"
            " room number  - Change the current room number to the supplied num.\r\n");
}

void do_room_switch(char *arg)
{
    ROOM_DATA *room;
    char buf[256];

    if (!isdigit(*(arg)))
        clientfv("Specify a vnum to switch to.");
    else {
        room = get_room(atoi(arg));
        if (!room)
            clientfv("No room with that vnum was found.");
        else {
            g_snprintf(buf, sizeof(buf), "Switched to '%s' (%s).", room->name, room->area->name);
            clientfv(buf);
            current_room = room;
            current_area = room->area;
            if (mode == GET_UNLOST)
                mode = FOLLOWING;
        }
    }
}

void do_find(char *arg)
{
    ROOM_DATA *room;
    char name[256];
    char buf[256];

    if (*(arg) == 0) {
        clientfv("What room do you want to find?");
        return;
    }

    /* Looking for a room env? */
    if (!strncmp(arg, "type ", 5)) {
        ROOM_TYPE *type;

        for (type = room_types; type; type = type->next)
            if (!strcmp(type->name, arg + 5))
                break;

        if (!type)
            clientfv("What room env? Use 'room env' for a list.");
        else {
            clientfv("Rooms that match:");
            for (room = world; room; room = room->next_in_world)
                if (room->room_type == type)
                    clientff(" " C_D "(" C_G "%d" C_D ")" C_0 " %s%s%s%s\r\n", room->vnum,
                             room->name, room->area != current_area ? " (" : "",
                             room->area != current_area ? room->area->name : "",
                             room->area != current_area ? ")" : "");
        }

        return;
    }

/* Safety measure - count number of matching rooms first if needed */
    int i = -1;

    if (!strncmp(arg, "y ", 2)) {
        strcpy(name, arg + 2);
        i = 0;
    } else {
        i = 0;
        for (room = world; room; room = room->next_in_world) {
            if (case_strstr(room->name, arg)) {
                i++;
            }
        }
        /* Didn't find anything? */
        if (i == 0)
            i = -1;
        strcpy(name, arg);
    }

    if (i > room_display_limit) {
        clientff(C_W "[" C_B "Map" C_W
                 "]: %d rooms match this search! If you want to still display them all, use " C_g
                 "find y <room name>" C_W ".\r\n" C_0, i);
    } else if (i == -1) {
        clientfv("No matching rooms found.");
    } else {
        clientfv("Rooms that match:");
        for (room = world; room; room = room->next_in_world) {
            if (case_strstr(room->name, name)) {
                char buf2[56];
                g_snprintf(buf2, sizeof(buf2), C_D "(" C_G "%d" C_D ")", room->vnum);
                g_snprintf(buf, sizeof(buf), " %-28s" C_0 " %-45s%s%s%s", buf2, room->name,
                        room->area != current_area ? " (" : "",
                        room->area != current_area ? room->area->name : "",
                        room->area != current_area ? ")" : "");
                if (mode == CREATING) {
                    int i, first = 1;
                    for (i = 1; dir_name[i]; i++) {
                        if (room->exits[i] || room->detected_exits[i]) {
                            if (first) {
                                strcat(buf, C_D " (");
                                first = 0;
                            } else
                                strcat(buf, C_D ",");

                            if (room->exits[i])
                                strcat(buf, C_B);
                            else
                                strcat(buf, C_R);

                            strcat(buf, dir_small_name[i]);
                        }
                    }
                    if (!first)
                        strcat(buf, C_D ")" C_0);
                }
                strcat(buf, "\r\n");
                clientf(buf);
            }
        }
    }
}

void do_room_area(char *arg){

    ROOM_DATA *room;
    char buf[256];

    DEBUG("do_room_area");

    if (arg[0] && isdigit(arg[0])) {
        if (!(room = get_room(atoi(arg)))) {
            clientfv("No room with that vnum found.");
            return;
        }
    } else {
        if (current_room)
            room = current_room;
        else {
            clientfv("I don't know where we are right now.");
            return;
        }
    }
    g_snprintf(buf, sizeof(buf), "Area: %s\r\n",room->area->name);
    clientfv(buf);
}

void do_room_look(char *arg)
{
    ROOM_DATA *room;
    EXIT_DATA *spexit;
    char buf[256];
    int i, nr;

    if (arg[0] && isdigit(arg[0])) {
        if (!(room = get_room(atoi(arg)))) {
            clientfv("No room with that vnum found.");
            return;
        }
    } else {
        if (current_room)
            room = current_room;
        else {
            clientfv("I don't know where we are right now.");
            return;
        }
    }

    g_snprintf(buf, sizeof(buf), "Room: %s  Vnum: %d.  Area: %s", room->name ? room->name : "-null-", room->vnum,
            room->area->name);
    clientfv(buf);
    g_snprintf(buf, sizeof(buf), "Type: %s.  Underwater: %s.", room->room_type->name, room->underwater ? "Yes" : "No");
    clientfv(buf);
	g_snprintf(buf, sizeof(buf), "Indoors: %s.", room->indoors ? "Yes" : "No");
	clientfv(buf);
    if (room->tags) {
        ELEMENT *tag;

        g_snprintf(buf, sizeof(buf), "Marks:");
        for (tag = room->tags; tag; tag = tag->next) {
            strcat(buf, " ");
            strcat(buf, (char *) tag->p);
        }
        strcat(buf, ".");
        clientfv(buf);
    }

    for (i = 1; dir_name[i]; i++) {
        if (room->exits[i]) {
            char lngth[128];
            if (room->exit_length[i])
                g_snprintf(lngth, sizeof(lngth), " (%d)", room->exit_length[i]);
            else
                lngth[0] = 0;

            clientff("  " C_B "%s" C_0 ": (" C_G "%d" C_0 ") %s%s\r\n", dir_name[i],
                     room->exits[i]->vnum, room->exits[i]->name, lngth);
        } else if (room->detected_exits[i]) {
            if (room->locked_exits[i])
                clientff("  %s: locked exit.\r\n", dir_name[i]);
            else
                clientff("  %s: unlinked exit.\r\n", dir_name[i]);
        }
    }

    nr = 0;
    for (spexit = room->special_exits; spexit; spexit = spexit->next) {
        if (!nr)
            clientfv("Special exits:");
        nr++;
        buf[0]=0;
        if (spexit->cost)
            g_snprintf(buf, sizeof(buf), "(cost: %d) ", spexit->cost);
        if (spexit->alias) {
            strcat(buf, "(alias: ");
            strcat(buf, dir_name[spexit->alias]);
            strcat(buf, ") ");
        }

        clientff(C_B "%3d" C_0 ": '" C_g "%s" C_0 "' %s-> \"" C_g "%s" C_0 "\"" " -> " C_G "%d" C_0
                 ".\r\n", nr, spexit->command ? spexit->command : "n/a", buf, spexit->message, spexit->vnum);
    }

    /* Debugging v2.0 */
    {
        REVERSE_EXIT *rexit;

        if (room->rev_exits)
            clientfv("Exits leading into this room:");

        for (rexit = room->rev_exits; rexit; rexit = rexit->next){
            if(rexit->spexit && rexit->spexit->command && !strcmp(rexit->spexit->command, "'duanathar"))
                continue;
            clientff(C_B "  %s" C_0 ": (" C_G "%d" C_0 ") From %s\r\n",
                     rexit->direction ? dir_name[rexit->direction] : (rexit->spexit->command ? rexit->
                                                                      spexit->command : "??"),
                     rexit->from->vnum, rexit->from->name);
         }

    }

    /* Debugging.
       sprintf( buf, "PF-Cost: %d. PF-Direction: %s.", room->pf_cost,
       room->pf_direction ? dir_name[room->pf_direction] : "none" );
       clientfv( buf );
       sprintf( buf, "PF-Parent: %s", room->pf_parent ? room->pf_parent->name : "none" );
       clientfv( buf ); */
}

void do_room_destroy(char *arg)
{
    int i;

    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!isdigit(*(arg)))
        clientfv("Specify a room's vnum to destroy.");
    else {
        ROOM_DATA *room;

        room = get_room(atoi(arg));
        if (!room) {
            clientfv("No room with such a vnum was found.");
            return;
        }

        if (room == current_room)
            clientfv("Can't destroy the room you're currently in.");
        else {
            for (i = 1; dir_name[i]; i++)
                if (room->exits[i])
                    unlink_rooms(room, i, room->exits[i]);

            /* We don't want pointers to point in unknown locations, do we? */
            while (room->rev_exits) {
                /* Normal exits. */
                if (room->rev_exits->direction)
                    unlink_rooms(room->rev_exits->from, room->rev_exits->direction, room);
                /* Special exits. */
                else
                    destroy_exit(room->rev_exits->spexit);
            }

            /* Replaced?
               for ( i = 1; dir_name[i]; i++ )
               if ( room->exits[i] )
               set_reverse( NULL, i, room->exits[i] );

               for ( r = world; r; r = r->next_in_world )
               {
               for ( i = 1; dir_name[i]; i++ )
               if ( r->exits[i] == room )
               r->exits[i] = NULL;
               for ( e = r->special_exits; e; e = e_next )
               {
               e_next = e->next;
               if ( e->to == room )
               destroy_exit( e );
               }
               }
             */

            destroy_room(room);
            clientfv("Room unlinked and destroyed.");
        }
    }

    refind_last_vnum();
}

void do_room_list(char *arg)
{
    AREA_DATA *area;
    ROOM_DATA *room;
    char buf[256], *d, number_arg[256];
	int number;

    d=(arg+strlen(arg));
	while (d > arg && *d != ' ')
				d--;

	get_string(d, number_arg, 256);

	number=atoi(number_arg);

	if(number)
		*d=0;

	area=get_area_by_name(arg,number);

    if (!area)
        return;

    room = area->rooms;

    clientff(C_R "[Rooms in " C_B "%s" C_R "]\r\n" C_0, room->area->name);
    while (room) {
        g_snprintf(buf, sizeof(buf), " " C_D "(" C_G "%d" C_D ")" C_0 " %s\r\n", room->vnum,
                room->name ? room->name : "no name");
        clientf(buf);
        room = room->next_in_area;
    }
}

void do_room_listall(char *arg)
{
    ROOM_DATA *room;
    char buf[256];

    room = world;

    clientfv("[Rooms in the whole world:");
    while (room) {
        g_snprintf(buf, sizeof(buf), " " C_D "(" C_G "%d" C_D ")" C_0 " %s\r\n", room->vnum, room->name ? room->name : "no name");
        clientf(buf);
        room = room->next_in_world;
    }
}

void do_room_underw(char *arg)
{
    if (!current_room) {
        clientfv("I don't know where we are right now.");
        return;
    }

    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    current_room->underwater = current_room->underwater ? 0 : 1;

    if (current_room->underwater)
        clientfv("Current room set as an underwater place.");
    else
        clientfv("Current room set as normal, with a nice sky (or roof) above.");
}

void do_room_env(char *arg)
{
    ROOM_TYPE *type, *t;
    char cmd[256], *p = arg, *color, *background_color;

    background_color = NULL;

    if (!current_room) {
        clientfv("I don't know where we are right now.");
        return;
    }

    type = current_room->room_type;

    if (!arg[0]) {
        clientff(C_R "[This room's type is set as '%s%s%s" C_0 C_R "'.]\r\n"
                 "[Must swim: %s. Underwater: %s. Avoid: %s.]\r\n"
                 " Syntax: room env [colour] [swim] [underwater] [avoid]\r\n"
                 "     Or: room env list\r\n"
                 " Example: room env bright-cyan swim (for a water room)\r\n" C_0,
                 !type->color2
                 || !strcmp(type->color2, C_0) ? "" : type->color2, type->color ? type->color : C_R,
                 type->name, type->must_swim ? "yes" : "no", type->underwater ? "yes" : "no",
                 type->avoid ? "yes" : "no");
        return;
    }

    p = get_string(arg, cmd, 256);
    while (cmd[0]) {
        color = get_color(cmd);

        if (!strcmp(cmd, "list")) {
            clientfv("room envs known:");

            clientff("   %-20s Underwater  Swim Avoid\r\n", "Name");
            for (t = room_types; t; t = t->next) {
                if (t->color)
                    clientff(" - %s%s%-25s" C_0 "   %s   %s   %s" C_0 "\r\n", t->color,
                             !t->color2 || !strcmp(t->color2, C_0) ? "" : t->color2, t->name,
                             t->underwater ? C_B "yes" C_0 : C_R " no" C_0,
                             t->must_swim ? C_B "yes" C_0 : C_R " no" C_0,
                             t->avoid ? C_B "yes" C_0 : C_R " no" C_0);
                else
                    clientff(" - " C_R "%-34s" C_D "[[unset]]" C_0 "\r\n", t->name);
            }
        } else if (!strcmp(cmd, "swim") || !strcmp(cmd, "water")) {
            type->must_swim = type->must_swim ? 0 : 1;
            clientff(C_R "[Environment '%s' will %s require swimming.]\r\n" C_0, type->name,
                     type->must_swim ? "now" : C_r "no longer" C_R);
        } else if (!strcmp(cmd, "underwater")) {
            type->underwater = type->underwater ? 0 : 1;
            clientff(C_R "[Environment '%s' is %s set as underwater.]\r\n" C_0, type->name,
                     type->underwater ? "now" : C_r "no longer" C_R);
        } else if (!strcmp(cmd, "avoid")) {
            type->avoid = type->avoid ? 0 : 1;
            clientff(C_R "[Environment '%s' will %s be avoided when possible.]\r\n" C_0, type->name,
                     type->avoid ? "now" : C_r "no longer" C_R);
        } else if (color) {
            type->color = color;
            type->color2 = background_color;
            clientff(C_R "[Environment '%s' will be shown as %s%s%s" C_R ".]\r\n" C_0, type->name,
                     color, !type->color2 || !strcmp(type->color2, C_0) ? "" : type->color2, cmd);
        } else {
            clientff(C_R "[I don't know what %s means.]\r\n" C_0, cmd);
            clientfv("If you tried to set a colour, check 'vmap colour' for a list!");
        }

        p = get_string(p, cmd, 256);
    }
}

void do_room_merge(char *arg)
{
    ROOM_DATA *r, *old_room, *new_room;
    EXIT_DATA *spexit;
    REVERSE_EXIT *rexit;
    char buf[4096];
    int found;
    int i;

    if (!current_room) {
        clientfv("I don't know where we are right now.");
        return;
    }

    if (!arg[0]) {
        /* Show this room... Just for comparison purposes. */
        int first = 1;

        clientfv("This room:");

        buf[0] = 0;
        for (i = 1; dir_name[i]; i++) {
            if (current_room->exits[i] || current_room->detected_exits[i]) {
                if (first) {
                    strcat(buf, C_D "(");
                    first = 0;
                } else
                    strcat(buf, C_D ",");

                if (current_room->exits[i])
                    strcat(buf, C_B);
                else
                    strcat(buf, C_R);

                strcat(buf, dir_small_name[i]);
            }
        }
        if (!first)
            strcat(buf, C_D ")" C_0);
        else
            strcat(buf, C_D "(" C_r "no exits" C_D ")" C_0);

        clientff("  %s -- %s " C_D "(" C_G "%d" C_D ")\r\n" C_0, buf, current_room->name, current_room->vnum);

        /* List all rooms that can be merged into this one. */

        clientfv("Other rooms:");
        found = 0;

        for (r = world; r; r = r->next_in_world)
            if (!strcmp(current_room->name, r->name) && r != current_room) {
                int good = 1;

                for (i = 1; dir_name[i]; i++) {
                    int e1 = r->exits[i] ? 1 : r->detected_exits[i];
                    int e2 = current_room->exits[i] ? 1 : current_room->detected_exits[i];

                    if (e1 != e2) {
                        good = 0;
                        break;
                    }
                }

                /* Print it away. */
                if (good) {
                    int first = 1;

                    found = 1;

                    buf[0] = 0;
                    for (i = 1; dir_name[i]; i++) {
                        if (r->exits[i] || r->detected_exits[i]) {
                            if (first) {
                                strcat(buf, C_D "(");
                                first = 0;
                            } else
                                strcat(buf, C_D ",");

                            if (r->exits[i])
                                strcat(buf, C_B);
                            else
                                strcat(buf, C_R);

                            strcat(buf, dir_small_name[i]);
                        }
                    }
                    if (!first)
                        strcat(buf, C_D ")" C_0);
                    else
                        strcat(buf, C_D "(" C_r "no exits" C_D ")" C_0);

                    clientff("  %s -- %s " C_D "(" C_G "%d" C_D ")\r\n" C_0, buf, r->name, r->vnum);
                }
            }

        if (!found)
            clientf("  No identical matches found.\r\n");
    } else {
        int vnum;

        old_room = current_room;

        if (mode != CREATING) {
            clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
            return;
        }

        vnum = atoi(arg);
        if (!vnum || !(new_room = get_room(vnum))) {
            clientfv("Merge with which vnum?");
            return;
        }

        if (new_room == old_room) {
            clientfv("You silly thing... What's the point in merging with the same room?");
            return;
        }

        if (strcmp(new_room->name, old_room->name)) {
            clientfv("That room doesn't even have the same name!");
            return;
        }

        /* Check for exits leading to other places. */
        for (i = 1; dir_name[i]; i++) {
            if (new_room->exits[i] && old_room->exits[i]
                && new_room->exits[i] != old_room->exits[i]) {
                clientff(C_R "[Problem with the %s exits; they lead to different places.]\r\n" C_0,
                         dir_name[i]);
                return;
            }

            if ((new_room->exits[i] || new_room->detected_exits[i]) != (old_room->exits[i]
                                                                        || old_room->detected_exits[i])) {
                clientff(C_R "[Problem with the '%s' exit; one room has it, the other doesn't.]\r\n"
                         C_0, dir_name[i]);
                return;
            }
        }

        /* Start merging old_room into new_room. */

        /* Exits from old_room, will now be from new_room. */
        for (i = 1; dir_name[i]; i++) {
            if (old_room->exits[i]) {
                if (new_room->exits[i])
                    unlink_rooms(new_room, i, new_room->exits[i]);

                link_rooms(new_room, i, old_room->exits[i]);
                unlink_rooms(old_room, i, old_room->exits[i]);

                new_room->detected_exits[i] |= old_room->detected_exits[i];
                new_room->locked_exits[i] |= old_room->locked_exits[i];
                new_room->exit_joins_areas[i] |= old_room->exit_joins_areas[i];

                if (!new_room->exit_length[i])
                    new_room->exit_length[i] = old_room->exit_length[i];
                if (!new_room->use_exit_instead[i])
                    new_room->use_exit_instead[i] = old_room->use_exit_instead[i];
                new_room->exit_stops_mapping[i] = old_room->exit_stops_mapping[i];
            }
        }
        /* Move special exits too. */
        while (old_room->special_exits) {
            spexit = create_exit(new_room);
            spexit->to = old_room->special_exits->to;
            spexit->vnum = old_room->special_exits->vnum;
            spexit->alias = old_room->special_exits->alias;
            spexit->cost = old_room->special_exits->cost;
            if (old_room->special_exits->command)
                spexit->command = strdup(old_room->special_exits->command);
            if (old_room->special_exits->message)
                spexit->message = strdup(old_room->special_exits->message);
            if (spexit->to)
                link_special_exit(new_room, spexit, spexit->to);

            destroy_exit(old_room->special_exits);
        }

        /* Exits going INTO old_room, should go into new_room. */
        while (old_room->rev_exits) {
            rexit = old_room->rev_exits;

            if (rexit->direction) {
                r = rexit->from, i = rexit->direction;
                unlink_rooms(r, i, old_room);
                link_rooms(r, i, new_room);
            } else if (rexit->spexit) {
                r = rexit->from, spexit = rexit->spexit;
                unlink_special_exit(r, spexit, old_room);
                spexit->to = new_room;
                spexit->vnum = new_room->vnum;
                link_special_exit(r, spexit, new_room);
            }
        }

        new_room->underwater |= old_room->underwater;

        /* Add all missing tags. */
        while (old_room->tags) {
            ELEMENT *tag;

            /* Look if it already exists. */
            for (tag = new_room->tags; tag; tag = tag->next)
                if (!strcmp((char *) old_room->tags->p, (char *) tag->p))
                    break;
            if (!tag) {
                tag = g_malloc0(sizeof(ELEMENT));
                tag->p = old_room->tags->p;
                link_element(tag, &new_room->tags);
            } else {
                free(old_room->tags->p);
                g_free(old_room->tags);
            }

            unlink_element(old_room->tags);
        }

        clientff(C_R "[Rooms " C_G "%d" C_R " and " C_G "%d" C_R " merged into " C_G "%d" C_R
                 ".]\r\n" C_0, new_room->vnum, old_room->vnum, new_room->vnum);
        destroy_room(old_room);

        current_room = new_room;
        current_area = new_room->area;

        refind_last_vnum();
    }
}

/* change the room vnum */
void do_room_number(char *arg){

    int roomnum;
    int *x = g_malloc(sizeof(int));
    GList *roomLink;
    ROOM_DATA *r;

    if(!arg[0] || !isdigit(*(arg))){
        clientfv("You must provide a vnum you want this room to switch to.");
        return;
    }

    if(mode!=CREATING){
        clientfv("Turn on mapping fist (vm c).");
        return;
    }

   roomnum=atoi(arg);

   if(get_room(roomnum)){
       clientfv("A room number with that number exists already.");
       return;
   }

   if(!current_room){
       clientfv("Don't know where we are at the moment. Walk around or ql to find ourselves.");
       return;
   }

    /* Unlink from hash table. */
    if (current_room == hash_world[current_room->vnum % MAX_HASH])
        hash_world[current_room->vnum % MAX_HASH] = current_room->next_in_hash;
    else {
        for (r = hash_world[current_room->vnum % MAX_HASH]; r; r = r->next_in_hash) {
            if (r->next_in_hash == current_room) {
                r->next_in_hash = current_room->next_in_hash;
                break;
            }
        }
    }

    recycle_room(current_room->vnum);
    
    
    *x = roomnum;
    roomLink = g_queue_find_custom(recycle_bin, (gpointer) x, (GCompareFunc) compare_recycled_rooms);
    g_queue_delete_link(recycle_bin, roomLink);
    g_free(x);

    current_room->vnum=roomnum;

    if(roomnum>last_vnum)
        last_vnum=roomnum;

    /* Link to new place in hashed table. */
    if (!hash_world[current_room->vnum % MAX_HASH]) {
        hash_world[current_room->vnum % MAX_HASH] = current_room;
        current_room->next_in_hash = NULL;
    } else {
        current_room->next_in_hash = hash_world[current_room->vnum % MAX_HASH];
        hash_world[current_room->vnum % MAX_HASH] = current_room;
    }
    clientff("Switched room number to %d.\r\n", current_room->vnum);

    if(current_room->rev_exits){
        REVERSE_EXIT *revexit;
        for(revexit = current_room->rev_exits; revexit; revexit=revexit->next){
            if(revexit->spexit){
                revexit->spexit->vnum=roomnum;
            }
        }
    }
}

void do_mark_list2(char *arg)
{
    AREA_DATA *area;
    ROOM_DATA *room;
    ELEMENT *tag;
    char buf[256];
    char name[256];
    int first;
    int found = 0;

    strcpy(name, arg);

    if (!name[0])
        clientfv("Marks throughout the world:");
    else
        clientff(C_W "[" C_B "Map" C_W "]: Rooms with the " C_D "%s" C_W " mark:\r\n", name);

    for (area = areas; area; area = area->next) {
        first = 1;

        for (room = area->rooms; room; room = room->next_in_area) {
            for (tag = room->tags; tag; tag = tag->next)
                if (!name[0] || !case_strcmp((char *) tag->p, name))
                    break;

            if (!tag)
                continue;

            if (!found)
                found = 1;

            /* And now... */

            /* Area name, if first room in the area's list. */
            if (first) {
                g_snprintf(buf, sizeof(buf), "\r\n" C_B "%s" C_0 "\r\n", area->name);
                clientf(buf);
                first = 0;
            }

            g_snprintf(buf, sizeof(buf), C_D " (" C_G "%d" C_D ") " C_0 "%s (", room->vnum, room->name);
            clientf(buf);

            clientff(C_D "%s" C_W, (char *) room->tags->p);
            for (tag = room->tags->next; tag; tag = tag->next)
                clientff(C_D ", %s" C_W, (char *) tag->p);

            clientff(C_0 ")\r\n" C_0);

        }
    }

    if (!found) {
        if (!name[0])
            clientf("None defined, use the 'room mark' command to add some.");
        else
            clientf("None found, check your spelling or add some with 'room tag'.");
    }

    clientf("\r\n");
}

void do_mark_list(char *arg)
{
    GQueue *tag_list;
    ELEMENT *tag;
    ROOM_DATA *room;
    AREA_DATA *area;
    char *x, buf[256];
    int found = 0, displayed_tag_name = 0, rooms_so_far = 0, display_msg = 0, specific_mark;
    const int max_rooms_per_area = 4;

    tag_list = g_queue_new();

    strcpy(buf, arg);

    /* Only want to display a specific mark? Don't cut off the output then */
    if (buf[0])
        specific_mark = 1;
    else
        specific_mark = 0;

    /* Get a list of all tags we got, or the ones we need */
    for (area = areas; area; area = area->next) {
        for (room = area->rooms; room; room = room->next_in_area) {
            for (tag = room->tags; tag; tag = tag->next) {
                if ((buf[0] && strcmp(buf, (char *) tag->p))
                    || g_queue_find_custom(tag_list, (gpointer) tag->p, (GCompareFunc) strcmp)) {
                    continue;
                }

                g_queue_push_head(tag_list, (gpointer) tag->p);
            }

        }
    }

    /* Now display them in an orderly fashion */
    while (!g_queue_is_empty(tag_list)) {
        x = g_queue_pop_head(tag_list);
        for (area = areas; area; area = area->next) {
            for (room = area->rooms; room; room = room->next_in_area) {
                for (tag = room->tags; tag; tag = tag->next) {
                    if (!strcmp(x, (char *) tag->p)) {

                        if (display_msg && !specific_mark) {
                            if (rooms_so_far > max_rooms_per_area)
                                clientff(C_D "(and %d more rooms).\r\n" C_0, rooms_so_far);
                            rooms_so_far = 0, display_msg = 0;
                        }

                        if (!displayed_tag_name) {
                            displayed_tag_name = 1;
                            clientff("%s" C_W "[" C_B "Map" C_W "]: rooms with mark " C_D "%s" C_W
                                     ":\r\n" C_0, found ? "\r\n" : "", (char *) tag->p);
                        }
                        if (!found)
                            found = 1;

                        if (specific_mark || rooms_so_far <= max_rooms_per_area) {
                            char room_name[strlen(room->name) + 1];
                            strcpy(room_name, room->name);
                            room_name[strlen(room_name) - 1] = '\0';
                            clientff(" " C_G " (" C_D "%d" C_G ")" C_0 " %s, in %s\r\n", room->vnum,
                                     room_name, room->area->name);
                        }

                        rooms_so_far++;
                        break;
                    }
                }

            }
        }
        displayed_tag_name = 0;
        display_msg = 1;
    }

    if (display_msg && !specific_mark) {
        if (rooms_so_far > max_rooms_per_area)
            clientff(C_D "(and %d more rooms).\r\n" C_0, rooms_so_far);
        rooms_so_far = 0, display_msg = 0;
    }

    if (!found)
        clientfv("didn't find any marks. Add some with " C_g "mark add <mark name>" C_W " first.");

}

void do_mark_export(char *arg)
{
    FILE *fl;
    ROOM_DATA *r;
    ELEMENT *tag;
    int i;

    if (!arg[0]) {
        clientfv("What mark do you want to export?");
        return;
    }

    if (!g_file_test("marks", G_FILE_TEST_IS_DIR)) {
        if ((g_mkdir("marks", 0755)) == -1) {
            debugf("Couln't create the directory");
            return;
        }
    }

    char buf[256];
    snprintf(buf, 256, "marks/%s-marks.txt", arg);

    fl = fopen(buf, "w");
    if (!fl) {
        clientff(C_W "[" C_B "Map" C_W "]: Unable to create the mark file: %s.\r\n" C_0, strerror(errno));
        return;
    }

    /* Check if we even have that tag. */
    i = 0;
    for (r = world; r; r = r->next_in_world) {
        if (r->tags) {
            for (tag = r->tags; tag->next; tag = tag->next);
            for (; tag; tag = tag->prev) {
                if (!strcmp((char *) tag->p, arg))
                    i = 1;

            }
        }
    }

    if (!i) {
        clientfv("We don't have that mark anywhere on the map. Can't export it :(");
        return;
    }

    snprintf(buf, 256,
             "# This is an exported list of the '%s' mark. To import it, simply do \"mark import %s\".\n\n",
             arg, arg);
    fprintf(fl, "%s", buf);

    i = 0;
    for (r = world; r; r = r->next_in_world) {
        if (r->tags) {
            /* Save them in reverse. */

            for (tag = r->tags; tag->next; tag = tag->next);
            for (; tag; tag = tag->prev) {
                if (!strcmp((char *) tag->p, arg)) {
                    int len = strlen(r->name);
                    char buf[len];
                    strcpy(buf, r->name);
                    buf[len - 1] = '\0';
                    fprintf(fl, "# %s, in %s\n", buf, r->area->name);
                    fprintf(fl, "Mark %d \"%s\"\n\n", r->vnum, (char *) tag->p);
                    i++;
                }
            }
        }
    }

    clientff(C_W "[" C_B "Map" C_W "]: Exported %d " C_D "%s" C_W
             " marks okay. You'll find the exported file\n"
             "(%s-marks.txt) in the systems \"marks\" folder.\r\n" C_0, i, arg, arg);

    fflush(fl);
    fclose(fl);

}

void do_mark_import(char *arg)
{
    FILE *fl;
    char file[256];
    char line[1024], value[1024], option[256], *p;
    int count = 0;

    if (!arg[0]) {
        clientfv("What mark do you want to import?");
        return;
    }

    snprintf(file, 256, "marks/%s-marks.txt", arg);

    if (!g_file_test("marks", G_FILE_TEST_IS_DIR)) {
        clientff(C_W "[" C_B "Map" C_W
                 "]: Unable to import the mark file: the marks folder doesn't even exist.\r\n" C_0);
        return;
    }

    if (!g_file_test(file, G_FILE_TEST_EXISTS)) {
        clientff(C_W "[" C_B "Map" C_W
                 "]: Unable to import the mark file: couldn't find the " C_D "%s-marks.txt" C_W
                 " file in the marks folder.\r\n" C_0, arg);
        return;
    }

    fl = fopen(file, "r");
    if (!fl) {
        clientff(C_W "[" C_B "Map" C_W "]: Unable to import the mark file: %s.\r\n" C_0, strerror(errno));
        return;
    }

    while (1) {
        if (!fgets(line, 1024, fl))
            break;

        if (feof(fl))
            break;

        /* Strip newline. */
        {
            p = line;
            while (*p != '\n' && *p != '\r' && *p)
                p++;

            *p = 0;
        }

        if (line[0] == '#' || line[0] == 0 || line[0] == ' ')
            continue;

        p = get_string(line, option, 256);

        p = get_string(p, value, 1024);

        if (!strcmp(option, "Mark")) {
            ROOM_DATA *room;
            ELEMENT *tag;
            char buf[256];
            int vnum;

            vnum = atoi(value);
            get_string(p, buf, 256);

            if (!vnum || !buf[0])
                debugf("Parse error in file '%s', in a Mark line.", file);
            else {
                room = get_room(vnum);
                if (!room)
                    debugf("Warning! Unable to mark room %d, it doesn't exist!", vnum);
                else if (strcmp(buf, arg))
                    debugf("Warning! Found a %s mark while importing a %s mark. Skipping.", buf, arg);
                else {
                    /* Make sure it doesn't exist, first. */
                    for (tag = room->tags; tag; tag = tag->next)
                        if (!strcmp(buf, (char *) tag->p))
                            break;

                    if (!tag) {
                        /* Link it. */
                        tag = g_malloc0(sizeof(ELEMENT));
                        tag->p = strdup(buf);
                        link_element(tag, &room->tags);
                        count++;
                    }
                }
            }
        }

        else
            debugf("Parse error in file '%s', unknown option '%s'.", file, option);
    }

    fclose(fl);

    clientff(C_W "[" C_B "Map" C_W "]: Imported %d new " C_D "%s" C_W " marks.\r\n" C_0, count, arg);

    save_settings("config.mapper.txt");
}

void do_mark_importlist(char *arg)
{
    GDir *dir;
    char buf[256], mark[256];
    const gchar *dir_name;
    int first = 0;

    if ((dir = g_dir_open("marks", 0, NULL)) == NULL) {
        clientfv("Couldn't open the marks folder.");
        return;
    }

    while ((dir_name = g_dir_read_name(dir)) != NULL) {
        snprintf(buf, 256, "%s", dir_name);
        if (!cmp("*-marks.txt", buf)) {
            if (!first) {
                first = 1;
                clientfv("List of importable marks:");
                extract_wildcard(0, mark, 256);
                clientff(C_D "%s", mark);
                continue;
            }
            extract_wildcard(0, mark, 256);
            clientff(C_B "," C_D " %s", mark);
        }
    }
    clientff(C_B ".\r\n" C_0);

}

void do_mark_add(char *arg)
{
    ROOM_DATA *room;
    ELEMENT *tag;
    char buf[256], buf2[256];
    char args[256];
    int vnum;

    strcpy(args, arg);
    arg = get_string(arg, buf, 256);
    arg = get_string(arg, buf2, 256);

    if ((vnum = atoi(buf2))) {
        room = get_room(vnum);
        if (!room) {
            clientfv("No room with that number found.");
            return;
        }

        arg = get_string(arg, buf2, 256);
    } else if (!(room = current_room)) {
        clientfv("I don't know where we are right now.");
        return;
    }

    if (!buf[0]) {
        clientff(C_W "[" C_B "Map" C_W "]: What mark do you want to add?\r\n" C_0);
        return;
    }

    /* Look if it already exists. */
    for (tag = room->tags; tag; tag = tag->next)
        if (!strcmp(buf, (char *) tag->p))
            break;

    if (!tag) {
        /* Link it. */
        tag = g_malloc0(sizeof(ELEMENT));
        tag->p = strdup(buf);
        link_element(tag, &room->tags);
    } else if (tag) {
        clientff(C_W "[" C_B "Map" C_W "]: mark " C_D "%s" C_W
                 " already exists in room %d (%s).\r\n" C_0, buf, room->vnum, room->name);
        return;
    }

    clientff(C_W "[" C_B "Map" C_W "]: added mark " C_D "%s" C_W
             " to \"%s\" (room %d).\r\nCurrent marks in it are: ", buf, room->name, room->vnum);
    if (room->tags) {
        clientff(C_D "%s" C_W, (char *) room->tags->p);
        for (tag = room->tags->next; tag; tag = tag->next)
            clientff(C_D ", %s" C_W, (char *) tag->p);
    } else
        clientf(C_D "(none)" C_W);
    clientf(".\r\n" C_0);

    save_settings("config.mapper.txt");
}

void do_mark_remove(char *arg)
{
    ROOM_DATA *room;
    ELEMENT *tag;
    char buf[256], buf2[256];
    char args[256];
    int vnum;

    strcpy(args, arg);
    arg = get_string(arg, buf, 256);
    arg = get_string(arg, buf2, 256);

    if ((vnum = atoi(buf2))) {
        room = get_room(vnum);
        if (!room) {
            clientfv("No room with that number found.");
            return;
        }

        arg = get_string(arg, buf2, 256);
    } else if (!(room = current_room)) {
        clientfv("I don't know where we are right now.");
        return;
    }

    if (!buf[0]) {
        clientff(C_W "[" C_B "Map" C_W "]: What tag do you want to remove?\r\n" C_0);
        return;
    }

    /* Look if it already exists. */
    for (tag = room->tags; tag; tag = tag->next)
        if (!strcmp(buf, (char *) tag->p))
            break;

    if (tag) {
        /* Unlink it. */
        free(tag->p);
        unlink_element(tag);
        g_free(tag);
    } else if (!tag) {
        clientff(C_W "[" C_B "Map" C_W "]: mark " C_D "%s" C_W
                 " already doesn't exist in room %d (%s).\r\n" C_0, buf, room->vnum, room->name);
        return;
    }

    clientff(C_W "[" C_B "Map" C_W "]: removed mark " C_D "%s" C_W
             " to \"%s\" (room %d).\r\nCurrent marks in it are: ", buf, room->name, room->vnum);
    if (room->tags) {
        clientff(C_D "%s" C_W, (char *) room->tags->p);
        for (tag = room->tags->next; tag; tag = tag->next)
            clientff(C_D ", %s" C_W, (char *) tag->p);
    } else
        clientf(C_D "(none)" C_W);
    clientf(".\r\n" C_0);

    save_settings("config.mapper.txt");
}

/* Exit commands. */

void do_exit_help(char *arg)
{
    clientfv("Module: Mapper. Room exit commands:");
    clientf(" exit link          - Link room # to this one. Both ways.\r\n"
            " exit stop          - Hide rooms beyond this exit.\r\n"
            " exit length        - Increase the exit length by #.\r\n"
            " exit map           - Map the next exit elsewhere.\r\n"
            " exit lock [c]      - Set all unlinked exits in room as locked or undo with 'c'.\r\n"
            " exit unilink       - One-way exit. Does not create a reverse link.\r\n"
            " exit destroy <dir> - Destroy an exit, and its reverse. Use the direction name\r\n"
            "                      for a ordinary exit or the exit number for a special exit.\r\n"
            " exit joinareas     - Show two areas on a single 'vmap'.\r\n"
            " exit special       - Create, destroy, modify or list special exits.\r\n"
            " exit warp          - Modify or destroy wormholes.\r\n");
}

void do_exit_length(char *arg)
{
    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!isdigit(*(arg)))
        clientfv("Specify a length. 0 is normal.");
    else {
        set_length_to = atoi(arg);

        if (set_length_to == 0)
            set_length_to = -1;

        clientfv("Move in the direction you wish to have this length.");
    }
}

void do_exit_stop(char *arg)
{
    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    clientfv("Move in the direction you wish to stop (or start again) mapping from.");
    switch_exit_stops_mapping = 1;
}

void do_exit_joinareas(char *arg)
{
    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    clientfv("Move into the other area.");
    switch_exit_joins_areas = 1;
}

void do_exit_map(char *arg)
{
    char buf[256];
    int i;

    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    use_direction_instead = 0;

    for (i = 1; dir_name[i]; i++) {
        if (!strcmp(arg, dir_small_name[i]))
            use_direction_instead = i;
    }

    if (!use_direction_instead) {
        use_direction_instead = -1;
        clientfv("Will use default exit, as map position.");
    } else {
        g_snprintf(buf, sizeof(buf), "The room will be mapped '%s' from here, instead.", dir_name[use_direction_instead]);
        clientfv(buf);
    }
}

void do_exit_link(char *arg)
{
    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!isdigit(*(arg))) {
        clientfv("Specify a vnum to link to.");
        return;
    }

    link_next_to = get_room(atoi(arg));

    unidirectional_exit = 0;

    if (!link_next_to)
        clientfv("Disabled.");
    else
        clientfv("Move in the direction the room is.");
}

void do_exit_unilink(char *arg)
{
    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (arg[0]) {
        if (!isdigit(*arg)) {
            clientfv("Specify a vnum to link to.");
            return;
        }

        link_next_to = get_room(atoi(arg));

        if (!link_next_to) {
            clientfv("That room does not exist.");
            return;
        }
    }

    unidirectional_exit = 1;

    clientfv("Move in the direction you want to create an unidirectional exit towards.");
}

void do_exit_lock(char *arg)
{
    int i, set, nr;

    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!current_room) {
        clientfv("I don't know where we are right now.");
        return;
    }

    if (arg[0] == 'c')
        set = 0;
    else
        set = 1;

    for (i = 1, nr = 0; dir_name[i]; i++) {
        if (current_room->detected_exits[i] && !current_room->exits[i]) {
            if (!nr++)
                clientff(C_R "[Marked as %slocked: ", set ? "" : "un");
            else
                clientff(", ");

            clientff(C_r "%s" C_R, dir_name[i]);
            current_room->locked_exits[i] = set;
        }
    }

    if (nr)
        clientff(".]\r\n" C_0);
    else
        clientfv("There are no unlinked exits to mark here.");
}

void do_exit_destroy(char *arg)
{
    EXIT_DATA *spexit;
    char buf[256];
    int dir = 0;
    int i;

    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!current_room) {
        clientfv("I don't know where we are right now.");
        return;
    }

    /* Destroy a normal exit? */
    for (i = 1; dir_name[i]; i++) {
		if (!case_strcmp(arg, dir_name[i])||!case_strcmp(arg,dir_small_name[i])){
            dir = i;
			break;
		}
    }

    if (dir) {
        if (!current_room->exits[dir]) {
            clientfv("No link in that direction.");
            return;
        }

        if (current_room->exits[dir]->exits[reverse_exit[dir]] != current_room) {
            clientfv("Warning: Reverse link was not destroyed.");
        } else {
            unlink_rooms(current_room->exits[dir], reverse_exit[dir], current_room);
        }

        i = current_room->exits[dir]->vnum;

        unlink_rooms(current_room, dir, current_room->exits[dir]);

        g_snprintf(buf, sizeof(buf), "Link to vnum %d destroyed.", i);
        clientfv(buf);
        return;
    }

    /* Destroy a special exit? */
    if (arg[0] && isdigit(arg[0])) {
        int nr;

        nr = atoi(arg);

        for (i = 1, spexit = current_room->special_exits; spexit; spexit = spexit->next, i++) {
            if (i == nr) {
				if(!strcmp(spexit->command,"worm warp")){
					clientfv("If you want to destroy wormholes, please use 'exit warp destroy'.");
					return;
				}
                destroy_exit(spexit);
                g_snprintf(buf, sizeof(buf), "Special exit %d destroyed.", nr);
                clientfv(buf);
                return;
            }
        }

        g_snprintf(buf, sizeof(buf), "Special exit %d was not found.", nr);
        clientfv(buf);

        return;
    }

    clientfv("Which link do you wish to destroy?");
    clientfv("Use 'room look' for a list.");
    return;
}

// Add "alias", <create/link <vnum>>, "nocommand"
// Real: exit special <create/nolink/link <vnum>> [nocommand] [alias <dir>]
// FIXME: Convert [1,n]!
void do_exit_special(char *arg)
{
    //  ROOM_DATA *room;
    char cmd[256];

    DEBUG("do_exit_special");

    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!arg[0]) {
        clientfv("Syntax: exit special <command> [arguments]");
        clientfv("Commands:");
        clientff(C_R " create             - On the other side, create a new room.\r\n" C_0);
        clientff(C_R " link <vnum>        - On the other side, link with <vnum>.\r\n" C_0);
        clientff(C_R " nolink             - On the other side, is always something random.\r\n" C_0);
        clientff(C_R " stop               - Stop capturing the special exit.\r\n" C_0);
		clientff(C_R " message <nr> <msg> - Change the message for the Special Exit\r\n" C_0);
        clientfv("Optional arguments:");
        clientff(C_R " nocommand     - The exit is not triggered by a command.\r\n" C_0);
        clientff(C_R " alias <dir>   - Create an alias for this exit's command.\r\n" C_0);

        return;
    }

    if (!current_room) {
        clientfv("I don't know where we are right now.");
        return;
    }

    arg = get_string(arg, cmd, 256);

    if (!strcmp(cmd, "stop")) {
        capture_special_exit = 0;
        clientfv("Capturing disabled.");
        return;
    } else if (capture_special_exit) {
        clientfv("Already capturing, use 'exit special stop' first.");
        return;
    }

    ROOM_DATA *room;
    room = NULL;

    if (!strcmp(cmd, "create")) {
        special_exit_vnum = 0;
    } else if (!strcmp(cmd, "nolink")) {
        special_exit_vnum = -1;
    } else if (!strcmp(cmd, "link")) {
        int vnum;

        arg = get_string(arg, cmd, 256);
        vnum = atoi(cmd);

        if (vnum < 1) {
            clientfv("Specify a valid vnum to link the exit to.");
            return;
        }
        if (!(room = get_room(vnum))) {
            clientfv("No room with that vnum exists!");
            return;
        }

        special_exit_vnum = vnum;
    } else if (!strcmp(cmd, "message")) {
		if(arg[0]){

			int nr;
			int i;
			EXIT_DATA *spe;
			spe=NULL;
			arg = get_string(arg, cmd, 256);
			nr = atoi(cmd);

			if (nr < 1) {
				clientfv("Specify a valid Special Exit to change the message.");
				return;
			}
			if(!current_room->special_exits){
				clientfv("This Special Exit doesn't exist.");
				return;
			}
			spe=current_room->special_exits;
			for(i=1;i<nr;i++){
				if(!spe->next){
					clientfv("This Special Exit doesn't exist.");
					return;
				}
				spe=spe->next;
			}
			if(spe==current_room->warp->link){
				clientfv("Have a look at 'exit warp' for changing wormholes.");
				return;
			}
			free(spe->message);
			spe->message=strdup(arg);
			clientff(C_R "[Message changed to \"%s\"]\r\n" C_0,arg);
			arg[0]=0;
			return;
		}
	}

    special_exit_nocommand = 0;
    special_exit_alias = 0;

    while (arg[0]) {
        arg = get_string(arg, cmd, 256);

        if (!strcmp(cmd, "nocommand"))
            special_exit_nocommand = 1;
        else if (!strcmp(cmd, "alias")) {
            int i;

            arg = get_string(arg, cmd, 256);

            for (i = 0; dir_name[i]; i++)
                if (!strcmp(cmd, dir_name[i]))
                    break;

            if (!cmd[0] || !dir_name[i]) {
                clientfv("Aliases can only be north, up, in, etc.");
                return;
            }

            special_exit_alias = i;
        } else {
            clientff(C_R "[Unknown option '%s'.]\r\n" C_0, cmd);
            return;
        }
    }

    capture_special_exit = 1;
    cse_message[0] = 0;
    cse_command[0] = 0;

    clientfv("Capturing. Use 'stop' to disable.");
    if (special_exit_vnum > 0)
        clientff(C_R "[The link will be made with '%s'.]\r\n" C_0, room->name);

    return;

#if 0
    // ---================ *** =================---
    if (!strcmp(cmd, "capture")) {
        arg = get_string(arg, cmd, 256);

        if (!cmd[0]) {
            clientfv("Syntax: Esp capture create");
            clientfv("        Esp capture link <vnum>");
            return;
        }

        if (!strcmp(cmd, "create")) {
            capture_special_exit = -1;

            clientfv("Capturing. A room will be created on the other end.");
            clientfv("Use 'stop' to disable capturing.");
        } else if (!strcmp(cmd, "link")) {
            ROOM_DATA *room;
            int vnum;

            get_string(arg, cmd, 256);
            vnum = atoi(cmd);

            if (vnum < 1) {
                clientfv("Specify a valid vnum to link the exit to.");
                return;
            }
            if (!(room = get_room(vnum))) {
                clientfv("No room with that vnum exists!");
                return;
            }

            capture_special_exit = vnum;

            clientff(C_R "[Capturing. The exit will be linked to '%s']\r\n" C_0, room->name);
            clientfv("Use 'stop' to disable capturing.");
        }

        cse_command[0] = 0;
        cse_message[0] = 0;

        return;
    }
    // FIXME: How can we create -1 vnum exits?
    // Doesn't conform to new reverse exits.
    else if (!strcmp(cmd, "link")) {
        /* E special link 0 -1 */
        char vnum[256];

        /* We'll store the number in cmd. */
        arg = get_string(arg, cmd, 256);

        if (!isdigit(cmd[0])) {
            clientfv("Link which special exit?");
            return;
        }

        nr = atoi(cmd);

        for (i = 0, spexit = current_room->special_exits; spexit; spexit = spexit->next) {
            if (i++ == nr) {
                get_string(arg, vnum, 256);

                if (vnum[0] == '-') {
                    ROOM_DATA *to;

                    to = spexit->to;

                    spexit->vnum = -1;
                    spexit->to = NULL;

                    g_snprintf(buf, sizeof(buf), "Link cleared on exit %d.", nr);
                    clientfv(buf);
                    return;
                } else if (isdigit(vnum[0])) {
                    spexit->vnum = atoi(vnum);
                    spexit->to = get_room(spexit->vnum);
                    if (!spexit->to) {
                        clientfv("A room whith that vnum was not found.");
                        spexit->vnum = -1;
                        return;
                    }
                    g_snprintf(buf, sizeof(buf), "Special exit %d linked to '%s'.", nr, spexit->to->name);
                    clientfv(buf);
                    return;
                } else {
                    clientfv("Link to which vnum?");
                    return;
                }
            }
        }

        g_snprintf(buf, sizeof(buf), "Can't find special exit %d.", nr);
        clientfv(buf);
    }

    else if (!strcmp(cmd, "alias")) {
        /* E special alias 0 down */

        if (!arg[0] || !isdigit(arg[0])) {
            clientfv("What special exit do you wish to destroy?");
            return;
        }

        nr = atoi(arg);

        for (i = 0, spexit = current_room->special_exits; spexit; spexit = spexit->next) {
            if (i++ == nr) {
                arg = get_string(arg, cmd, 256);

                if (!strcmp(arg, "none")) {
                    g_snprintf(buf, sizeof(buf), "Alias on exit %d cleared.", nr);
                    clientfv(buf);
                    return;
                }

                for (i = 1; dir_name[i]; i++) {
                    if (!strcmp(arg, dir_name[i])) {
                        g_snprintf(buf, sizeof(buf), "Going %s will now trigger exit %d.", dir_name[i], nr);
                        clientfv(buf);
                        spexit->alias = i;
                        return;
                    }
                }

                clientfv("Use an exit, such as 'north', 'up', 'none', etc.");
                return;
            }
        }

        g_snprintf(buf, sizeof(buf), "Special exit %d was not found.", nr);
        clientfv(buf);
    }

    else {
        clientfv("Unknown command... Try 'exit special help'.");
    }
#endif
}

void do_exit_warp(char *arg){

	char cmd[256];

    DEBUG("do_exit_warp");

    if (mode != CREATING) {
        clientfv("Turn mapping on, first (" C_g "vmap create" C_0 " or " C_g "vm c" C_W ").");
        return;
    }

    if (!arg[0]) {
        clientfv("Syntax: exit warp <command> [arguments]");
        clientfv("Commands:");
        clientff(C_R " destroy            - Destroy a wormhole.\r\n" C_0);
		clientff(C_R " destination <vnum> - Change the destination of the wormhole.\r\n" C_0);

        return;
    }

    if (!current_room) {
        clientfv("I don't know where we are right now.");
        return;
    }

	if(!current_room->warp){
		clientfv("No warp in this room.");
		return;
	}

    arg = get_string(arg, cmd, 256);

	if (!strcmp(cmd, "destroy")){

		destroy_warp(current_room->warp);
		clientfv("Warp sucessfully deleted");
        return;
	}

	if (!strcmp(cmd, "destination")){

		char num[256];
		int number;
		ROOM_DATA *room;
		WARP_DATA *warp1, *warp2 = NULL;

		arg = get_string(arg, num, 256);

		if(!isdigit(num[0])){
			clientfv("Please enter a vnum. For the command see 'exit warp'.");
			return;
		}

		number=atoi(num);

		room = get_room(number);

		if(!room){
			clientfv("This room doesn't exist. Please enter an existing vnum.");
			return;
		}

		if(room->warp){
			clientfv("The destination has already a warp.");
			return;
		}

		destroy_warp(current_room->warp);

		warp1 = create_warp(current_room, room);
		if(warp1){

			warp2 = create_warp(room, current_room);
			warp1->reverse = warp2;
		}

		if(warp2){

			warp2->reverse = warp1;
		}
		clientfv("Warp destination sucessfully changed.");
        return;
	}
	clientfv("This is no known command. Try 'exit warp' to see the commands.");
}

/* Normal commands. */

void do_go(char *arg)
{
    if (q_top) {
        clientfv("Command queue isn't empty, clear it first.");
        return;
    }

    dash_command = 0;

    if (!strcmp(arg, "dash"))
        dash_command = "dash ";
    else if (!strcmp(arg, "sprint"))
        dash_command = "sprint ";
    else if (!strcmp(arg, "gallop"))
        dash_command = "gallop ";
    else if (!strcmp(arg, "runaway"))
        dash_command = "runaway ";
    else if (arg[0]) {
        clientfv("Usage: go [dash/sprint/gallop/runaway]");
        return;
    }

    go_next();
    clientf("\r\n");
}

void do_areainfo(char *arg)
{
    if (!arg[0]) {
        clientfv("About what area do you want to get the information?");
        return;
    }

    ROOM_DATA *room;

    int found = 0;
    int first_room = 0;
    int lala;

    char *area_name = NULL;

    char *color = C_R;

    for (room = world; room; room = room->next_in_world) {

        if (case_strstr((char *) room->area->name, arg)) {
            if (area_name != room->area->name) {
                clientff(C_B "\r\nMap information for %s\r\n\r\n" C_0, room->area->name);
                first_room = 1;
                area_name = room->area->name;
            }

            color = room->room_type->color;

            clientff(C_B "(" C_W "%d" C_B ")" C_R ": " C_D "%s", room->vnum, room->name);

            if (room->room_type == null_room_type)
                clientff(C_B " (" C_Y "enviroment not set" C_B ")");
            else
                clientff(C_B " (" "%s%s" C_B ")", color, room->room_type->name);

            for (lala = 1; dir_name[lala]; lala++) {
                if (!room->exits[lala] && room->detected_exits[lala] && !room->locked_exits[lala])
                    clientff(C_B " (" C_G "has unmapped exits" C_B ")");
                break;
            }

            found = 1;

            clientff(C_0 "\r\n");
        }

    }
    if (!found) {
        clientfv("[No such area exists.]\r\n");
    }
}

void do_mstop(char *arg)
{
    clean_paths();
    clientfv("Okay, stopped.");
}

void do_goto(char *arg)
{
    ROOM_DATA *room;

    if (mode == CREATING) {
        clientfv("Are you sure you want to use goto? If yes, then switch to the following " C_0 "("
                 C_g "vmap follow" C_0 ")" C_W " mode first.");
        return;
    } else if (mode == GET_UNLOST) {
        clientfv("We're lost. Need to find out where we are (ql, walk about) first.");
        return;
    } else if (mode != FOLLOWING) {
        clientfv("Oops! Switch to the following " C_0 "(" C_g "vmap follow" C_0 ")" C_W " mode first.");
        return;
    }

    if (!current_room) {
        clientfv("I don't know where we are to begin with - find ourselves on the map first.");
        return;
    }

    if (!arg[0]) {
        clientfv("What room, mark, person or area do you want to go to?");
        return;
    } else {

	if(!goto_person)
        	dash_command = 0;

        if (strchr(arg, ' ')) {
            if (strstr(arg, " sprint")) {
                dash_command = "sprint ";
                arg[strlen(arg) - 7] = 0;
            } else if (strstr(arg, " dash")) {
                dash_command = "dash ";
                arg[strlen(arg) - 5] = 0;
            } else if (strstr(arg, " gallop")) {
                dash_command = "gallop ";
                arg[strlen(arg) - 7] = 0;
            } else if (strstr(arg, " runaway")) {
                dash_command = "runaway ";
                arg[strlen(arg) - 8] = 0;
            }
        }

        /* goto <room number> */
        if (isdigit(*arg)) {
            do_vmap_path(arg);
            clientf("\r\n");

        /* goto mark <mark> */
        } else if (!strncmp(arg, "mark ", 5)) {
            ELEMENT *tag;
            char buf[256];

			clean_paths();
            strcpy(buf, arg + 5);

            int found = 0;

            for (room = world; room; room = room->next_in_world)
                for (tag = room->tags; tag; tag = tag->next)
                    if (!case_strcmp(buf, (char *) tag->p)) {
                        add_to_pathfinder(room, 1);

                        found = 1;
                    }

            if (!found) {
                clientff(C_W "[" C_B "Map" C_W "]: No room with the mark '%s' was found.\r\n" C_0, buf);
                clean_paths();
                return;
            }


        /* goto room <name>*/
        } else if(!strncmp(arg, "room ",5)){

			ROOM_DATA *found;
			int count=0;
			char name[256];

                        //TODO: rethink when speedwalking (maybe even if 3 are found, but warn?)

			clean_paths();
			strcpy(name,arg+5);
			found=locate_room_by_name(name);

			for (room = world; room; room = room->next_in_world)
				if (case_strstr((char *) room->name, name) ){
					if(!found){
						found = room;
					}else if(!count){
						clientfv("Multiple matches found:");
						char buf[256], buf2[56];
						g_snprintf(buf2, sizeof(buf2), C_D "(" C_G "%d" C_D ")", found->vnum);
						g_snprintf(buf, sizeof(buf), " %-28s" C_0 " %-45s%s%s%s", buf2, found->name,
								found->area != current_area ? " (" : "",
								found->area != current_area ? found->area->name : "",
								found->area != current_area ? ")" : "");
						strcat(buf,"\r\n");
						clientf(buf);
						g_snprintf(buf2, sizeof(buf2), C_D "(" C_G "%d" C_D ")", room->vnum);
						g_snprintf(buf, sizeof(buf), " %-28s" C_0 " %-45s%s%s%s", buf2, room->name,
								room->area != current_area ? " (" : "",
								room->area != current_area ? room->area->name : "",
								room->area != current_area ? ")" : "");
						strcat(buf,"\r\n");
						clientf(buf);
						count=2;
					}else if(count<11){
						char buf[256], buf2[56];
						count++;
						g_snprintf(buf2, sizeof(buf2), C_D "(" C_G "%d" C_D ")", room->vnum);
						g_snprintf(buf, sizeof(buf), " %-28s" C_0 " %-45s%s%s%s", buf2, room->name,
								room->area != current_area ? " (" : "",
								room->area != current_area ? room->area->name : "",
								room->area != current_area ? ")" : "");
						strcat(buf,"\r\n");
						clientf(buf);
					}else{
						break;
					}
				}

                    if (!found) {
                        clientff(C_W "[" C_B "Map" C_W "]: No room with the name '%s' was found.\r\n" C_0, arg+5);
                        clean_paths();
                        return;
                    }else if(!count){
                        char buf[256];
                        add_to_pathfinder(found,1);
                        g_snprintf(buf, sizeof(buf), "Off we go to %s", found->name);
                        clientfv(buf);
                    } else{
                        if(count==11){
                            clientff("More than 10 rooms found.\r\n");
                        }
                        clientff("Please use goto <number> to reach your destination.\r\n");
                        clean_paths();
                    }

        }else if(!strncmp(arg, "person ",7)){


            if(!scry_command[0]){
                clientfv("No scrying command specified. To set one, use " C_R "vm config scry_command <command>" C_0 ".");
                return;
            }

            send_to_server(scry_command);
            send_to_server(arg+7);
            send_to_server("\r\n");
            if(!strcmp(scry_command, "farsee ")){
                farseeing = 1; //necessary to enable walking to the persons area at least
                add_timer("farsee", 5, t_farsee, 0, 0, 0);
            }
            goto_person = 1;
            return;


        /* goto <area> */
        }else {

                        int number;
			AREA_DATA *area;
			char *d, number_arg[256];


                        clean_paths();


			d=(arg+strlen(arg));
			while (d > arg && *d != ' ')
						d--;
			get_string(d, number_arg, 256);

			number=atoi(number_arg);

			if(number)
				*d=0;

			area=get_area_by_name(arg,number);


			if(area){
				if(!strcmp(area->name,current_area->name)){
                                    clientff(C_W "[" C_B "Map" C_W "]: We're already in " C_D "%s" C_W
                                        " though.\r\n" C_0, area->name);
				}else{
                                    char buf[256];
                                    for(room=area->rooms;room;room =room->next_in_area){
						add_to_pathfinder(room, 1);
                                    }
                                    g_snprintf(buf, sizeof(buf), "Off we go to %s", area->name);
                                    clientfv(buf);
				}
			}else{
                            clientfv("Can't find that area on the map.");
                            clean_paths();
                            return;
			};

        }

        get_timer();
        path_finder_2();

        go_next();
        clientf("\r\n");
    }
}

void do_room_check(char *arg)
{
    ROOM_DATA *room;
    char buf[256];
    //int detected_only = 0, unknown_type = 0;
    int rooms = 0, i;

    DEBUG("do_room_check");

    if (!current_area) {
        clientfv("I need to know what area are you in to begin with.");
        return;
    }

    for (room = current_area->rooms; room; room = room->next_in_area) {
        int unlinked = 0, notype = 0, locked = 0;

        if (room->room_type == null_room_type)
            notype = 1;
        for (i = 1; dir_name[i]; i++)
            if (!room->exits[i] && room->detected_exits[i]) {
                if (room->locked_exits[i])
                    locked = 1;
                else
                    unlinked = 1;
            }

        if (unlinked || notype) {
            rooms++;
            clean_paths();
            g_snprintf(buf, sizeof(buf), "Heading off to '%s' (%d) to fix ", room->name, room->vnum);
            if (unlinked && notype)
                strcat(buf, "unlinked exits and the unset enviroment.");
            else if (unlinked)
                strcat(buf, "unlinked exits.");
            else if (notype)
                strcat(buf, "the unset enviroment.");
            else
                strcat(buf, "something.");

            clientfv(buf);
            add_to_pathfinder(room, 1);
            path_finder_2();
            go_next();
            return;
        }
    }
    if (!rooms)
        clientfv("All rooms are good! The area is complete.\r\n");

}

void print_mhelp_line(char *line)
{
    char buf[8096];
    char *p, *b, *c;

    p = line;
    b = buf;

    while (*p) {
        if (*p != '^') {
            *(b++) = *(p++);
            continue;
        }

        p++;
        switch (*p) {
        case 'r':
            c = "\33[0;31m";
            break;
        case 'g':
            c = "\33[0;32m";
            break;
        case 'y':
            c = "\33[0;33m";
            break;
        case 'b':
            c = "\33[0;34m";
            break;
        case 'm':
            c = "\33[0;35m";
            break;
        case 'c':
            c = "\33[0;36m";
            break;
        case 'w':
            c = "\33[0;37m";
            break;
        case 'D':
            c = "\33[1;30m";
            break;
        case 'R':
            c = "\33[1;31m";
            break;
        case 'G':
            c = "\33[1;32m";
            break;
        case 'Y':
            c = "\33[1;33m";
            break;
        case 'B':
            c = "\33[1;34m";
            break;
        case 'M':
            c = "\33[1;35m";
            break;
        case 'C':
            c = "\33[1;36m";
            break;
        case 'W':
            c = "\33[1;37m";
            break;
        case 'x':
            c = "\33[0;37m";
            break;
        case '^':
            c = "^";
            break;
        case 0:
            c = NULL;
            break;
        default:
            c = "-?-";
        }

        if (!c)
            break;

        while (*c)
            *(b++) = *(c++);
        p++;
    }

    *b = 0;

    clientf(buf);
}

void do_mhelp(char *arg)
{
    FILE *fl;
    char buf[4096];
    char name[256];
    char *p;
    int found = 0;
    int empty_lines = 0;

    if (!arg[0]) {
        g_snprintf(buf, sizeof(buf), "index");
    }

    fl = fopen("mhelp", "r");
    if (!fl)
        fl = fopen("mhelp.txt", "r");

    if (!fl) {
        clientff(C_W "[" C_B "Map" C_W "]: Unable to open the map log: %s.", strerror(errno));
        return;
    }

    while (1) {
        if (!fgets(buf, 4096, fl))
            break;

        /* Comments... Ignored. */
        if (buf[0] == '#')
            continue;

        /* Strip newline. */
        p = buf;
        while (*p != '\n' && *p != '\r' && *p)
            p++;
        *p = 0;

        /* Names. */
        if (buf[0] == ':') {
            if (found)
                break;

            p = buf + 1;

            while (*p) {
                p = get_string(p, name, 256);
                if (!case_strcmp(arg, name)) {
                    /* This is it. Start printing. */

                    found = 2;
                    empty_lines = 0;

                    get_string(buf + 1, name, 256);
                    clientff(C_C "Mapper" C_0 " - " C_W "%s" C_0 "\r\n\r\n", name);
                    break;
                }
            }

            continue;
        }

        /* One line that must be displayed. */
        if (found) {
            /* Remember empty lines, print them later. */
            /* Helps to skip them at the beginning and at the end. */
            if (!buf[0]) {
                empty_lines++;
                continue;
            }

            if (found == 2)
                empty_lines = 0, found = 1;
            else if (empty_lines > 0)
                while (empty_lines) {
                    clientf("\r\n");
                    empty_lines--;
                }

            print_mhelp_line(buf);
            clientf("\r\n");
        }
    }

    fclose(fl);

    if (!found)
        clientfv
            ("No help topic by that name yet, sorry. Try asking what you need help with in the chatroom: http://vadisystems.com/vadi-mapper/index.php/Chatroom");
}

void do_map_log(char *arg)
{
    FILE *fl;
    char buf[4096];
    char name[256];
    char *p;
    int found = 0;
    int empty_lines = 0;

    /* Map log isn't fully finished yet, so we just point to an online link for now */

    clientfv
        ("Sorry, but this feature isn't yet fully finished. You can however view the map log details at this url:\r\n\r\n"
         "http://snipurl.com/28wjb\r\n\r\n"
         "(click on the black triangles to make the full text show, or on the \"> expand all\" link top-left)\r\n");
    return;

    if (!arg[0]) {
        g_snprintf(buf, sizeof(buf), "index");
    }

    fl = fopen("maplog", "r");

    if (!fl) {
        clientff(C_W "[" C_B "Map" C_W "]: Unable to open the map log: %s.\r\n" C_0, strerror(errno));
        return;
    }

    while (1) {
        if (!fgets(buf, 4096, fl))
            break;

        /* Comments... Ignored. */
        if (buf[0] == '#')
            continue;

        /* Strip newline. */
        p = buf;
        while (*p != '\n' && *p != '\r' && *p)
            p++;
        *p = 0;

        /* Names. */
        if (buf[0] == ':') {
            if (found)
                break;

            p = buf + 1;

            while (*p) {
                p = get_string(p, name, 256);
                if (!case_strcmp(arg, name)) {
                    /* This is it. Start printing. */

                    found = 2;
                    empty_lines = 0;

                    get_string(buf + 1, name, 256);
                    clientff(C_C "Map Log" C_0 " - " C_W "%s" C_0 "\r\n\r\n", name);
                    break;
                }
            }

            continue;
        }

        /* One line that must be displayed. */
        if (found) {
            /* Remember empty lines, print them later. */
            /* Helps to skip them at the beginning and at the end. */
            if (!buf[0]) {
                empty_lines++;
                continue;
            }

            if (found == 2)
                empty_lines = 0, found = 1;
            else if (empty_lines > 0)
                while (empty_lines) {
                    clientf("\r\n");
                    empty_lines--;
                }

            print_mhelp_line(buf);
            clientf("\r\n");
        }
    }

    fclose(fl);

    if (!found)
        clientfv("There is no log for that revision.");
}

void do_wholist_show(char* arg)
{
    wholist=2; //why 2? Because we process the Prompt again, before we get all our stuff
    send_to_server("who b\r\n");
    gag_next_prompt = 1;
}

void do_wholist_room(char* arg)
{
    strcpy(wholist_argument, "room ");
    strcat(wholist_argument, arg);
    do_wholist_show(arg);
}

void do_wholist_help(char* arg)
{
    clientfv("Module: Mapper. Wholist commands:");
    clientf(" wholist              - add room numbers to the 'who b' output.\r\n"
            " wholist room <room>  - Show all people in the given room.\r\n");
}

#define CMD_NONE        0
#define CMD_MAP         1
#define CMD_AREA        2
#define CMD_ROOM        3
#define CMD_EXIT        4
#define CMD_MARK        5
#define CMD_WHOLIST     6

FUNC_DATA cmd_table[] = {
    /* Map commands. */
    {"help", do_vmap_help, CMD_MAP},
    {"create", do_vmap_create, CMD_MAP},
    {"follow", do_vmap_follow, CMD_MAP},
    {"none", do_vmap_none, CMD_MAP},
    {"off", do_vmap_off, CMD_MAP},
    {"save", do_vmap_save, CMD_MAP},
    {"load", do_vmap_load, CMD_MAP},
    {"path", do_vmap_path, CMD_MAP},
    {"backpath", do_vmap_backpath, CMD_MAP},
    {"status", do_vmap_status, CMD_MAP},
    {"color", do_vmap_color, CMD_MAP},
    {"colour", do_vmap_color, CMD_MAP},
    {"recycle list", do_vmap_recycle_list, CMD_MAP},
    {"bump", do_vmap_bump, CMD_MAP},
    {"old", do_vmap_old, CMD_MAP},
    {"file", do_vmap_file, CMD_MAP},
    {"teleport", do_vmap_teleport, CMD_MAP},
    {"queue", do_vmap_queue, CMD_MAP},
    {"config", do_vmap_config, CMD_MAP},
    {"window", do_vmap_window, CMD_MAP},
    {"log", do_map_log, CMD_MAP},
    {"update", do_vmap_update, CMD_MAP},
    {"upload", do_vmap_upload, CMD_MAP},
    {"edit", do_vmap_edit, CMD_MAP},

    /* Area commands. */
    {"help", do_area_help, CMD_AREA},
    {"list", do_area_list, CMD_AREA},
    /* {"save", do_area_save, CMD_AREA}
       , */
    {"switch", do_area_switch, CMD_AREA},
    {"check", do_area_check, CMD_AREA},
    /* {"update", do_area_update, CMD_AREA}
       , */
    {"off", do_area_off, CMD_AREA},
    {"orig", do_area_orig, CMD_AREA},
    {"orig", do_vmap_orig, CMD_MAP},
    {"info", do_areainfo, CMD_AREA},
    {"destroy", do_area_destroy, CMD_AREA},

    /* Room commands. */
    {"help", do_room_help, CMD_ROOM}
    ,
    {"switch", do_room_switch, CMD_ROOM}
    ,
    {"find", do_find, CMD_ROOM}
    ,
    {"area", do_room_area, CMD_ROOM}
    ,
    {"look", do_room_look, CMD_ROOM}
    ,
    {"destroy", do_room_destroy, CMD_ROOM}
    ,
    {"list", do_room_list, CMD_ROOM}
    ,
    {"listall", do_room_listall, CMD_ROOM}
    ,
    {"underwater", do_room_underw, CMD_ROOM}
    ,
    {"merge", do_room_merge, CMD_ROOM}
    ,
    {"env", do_room_env, CMD_ROOM}
    ,
    {"check", do_room_check, CMD_ROOM}
    ,
    {"number", do_room_number, CMD_ROOM}
    ,

    /* Exit commands. */
    {"help", do_exit_help, CMD_EXIT}
    ,
    {"length", do_exit_length, CMD_EXIT}
    ,
    {"stop", do_exit_stop, CMD_EXIT}
    ,
    {"map", do_exit_map, CMD_EXIT}
    ,
    {"link", do_exit_link, CMD_EXIT}
    ,
    {"unilink", do_exit_unilink, CMD_EXIT}
    ,
    {"lock", do_exit_lock, CMD_EXIT}
    ,
    {"destroy", do_exit_destroy, CMD_EXIT}
    ,
    {"joinareas", do_exit_joinareas, CMD_EXIT}
    ,
    {"special", do_exit_special, CMD_EXIT}
    ,
	{"warp", do_exit_warp, CMD_EXIT}
	,

    /* mark commands */
    {"export", do_mark_export, CMD_MARK}
    ,
    {"import", do_mark_import, CMD_MARK}
    ,
    {"importlist", do_mark_importlist, CMD_MARK}
    ,
    {"list", do_mark_list, CMD_MARK}
    ,
    {"list2", do_mark_list2, CMD_MARK}
    ,
    {"add", do_mark_add, CMD_MARK}
    ,
    {"remove", do_mark_remove, CMD_MARK}
    ,

    /* Wholist commands */
    {"show", do_wholist_show, CMD_WHOLIST}
    ,
    {"room", do_wholist_room, CMD_WHOLIST}
    ,
    {"help", do_wholist_help, CMD_WHOLIST}
    ,
    /* Normal commands. */
    {"vmap", do_vmap, CMD_NONE}
    ,
    {"mhelp", do_mhelp, CMD_NONE}
    ,
    {"goto", do_goto, CMD_NONE}
    ,
    {"mstop", do_mstop, CMD_NONE}
    ,
    {"find", do_find, CMD_NONE}
    ,
    {"download", download, CMD_NONE}
    ,
    {"maplog", do_map_log, CMD_NONE}
    ,

    {NULL, NULL, 0}
};

int i_mapper_process_client_aliases(char *line)
{
    int i;
    char command[4096], *l;
    int base_cmd;

    DEBUG("i_mapper_process_client_aliases");

    while(line && *line==' ') //remove leading spaces
	line++;

    while(line && *(line+strlen(line)-1)==' ') //remove trailing spaces
	*(line+strlen(line)-1)=0;

    if(composing){

        if(composing==1 && (!case_strcmp("*quit",line)||!case_strcmp("*q",line)||!case_strcmp("*x",line)||!case_strcmp("*exit",line))){
            composing = 2;
        }

        if(composing==1 && (!case_strcmp("*cancel",line)||!case_strcmp("*c",line))){
            composing = 3;
        }

        if(composing==2){
            if(!case_strcmp("n",line)){
                composing=0;
            }else if(!case_strcmp("no",line)){
                composing=0;
            }else if(!case_strcmp("yes",line)){
                composing=0;
            }else if(!case_strcmp("y",line)){
                composing=0;
            }else if(!case_strcmp("c",line)){
                composing=1;
            }else if(!case_strcmp("cancel",line)){
                composing=1;
            }
        }

        if(composing==3){
            if(!case_strcmp("n",line)){
                composing=1;
            }else if(!case_strcmp("no",line)){
                composing=1;
            }else if(!case_strcmp("yes",line)){
                composing=0;
            }else if(!case_strcmp("y",line)){
                composing=0;
            }
        }

        if(composing==1 && (!case_strcmp("*save",line)||!case_strcmp("*s",line))){
            composing = 0;
        }
        
        return 0;
    }

    /* Room commands queue. */
    if (!case_strcmp(line, "l") || !case_strcmp(line, "look") || !case_strcmp(line, "ql")) {
        /* Add -1 to queue. */
        if (mode == CREATING || mode == FOLLOWING)
            add_queue(-1);

        return 0;
    }

    /* Accept swimming. */
    if (!case_strncmp(line, "swim ", 5))
        line += 5;

    /* Same with evading. */
    if (!case_strncmp(line, "evade ", 6))
        line += 6;

    if (!case_strncmp(line, "mj ", 3))
        line += 3;

    if (!case_strncmp(line, "mountjump ", 10))
        line += 10;

    if (!case_strncmp(line, "ford ", 5))
	line+=5;

    for (i = 1; dir_name[i]; i++) {
        if (!case_strcmp(line, dir_name[i]) || !case_strcmp(line, dir_small_name[i])) {
            if (mode == FOLLOWING && current_room && current_room->special_exits) {
                EXIT_DATA *spexit;

                for (spexit = current_room->special_exits; spexit; spexit = spexit->next) {
                    if (spexit->alias && spexit->command && spexit->alias == i) {
                        send_to_server(spexit->command);
                        send_to_server("\r\n");
                        return 1;
                    }
                }
            }

            if (mode == CREATING || mode == FOLLOWING)
                add_queue(i);
            return 0;
        }
    }

    /* Go? */
    if (!strcmp(line, "go") || !strcmp(line, "go sprint") || !strcmp(line, "go dash")
        || !strcmp(line, "go gallop") || !strcmp(line, "go runaway")) {
        if (q_top) {
            clientfv("Command queue isn't empty, clear it first.");
            show_prompt();
            return 1;
        }

        if (!strcmp(line, "go dash"))
            dash_command = "dash ";
        else if (!strcmp(line, "go sprint"))
            dash_command = "sprint ";
        else if (!strcmp(line, "go gallop"))
            dash_command = "gallop ";
        else if (!strcmp(line, "go runaway"))
            dash_command = "runaway ";
        else
            dash_command = NULL;

        /* Go! */
        go_next();
        clientf("\r\n");
        return 1;
    }

/* Toggle */
    if (!strcmp("mp", line)) {
        if (auto_walk != 0) {
            clientfv("Speedwalking paused.");
            auto_walk = 0;
            dash_command = 0;
            show_prompt();
            return 1;
        } else {
            clientfv("Speedwalking resumed.");
            show_prompt();
            go_next();
        }
        return 1;
    }

/* Explicit */
    if (!strcmp(line, "mpause")) {
        if (auto_walk != 0) {
            clientfv("Speedwalking paused.");
            auto_walk = 0;
            dash_command = 0;
            show_prompt();
            return 1;
        } else if (capture_special_exit) {
            capture_special_exit = 0;
            clientfv("Capturing disabled.");
            show_prompt();
            return 1;
        }
    }

    if (!strcmp("munpause", line)) {
        clientfv("Speedwalking resumed.");
        go_next();
        return 1;
    }

    if (!strncmp("farsee ", line, 7)) {
        farseeing = 1;
        add_timer("farsee", 5, t_farsee, 0, 0, 0);
    }

    if (!cmp
            ("gallop *",
             line) 
     || !cmp
            ("runaway *",
             line)) {
			char gallop_dir[256];
		    int i;
            extract_wildcard(0, gallop_dir, 256);
			for (i = 1; dir_name[i]; i++) {
				if (!case_strcmp(gallop_dir, dir_small_name[i])) {
					gallop_direction=i;
					break;
				}
			}
	}

    /* Process from command table. */

    l = line;

    /* First, check for compound commands. */
    if (!strncmp(l, "room ", 5))
        base_cmd = CMD_ROOM, l += 5;
    else if (!strncmp(l, "r ", 2))
        base_cmd = CMD_ROOM, l += 2;
    else if (!strncmp(l, "area ", 5))
        base_cmd = CMD_AREA, l += 5;
    else if (!strncmp(l, "a ", 2))
        base_cmd = CMD_AREA, l += 2;
    else if (!strncmp(l, "vmap ", 5))
        base_cmd = CMD_MAP, l += 5;
    else if (!strncmp(l, "vm ", 3))
        base_cmd = CMD_MAP, l += 3;
    else if (!strncmp(l, "m ", 2))
        base_cmd = CMD_MAP, l += 1;
    else if (!strncmp(l, "exit ", 5))
        base_cmd = CMD_EXIT, l += 5;
    else if (!strncmp(l, "ex ", 3))
        base_cmd = CMD_EXIT, l += 3;
    else if (!strncmp(l, "e ", 2))
        base_cmd = CMD_EXIT, l += 1;
    else if (!strncmp(l, "mark ", 5))
        base_cmd = CMD_MARK, l += 5;
    else if (!strncmp(l, "mk ", 3))
        base_cmd = CMD_MARK, l += 3;
    else if (!strncmp(l, "wholist ", 8))
        base_cmd = CMD_WHOLIST, l+=8;
    else
        base_cmd = CMD_NONE;

    l = get_string(l, command, 4096);

    if (!command[0])
        return 0;

    for (i = 0; cmd_table[i].name; i++) {
        /* If a normal command, compare it full.
         * If after a base command, it's okay to abbreviate it.. */
        if (base_cmd == cmd_table[i].base_cmd
            && ((base_cmd != CMD_NONE && !case_strncmp(command, cmd_table[i].name, strlen(command)))
                || (base_cmd == CMD_NONE && !strcmp(command, cmd_table[i].name)))) {
            (cmd_table[i].func) (l);
            show_prompt();
            return 1;
        }
    }

    /* A little shortcut to 'exit link'. */
    if (base_cmd == CMD_EXIT && isdigit(command[0])) {
        do_exit_link(command);
        show_prompt();
        return 1;
    }
    if (base_cmd == CMD_MAP && isdigit(command[0])) {
        do_vmap(command);
        show_prompt();
        return 1;
    }

    if (capture_special_exit && !special_exit_nocommand) {
        strcpy(cse_command, line);
        clientff(C_R "[Command changed to '%s'.]\r\n" C_0, cse_command);
    }

    return 0;
}
