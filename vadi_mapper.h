/*
 * Copyright (c) 2004, 2005, 2008  Andrei Vasiliu, Vadim Peretokin.
 * 
 * 
 * This file is part of MudBot.
 * 
 * MudBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MudBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MudBot; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Header file specific to Vadi Mapper. */

#define I_MAPPER_H_ID "vadi_mapper"

void do_vmap_update(char *arg);

/* Misc. */
int parsing_room;

int mode;
#define NONE            0
#define FOLLOWING       1
#define CREATING        2
#define GET_UNLOST      3

int get_unlost_exits;
int get_unlost_detected_exits[13];
int auto_walk;
int pear_defence;
int sense_message;
int scout_list;
int evstatus_list;
int pet_list;
int destroying_world;
int door_closed;
int door_locked;
int locate_arena;
char cse_command[5120];
char cse_message[5120];
int close_rmtitle_tag;
int similar;
int title_offset;
int floating_map_enabled;
int floating_normal_map_enabled;
int skip_newline_on_map;
int gag_next_prompt;
int check_for_duplicates;
int godname;
int warn_misaligned;
int warn_environment;
int warn_unlinked;
int allow_title_update;
int capture_special_exit;
int special_exit_vnum;
int special_exit_nocommand;
int special_exit_alias;
char *special_exit_tag;

/* config.mapper.txt options. */
int disable_swimming;
int room_display_limit = 30;
int disable_wholist;
int disable_alertness;
int disable_locating;
int disable_areaname;
int disable_mxp_title;
int disable_mxp_exits;
int disable_mxp_map;
int disable_autolink;
int disable_sewer_grates;
int disable_mhaldorian_sewer_grates;
int auto_pear;
int enable_godrooms;
int disable_icon_travel=0;
int disable_warp_travel=1;
int disable_duanathar=1;
int alert_wormhole=0;
char scry_command[256];
int celerity = 0;
int map_version = 0;

/* Check for updates by default */
int auto_update = 1;

char *dash_command;
char custom_alias[1024];
int my_smiley;

int set_length_to;
int switch_exit_stops_mapping;
int switch_exit_joins_areas;
int update_area_from_survey;
int use_direction_instead;
int unidirectional_exit;

/* goes on top of the create mode - if enabled, no new room ID's should be assigned */
int edit_only;

typedef struct room_data ROOM_DATA;
typedef struct area_data AREA_DATA;
typedef struct exit_data EXIT_DATA;
typedef struct room_type_data ROOM_TYPE;
typedef struct function_data FUNC_DATA;
typedef struct color_data COLOR_DATA;
typedef struct map_element MAP_ELEMENT;
typedef struct element_data ELEMENT;
typedef struct reverse_exit_data REVERSE_EXIT;
typedef struct warp_data WARP_DATA;

/* Special exit structure. */
struct exit_data {
    char *command;              /* This command will trigger the exit. */
    char *message;              /* Or, this message will trigger the exit. */
    int cost;                   /* And this is the cost */

    int alias;                  /* This will contain a direction. */

    /* And when it was triggered, it will take us here. */
    /* If vnum is -1, then set get_unlost. */
    int vnum;
    ROOM_DATA *to;

    /* Chain links. */
    EXIT_DATA *next;
    ROOM_DATA *owner;
};

struct reverse_exit_data {
    /* An exit leads from this room, to here. */
    ROOM_DATA *from;

    /* Via this special exit.. (!= NULL) */
    EXIT_DATA *spexit;

    /* ..or this direction. (!= 0) */
    int direction;

    REVERSE_EXIT *next;
};

/* Room structure. */
struct room_data {
    char *name;
    int vnum;

    /* was the room created from a recycled id? */
    int recycled;

    ROOM_TYPE *room_type;

    short mapped;
    short landmark;
    short underwater;
	short indoors;				/* Is the room indoors? 
								   Outdoors				= 0
								   Indoors				= 1 
								   Unknown				= 2
								   Off-Plane			= 3
								   Orb of Confinement	= 4*/

    ROOM_DATA *next_in_world;
    ROOM_DATA *next_in_area;
    ROOM_DATA *next_in_hash;

    int next_direction;

    /* NULL, N, NE, E, SE, S, SW, W, NW, UP, DOWN, IN, OUT */
    ROOM_DATA *exits[13];

    REVERSE_EXIT *rev_exits;

    /* Beginning of binary-saved values. If you change these, change save_binary_map! */
    int vnum_exits[13];
    short detected_exits[13];
    short locked_exits[13];
    short exit_length[13];
    short use_exit_instead[13];
    short exit_stops_mapping[13];
    short exit_joins_areas[13];
    /* End. */

    ELEMENT *tags;

    EXIT_DATA *special_exits;
    AREA_DATA *area;
	WARP_DATA *warp;

    /* Pathfinder. */
    int pf_cost;
    int pf_direction;
    int pf_highlight;
    ROOM_DATA *pf_parent;
    ROOM_DATA *next_in_pfno;
    ROOM_DATA *next_in_pfco;

    /* Pathfinder 2. */
    ROOM_DATA *pf_next;
    ROOM_DATA **pf_prev;

    /* Person in this room. */
    char *person_here;
    char *mark_here;
    char *wormhole_here;
};

/* Area structure. */
struct area_data {
    char *name;

    int disabled;
    int ID;

    int needs_cleaning;

    ROOM_DATA *rooms;
    ROOM_DATA *last_room;

    AREA_DATA *next;
	
	WARP_DATA *warps;
};

/* Exit flags. If more are added, change from 'short' to 'int'. */
#define EXIT_NORMAL	1
#define EXIT_STOPPING	2
#define EXIT_OTHER_AREA	4
#define EXIT_PATH	8
#define EXIT_UNLINKED  16
#define EXIT_LOCKED    32

/* One room on the map. */
struct map_element {
    ROOM_DATA *room;

    char *color;
    char *color2;
    short warn;

    short In;
	short Out;

    short up;
    short down;

    short e;                    /* - */
    short s;                    /* | */
    short se;                   /* \ *//* se AND se_rev: */
    short se_rev;               /* / *//* X */

    short extra_e;
    short extra_s;
    short extra_se;
    short extra_se_rev;
};

#define MAX_HASH 512

//#define MAP_X 18
//#define MAP_Y 14
#define MAP_X 14
#define MAP_Y 10

#define EX_N	1
#define EX_NE	2
#define EX_E	3
#define EX_SE	4
#define EX_S	5
#define EX_SW	6
#define EX_W	7
#define EX_NW	8
#define EX_U	9
#define EX_D	10
#define EX_IN	11
#define EX_OUT	12

/* Room type structure. */
struct room_type_data {
    char *name;
    char *color;
    char *color2;

    int must_swim;
    int underwater;
    int avoid;

    ROOM_TYPE *next;
};

/* Command table. */
struct function_data {
    char *name;

    void (*func) (char *arg);

    int base_cmd;
};

/* Colors table. */
struct color_data {
    char *name;
    char *code;

    char *title_code;
    int length;
};

/* A link in a chain. This can be anything. */

struct element_data {
    ELEMENT *next;
    ELEMENT *prev;
    ELEMENT **first;

    /* One pointer value. */
    void *p;

    /* And one integer value. */
    int value;
};

struct warp_data {

	AREA_DATA *area;
	EXIT_DATA *link;
	WARP_DATA *reverse;
	WARP_DATA *next;
	ROOM_DATA *source;

};

/* Used by the vmap voodoo - draws the exit character at a given coordinate */
void add_exit_char(char *var, int dir);
/* Adds a number onto the queue of actions - "-1" for look, or a number corresponding to an element in the dir_name array for a walking direction */
void add_queue(int value);
/* Same as above except adds to top of the queue */
void add_queue_top(int value);
/* Creates a new room type, with the given name. color is foreground, color2 background */
ROOM_TYPE *add_room_type(char *name, char *color, char *color2);
/* Adds a room to the pathfinder net */
int add_to_pathfinder(ROOM_DATA * room, int cost);
/* Clears the pathfinder net */
void clean_paths();
/* Obtains the corresponding foreground color, given a background one */
char *background_foreground(char *name);
/* Checks if we can dash to the given room OK */
int can_dash(ROOM_DATA * room);

/* Non-case sensitive version of strstr */
int case_strstr(char *haystack, char *needle);

/* Wipes the given area from the internal map */
void destroy_area(AREA_DATA * area);
